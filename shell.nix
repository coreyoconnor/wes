{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/refs/tags/23.05.tar.gz") {} }:
pkgs.mkShell {
  CLANG_PATH = pkgs.clang + "/bin/clang";
  CLANGPP_PATH = pkgs.clang + "/bin/clang++";
  hardeningDisable = [ "all" ];
  buildInputs = with pkgs; [
    stdenv
    openjdk
    boehmgc
    libunwind
    re2
    clang
    zlib
    coursier
    sbt
  ];
}

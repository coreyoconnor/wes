import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}
import scala.scalanative.build._

lazy val stage = taskKey[File]("stage")

Global / resolvers ++= Resolver.sonatypeOssRepos("public")

ThisBuild / nativeConfig ~= { c =>
  c.withMode(Mode.releaseFast)
    .withLTO(LTO.thin)
    .withEmbedResources(true)
    .withResourceExcludePatterns(Seq("**/*.proto", "/BUILD", "/LICENSE.txt"))
}
ThisBuild / scalaVersion := "3.6.2"
ThisBuild / scalacOptions += "-explain"
ThisBuild / scalacOptions += "-experimental"

val sharedSettings = Seq(
  libraryDependencies += "org.scodec" %%% "scodec-core" % "2.3.2",
  libraryDependencies += "com.lihaoyi" %%% "fansi" % "0.5.0",
  libraryDependencies += "com.lihaoyi" %%% "fastparse" % "3.1.1",
  libraryDependencies += "org.typelevel" %%% "cats-core" % "2.12.0",
  libraryDependencies += "io.github.cquiroz" %%% "scala-java-time" % "2.6.0",
  scalapropsVersion := "0.9.1",
  Compile / scalacOptions -= "-Xfatal-warnings",
  Compile / run / fork := true,
  Compile / run / connectInput := true,
  javaOptions += s"-DtestROMsPath=${file("nes-test-roms").absolutePath}",
  outputStrategy := Some(StdoutOutput),
) ++ scalapropsSettings

lazy val lib = crossProject(JVMPlatform, NativePlatform)
  .crossType(CrossType.Full)
  .withoutSuffixFor(JVMPlatform)
  .settings(sharedSettings)
  .settings(
    libraryDependencies += "org.scalameta" %% "munit" % "1.0.4" % Test,
    testFrameworks += new TestFramework("munit.Framework"),
    Compile / mainClass := Some("wes.nes.tui.Main"),
  )

lazy val libNative = lib.native
lazy val libJVM = lib.jvm.settings(Test/fork := true)

lazy val gui = crossProject(NativePlatform)
  .crossType(CrossType.Full)
  .withoutSuffixFor(NativePlatform)
  .settings(
    sharedSettings,
    Compile / scalacOptions -= "-Xfatal-warnings",
    Compile / mainClass := Some("wes.gui.Main"),
  )
  .dependsOn(lib)

test := (libJVM / Test / test).value

stage := {
  val guiExe = (gui.native / Compile / nativeLink).value
  val targetGuiExe = target.value / "wes-gui"

  val tuiExe = (libNative / Compile / nativeLink).value
  val targetTuiExe = target.value / "wes-tui"

  sbt.IO.copyFile(guiExe, targetGuiExe)
  sbt.IO.copyFile(tuiExe, targetTuiExe)

  target.value
}

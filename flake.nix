{
  description = "wes";

  inputs = {
    flake-utils.follows = "typelevel-nix/flake-utils";
    nixpkgs.follows = "typelevel-nix/nixpkgs";
    sbt = {
      url = "github:zaninime/sbt-derivation/master";
      inputs.nixpkgs.follows = "typelevel-nix/nixpkgs";
    };
    typelevel-nix.url = "github:typelevel/typelevel-nix";
  };

  outputs = {
    self,
    flake-utils,
    nixpkgs,
    sbt,
    typelevel-nix
  }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ typelevel-nix.overlays.default ];
        };
      in rec {
        packages.default = (sbt.mkSbtDerivation.${system}).withOverrides({ stdenv = pkgs.llvmPackages.stdenv; }) {
          pname = "wes";
          version = "0.1.0";
          src = self;
          depsSha256 = "sha256-4qWN0KL8cRWOwltr8kVAW3GWmzXrO6hrhDxbn3RRVNk=";
          depsWarmupCommand = ''
            sbt '+Test/updateFull ; +Test/compileIncSetup'
          '';
          buildPhase = ''
            sbt update
          '';
          installPhase = ''
            mkdir -p $out/bin
            cp target/wes-gui $out/bin/
            cp target/wes-tui $out/bin/
          '';
          buildInputs = with pkgs; [
            gtk4
            libunwind
            zlib
          ];
          nativeBuildInputs = with pkgs; [
            pkg-config
            which
          ];
          hardeningDisable = [ "fortify" ];
        };

        devShell = pkgs.devshell.mkShell {
          imports = [ typelevel-nix.typelevelShell "${pkgs.devshell.extraModulesDir}/language/c.nix"];
          name = "wes-devshell";
          typelevelShell = {
            jdk.package = pkgs.jdk21;
            native.enable = true;
          };
          env = [
            {
              name = "NIX_CFLAGS_COMPILE_${pkgs.stdenv.cc.suffixSalt}";
              value = "-Wno-unused-command-line-argument";
            }
          ];
          packagesFrom = [ packages.default ];
          language.c = {
            libraries = packages.default.buildInputs;
            includes = packages.default.buildInputs;
          };
        };
      }
    );
}

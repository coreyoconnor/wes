self: super:
let
  lib = self.lib;
  CLANG_PATH = self.clang + "/bin/clang";
  CLANGPP_PATH = self.clang + "/bin/clang++";
  deps = with self; [ stdenv openjdk boehmgc.dev libunwind.dev re2 clang zlib.dev ];
in with lib; rec {
  sbt-scala-native-env = self.stdenv.mkDerivation rec {
    name = "sbt-scala-native-env";

    inherit CLANG_PATH CLANGPP_PATH;
    shellHook = ''
      export PATH=${self.sbt}/bin:$PATH
    '';
    propagatedBuildInputs = deps ++ [ self.sbtix ];
  };

  sbt-scala-native-setup = self.makeSetupHook {
    inherit deps;
    substitutions = { clangPath = CLANG_PATH; clangPPPath = CLANGPP_PATH; };
  } ./sbt-scala-native-setup.sh;

  sbt = super.sbt.overrideAttrs (oldAttrs: rec {
    propagatedBuildInputs = [ sbt-scala-native-setup ];
  });
}

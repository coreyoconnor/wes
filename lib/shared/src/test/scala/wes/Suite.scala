package wes

abstract class Suite extends munit.FunSuite {
  val testROMsPath = sys.props("testROMsPath")

  assert(testROMsPath != null, "set testROMsPath system property")
}

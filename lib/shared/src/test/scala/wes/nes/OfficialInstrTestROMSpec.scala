package wes
package nes

import wes.native.conversions.*
import wes.native.types.*
import wes.rom.nes.*

import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.concurrent.duration.FiniteDuration
import java.util.concurrent.TimeUnit

class OfficialInstrTestROMSpec extends Suite {
  import GBlarggTest.*

  val testPath = "../../reference/instrs_v5_official_only.nes"

  test("starts inst_test-v5 official only") {
    val rom = FileIO.Live.dataForPath(testPath)
    val Right(v1: Formats.V1) = Formats.read(rom): @unchecked
    println(v1)

    val state = TestMapper1.init(v1)

    // experimentally determined limit
    val maxInsts = 35000
    (0 until maxInsts) foreach { _ =>
      val pc = state.cpu.registers.pc
      try {
        TestMapper1.execNext(state)
      } catch {
        case ex: Throwable => {
          println(f"PC: $pc%04X")
          val (asm, inc) = TestMapper1.disasmNext(state)(pc)
          println(s"$asm")
          throw ex
        }
      }
    }

    if (!state.started) {
      sys.error(s"failed to start within $maxInsts instructions")
    }
  }

  override def munitTimeout: Duration = new FiniteDuration(60, TimeUnit.SECONDS)

  test("official only - completes") {
    val state = run(testPath)
    assert(
      state.statusCode == 0.toU8,
      f"Failed with status ${state.statusCode}%02X:" + state.message
    )
  }
}

package wes
package nes
package ppu

import wes.native.conversions.*
import wes.native.types.*

class StateSpec extends Suite {
  test("nametable select to t") {
    val state = new State

    state.baseNametableIndex = 0
    assertEquals(state.t.value.slice(2, 10), 0.toU16)
    state.baseNametableIndex = 1
    assertEquals(state.t.value.slice(2, 10), 1.toU16)
    state.baseNametableIndex = 2
    assertEquals(state.t.value.slice(2, 10), 2.toU16)
    state.baseNametableIndex = 3
    assertEquals(state.t.value.slice(2, 10), 3.toU16)
  }

  test("ppuscroll first write") {
    val state = new State

    state.scrollX = 0x7d.toByte
    assertEquals(state.t.value.slice(5, 0), 0b01111.toU16)
    assertEquals(state.fineScrollX, 0b101)

    state.scrollX = 0x70.toByte
    assertEquals(state.t.value.slice(5, 0), 0b01110.toU16)
    assertEquals(state.fineScrollX, 0)

    state.scrollX = 0xff.toByte
    assertEquals(state.t.value.slice(5, 0), 0b11111.toU16)
    assertEquals(state.fineScrollX, 0b111)
  }

  test("ppuscroll second write") {
    val state = new State

    state.baseNametableIndex = 0
    state.scrollX = 0x7d.toByte
    state.scrollY = 0x5e.toByte

    assertEquals(state.t.value, 0b01100001_01101111.toU16)
    assertEquals(state.fineScrollX, 0b101)
  }

  test("ppuaddr first write") {
    val state = new State

    state.baseNametableIndex = 0
    state.scrollX = 0x7d.toByte
    state.scrollY = 0x5e.toByte
    state.t.updateHigh(0x3d.toU8)

    assertEquals(state.t.value, 0b00111101_01101111.toU16)
    assertEquals(state.fineScrollX, 0b101)
  }

  test("ppuaddr second write") {
    val state = new State

    state.baseNametableIndex = 0
    state.scrollX = 0x7d.toByte
    state.scrollY = 0x5e.toByte
    state.t.updateHigh(0x3d.toU8)
    state.t.updateLow(0xf0.toU8)
    state.updateVFromT()

    assertEquals(state.t.value, 0b0111101_11110000.toU16)
    assertEquals(state.v.value, 0b0111101_11110000.toU16)
    assertEquals(state.fineScrollX, 0b101)
  }

  test("course X increment") {
    val state = new State

    def check(v: Int, n: Boolean) = {
      assertEquals(state.v.value & 0x1f, v.toU16)
      assertEquals(state.v.value.testBit(10), n)
    }

    check(0b00000, false)

    state.incrementVCourseHoriz()

    check(0b00001, false)

    // 31 should reach right before overflow
    (1 until 31) foreach { _ =>
      state.incrementVCourseHoriz()
    }

    check(0b11111, false)

    // overflow
    state.incrementVCourseHoriz()
    check(0, true)

    // and again
    (0 until 31) foreach { _ =>
      state.incrementVCourseHoriz()
    }

    check(0b11111, true)

    // overflow
    state.incrementVCourseHoriz()
    check(0, false)
  }

  test("v Y increment - fine overflows") {
    val state = new State

    assertEquals(state.v.fineScrollY, 0)

    (1 until 8) foreach { i =>
      state.incrementVVert()

      assertEquals(state.v.fineScrollY, i)
    }

    state.incrementVVert()
    assertEquals(state.v.fineScrollY, 0)
    assertEquals(state.v.courseScrollY, 1)
    assertEquals(state.v.nametableY, 0)

    // up to last row
    for {
      row <- 1 until 29
      y <- 0 until 8
    } {
      state.incrementVVert()
    }

    assertEquals(state.v.courseScrollY, 29)
    assertEquals(state.v.nametableY, 0)

    // last tile row - 29
    // increment up to last pixel row
    (0 until 7) foreach { _ => state.incrementVVert() }

    // should be edge of overflow
    assertEquals(state.v.fineScrollY, 7)
    assertEquals(state.v.nametableY, 0)

    // overflow
    state.incrementVVert()

    assertEquals(state.v.fineScrollY, 0)
    assertEquals(state.v.courseScrollY, 0)
    assertEquals(state.v.nametableY, 1)

    // again
    for {
      row <- 0 until 30
      line <- 0 until 8
    } state.incrementVVert()

    assertEquals(state.v.fineScrollY, 0)
    assertEquals(state.v.courseScrollY, 0)
    assertEquals(state.v.nametableY, 0)
  }

  test("v value <-> update iso") {
    val state = new State

    (0 until (1 << 15)) foreach { i =>
      state.v.update(i.toU16)
      val actual = state.v.value

      assertEquals(actual, i.toU16)
    }
  }
}

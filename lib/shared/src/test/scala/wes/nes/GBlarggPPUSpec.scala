package wes
package nes

import wes.native.conversions.*
import wes.native.types.*
import wes.rom.nes.*

import scala.collection.mutable
import wes.nes.TestROMRunner.Config

class GBlarggPPUSpec extends Suite {
  import GBlarggTest.*

  val testPath = testROMsPath + "/blargg_ppu_tests_2005.09.15b/vram_access.nes"

  test("completes") {
    val state = run(testPath, Config())
    println(state.message)
    assert(
      state.statusCode == 0.toU8,
      f"Failed with status ${state.statusCode}%02X:" + state.message
    )
  }
}

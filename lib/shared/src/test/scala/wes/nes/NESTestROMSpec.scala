package wes
package nes

import wes.native.conversions.*
import wes.native.types.*
import wes.rom.nes.*

object NESTestTrace {
  import NES.*

  case class Trace(
      pc: PC,
      asm: String,
      a: U8,
      x: U8,
      y: U8,
      status: U8,
      sp: U8,
      ppuH: Int,
      ppuV: Int,
      cycles: Int
  ) {
    def cpuState: String =
      f"$pc%04X A:$a%02X X:$x%02X Y:$y%02X P:$status%02X SP:$sp%02X"
    def ppuState: String =
      f"PPU:$ppuV,$ppuH"
  }

  val TraceMatch =
    raw"^(\w{4})\s.{10}[ \*](\w.*?)A:(\w\w) X:(\w\w) Y:(\w\w) P:(\w\w) SP:(\w\w) PPU:(.*?),(.*?) CYC:(.*)$$".r

  def parse(path: String): Seq[Trace] = {

    scala.io.Source.fromFile(path, "UTF-8").getLines().toSeq map {
      case TraceMatch(
            pcStr,
            instStr,
            aStr,
            xStr,
            yStr,
            statusStr,
            spStr,
            ppuVStr,
            ppuHStr,
            cyclesStr
          ) => {
        Trace(
          pc = parseHexU16(pcStr).toAddr,
          asm = instStr.trim,
          a = parseHexU8(aStr),
          x = parseHexU8(xStr),
          y = parseHexU8(yStr),
          status = parseHexU8(statusStr),
          sp = parseHexU8(spStr),
          ppuH = ppuHStr.trim.toInt,
          ppuV = ppuVStr.trim.toInt,
          cycles = cyclesStr.trim.toInt
        )
      }
    }
  }
}

class NESTestROMSpec extends Suite {
  import NESTestTrace.*

  println(java.nio.file.Paths.get(".").toAbsolutePath().normalize())

  val testPath = "../../reference/nestest.nes"
  val rom = FileIO.Live.dataForPath(testPath)
  val Right(v1: Formats.V1) = Formats.read(rom): @unchecked
  assert(v1.mapperId == 0)

  val expectedTrace = NESTestTrace.parse("../../reference/nestest.log")

  object NESTestMapper extends NES.Mapper {
    import NES.*

    type State = Configs.Mapper0.State

    object CPUBus extends DelegateCPUBus(Configs.Mapper0.CPUBus)

    object PPUBus extends DelegatePPUBus(Configs.Mapper0.PPUBus)

    def init(rom: Formats.V1): State = Configs.Mapper0.init(rom)

    def disasmNext(state: State) = deriveDisasmNext(CPUBus, state)

    def execNext(state: State) = deriveExecNext(CPUBus, PPUBus, state)
  }

  test("trace parses") {
    val expectedFirstTrace = Trace(
      pc = 0xc000.toU16,
      asm = "JMP $C5F5",
      a = 0x00.toU8,
      x = 0x00.toU8,
      y = 0x00.toU8,
      status = 0x24.toU8,
      sp = 0xfd.toU8,
      ppuV = 0,
      ppuH = 21,
      cycles = 7
    )
    val expectedLastTrace = Trace(
      pc = 0xc66e.toU16,
      asm = "RTS",
      a = 0x00.toU8,
      x = 0xff.toU8,
      y = 0x15.toU8,
      status = 0x27.toU8,
      sp = 0xfd.toU8,
      ppuV = 233,
      ppuH = 209,
      cycles = 26554
    )
    assertEquals(expectedTrace.head, expectedFirstTrace)
    assertEquals(expectedTrace.last, expectedLastTrace)
  }

  def traceNext(state: NESTestMapper.State): Trace = {
    val pc = state.cpu.registers.pc
    val a = state.cpu.registers.acc
    val x = state.cpu.registers.x
    val y = state.cpu.registers.y
    val status = state.cpu.flags.registerValue
    val sp = state.cpu.registers.sp
    val (asm, _) = NESTestMapper.disasmNext(state)(state.cpu.registers.pc)
    val ppuH = state.ppu.hPos
    val ppuV = state.ppu.vPos
    val cycles = state.cpu.cycleCount
    NESTestMapper.execNext(state)

    Trace(
      pc = pc,
      asm = asm,
      a = a,
      x = x,
      y = y,
      status = status,
      sp = sp,
      ppuH = ppuH,
      ppuV = ppuV,
      cycles = cycles.toInt
    )
  }

  test("direct - start") {
    val state = NESTestMapper.init(v1)
    state.cpu.registers.pc = 0xc000.toU16

    val maxInsts = 25
    val actualTrace = (0 until maxInsts) map { _ =>
      traceNext(state)
    }

    actualTrace.zip(expectedTrace).foreach { case (actual, expected) =>
      assertEquals(actual.cpuState, expected.cpuState)
    }
  }

  val instsToCompleteOfficial = 4782

  test("direct - official - more or less") {
    val state = NESTestMapper.init(v1)
    state.cpu.registers.pc = 0xc000.toU16

    val actualTrace = (0 until instsToCompleteOfficial) map { _ =>
      traceNext(state)
    }

    actualTrace.zip(expectedTrace).foreach { case (actual, expected) =>
      assertEquals(actual.cpuState, expected.cpuState)
    }
  }

  test("direct - official - more or less - with PPU") {
    val state = NESTestMapper.init(v1)
    state.cpu.registers.pc = 0xc000.toU16

    val actualTrace = (0 until instsToCompleteOfficial) map { _ =>
      traceNext(state)
    }

    actualTrace.zip(expectedTrace).foreach { case (actual, expected) =>
      assertEquals(actual.cpuState, expected.cpuState)
      assertEquals(actual.cycles, expected.cycles, actual.cpuState)
      assertEquals(actual.ppuState, expected.ppuState, actual.cpuState)
    }
  }
}

package wes
package nes

import wes.native.conversions.*
import wes.native.types.*
import wes.rom.nes.*

class SpriteHitTestROMSpec extends Suite {
  val testPaths = Seq(
    "01.basics.nes",
    "02.alignment.nes",
    "03.corners.nes",
    "04.flip.nes",
    "05.left_clip.nes",
    "06.right_edge.nes",
    "07.screen_bottom.nes",
    "08.double_height.nes",
    "09.timing_basics.nes",
    "10.timing_order.nes",
    "11.edge_timing.nes"
  ).map(testROMsPath + "/sprite_hit_tests_2005.10.05/" + _)

  val disasmExclusions = {
    import TestROMMapper.Disasm.Region

    TestROMMapper.Disasm.RegionExclusions(
      Region("delay_msec", 0xe280, 0xe28f),
      Region("wait_vbl", 0xe577, 0xe582),
      Region("lib", 0x0000, 0xe1e6),
      Region("lib", 0xe224, 0xffff)
    )
  }

  val config = TestROMRunner.Config(trace = true, disasmExclusions = disasmExclusions)

  object TestMapper extends NES.Mapper with TestROMMapper {
    import NES.*

    class State(mirroring: Formats.V1.Mirroring, sramSize: Int, prgBanks: Int, chrSize: Int)
        extends Configs.Mapper0.State(mirroring, sramSize, prgBanks, chrSize) {
      var readCount = 0
      var done = false
      var statusCode: U8 = 0x00.toU8
      var message: String | Null = null
    }

    override val requiresInit: Boolean = false

    override def isDone(state: State): Boolean =
      state.cpu.registers.pc == 0xe635.toU16 || state.done

    object CPUBus extends DelegateCPUBus(Configs.Mapper0.CPUBus) {
      override def readByte(
          addr: NES.Addr,
          state: State
      ): Byte = {

        if (addr.toInt == 0xe00b) then {
          println("""reached label "report_final_result"""")
          state.done = true
        }

        super.readByte(addr, state)
      }

      override def writeByte(
          addr: NES.Addr,
          value: Byte,
          state: State
      ): Byte = {
        if addr.toInt == 0xf8 then {
          println(f"result $value%02X")
          state.statusCode = value.toU8
        }

        super.writeByte(addr, value, state)
      }
    }

    object PPUBus extends DelegatePPUBus(Configs.Mapper0.PPUBus)

    def init(rom: Formats.V1): State = {
      val state: State = new State(rom.mirroring, rom.ramPages, rom.prgSize, rom.chrSize)
      state.load(rom)
      state.cpu.registers.pc = CPUBus.readU16(CPU6502.resetVector, state)
      state
    }

    def disasmNext(state: State) = deriveDisasmNext(CPUBus, state)

    def execNext(state: State) = deriveExecNext(CPUBus, PPUBus, state)
  }

  testPaths foreach { testPath =>
    test(testPath) {
      val state = TestROMRunner.run(TestMapper, testPath, config = config)
      println(s"status code: ${state.statusCode}")
      assert(state.statusCode.toInt == 1)
    }
  }
}

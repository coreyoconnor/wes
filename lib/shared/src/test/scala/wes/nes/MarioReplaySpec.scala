package wes
package nes

import wes.native.conversions.*
import wes.native.types.*
import wes.rom.nes.*
import wes.nes.TUI

class MarioReplaySpec extends Suite {
  val testPath =
    "/mnt/storage/media/Software/Games/NES/Super Mario Bros. (E).nes"

  object TestMapper extends NES.Mapper with TestROMMapper {
    import NES.*

    class State(mirroring: Formats.V1.Mirroring, sramBanks: Int, prgSize: Int, chrSize: Int)
        extends Configs.Mapper0.State(mirroring, sramBanks, prgSize, chrSize) {
      var statusCode: Int = 0
    }

    override def hasStarted(state: State): Boolean = true

    val targetHash: Int = 0

    val screenshotPeriod = 90
    var nextFrame = screenshotPeriod

    override def isDone(state: State): Boolean = {
      if state.ppu.frameCount > nextFrame then {
        val pixels = TUI.bmpScreenshotColorOut(state)
        nextFrame += screenshotPeriod
        pixels.hashCode() == targetHash
      } else false
    }

    object CPUBus extends DelegateCPUBus(Configs.Mapper0.CPUBus)

    object PPUBus extends DelegatePPUBus(Configs.Mapper0.PPUBus)

    def init(rom: Formats.V1): State = {
      val state: State = new State(rom.mirroring, rom.ramPages, rom.prgSize, rom.chrSize)
      state.load(rom)
      state.cpu.registers.pc = CPUBus.readU16(CPU6502.resetVector, state)
      state
    }

    def disasmNext(state: State) = deriveDisasmNext(CPUBus, state)

    def execNext(state: State) = deriveExecNext(CPUBus, PPUBus, state)
  }

  test("up to start game") {
    val state =
      TestROMRunner.run(TestMapper, testPath, TestROMRunner.Config().copy(completeLimit = 6000000L))
    println(s"status code: ${state.statusCode}")
    assert(state.statusCode.toInt == 1)
  }
}

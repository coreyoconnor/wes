package wes
package nes

import wes.native.conversions.*
import wes.native.types.*
import wes.rom.nes.*

import scala.collection.mutable

class PPUReadBufferTestROMSpec extends Suite {
  import GBlarggTest.*

  val testPath = testROMsPath + "/ppu_read_buffer/test_ppu_read_buffer.nes"

  test("completes") {
    val state = run(testPath)
    println(state.message)
    assert(
      state.statusCode == 0.toU8,
      f"Failed with status ${state.statusCode}%02X:" + state.message
    )
  }
}

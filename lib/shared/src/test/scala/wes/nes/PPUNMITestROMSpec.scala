package wes
package nes

import wes.lines.*
import wes.native.conversions.*
import wes.native.types.*
import wes.rom.nes.*

import scala.collection.mutable

class PPUNMITestROMSpec extends Suite {
  import GBlarggTest.*

  val baseDir = testROMsPath + "/ppu_vbl_nmi/rom_singles/"

  val testROMs = lines"""
01-vbl_basics.nes
02-vbl_set_time.nes
03-vbl_clear_time.nes
04-nmi_control.nes
05-nmi_timing.nes
06-suppression.nes
07-nmi_on_timing.nes
08-nmi_off_timing.nes
09-even_odd_frames.nes
10-even_odd_timing.nes
"""

//   val testROMs = lines"""
// 01-vbl_basics.nes
// 02-vbl_set_time.nes
// 03-vbl_clear_time.nes
// 04-nmi_control.nes
// 09-even_odd_frames.nes
// 10-even_odd_timing.nes
// """

  val disasmExclusions = {
    import TestROMMapper.Disasm.Region

    TestROMMapper.Disasm.RegionExclusions(
      Region("lib", 0x0000, 0xe199),
      Region("lib", 0xe400, 0xffff)
    )
  }

  val config =
    TestROMRunner.Config(trace = true, disasmExclusions = disasmExclusions, initLimit = 20000)

  testROMs foreach { testROM =>
    test(s"completes $testROM") {
      val state = run(baseDir + testROM, config = config)
      println(state.message)
      assert(
        state.statusCode == 0.toU8,
        f"Failed with status ${state.statusCode}%02X:" + state.message
      )
    }
  }
}

package wes
package nes
package tui

import fastparse.*, ScriptWhitespace.*

import wes.nes.NES.{Addr, toAddr}

class REPLModeParserSpec extends Suite {
  import REPLMode.*

  test("parses tsr") {
    val input = "tsr"
    val result = parse(input, Parser.stmt(using _), verboseFailures = true)
    assertEquals(result, Parsed.Success(TraceAndShowRegisters(1), 3))
  }

  test("parses trace 10") {
    val input = "trace 10"
    val result = parse(input, Parser.stmt(using _), verboseFailures = true)
    assertEquals(result, Parsed.Success(TraceCount(10), 8))
  }

  test("parses trace    10") {
    val input = "trace    10"
    val result = parse(input, Parser.stmt(using _), verboseFailures = true)
    assertEquals(result, Parsed.Success(TraceCount(10), 11))
  }

  test("parses tap start ") {
    val input = "tap start"
    val result = parse(input, Parser.stmt(using _), verboseFailures = true)
    assertEquals(result, Parsed.Success(InputTap(Joysticks.Button.Start), 9))
  }

  test("parses sm $28 12") {
    val input = "sm $28 12"
    val result = parse(input, Parser.stmt(using _), verboseFailures = true)
    assertEquals(result, Parsed.Success(ShowMemory(0x28.toAddr, 12), 9))
  }
}

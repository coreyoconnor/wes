package wes

import wes.native.conversions.*
import wes.native.types.*
import scodec.bits.*
import wes.rom.nes.Formats

class DisasmSpec extends Suite {
  Seq(
    // ADC
    "6102" -> "ADC ($02,X)",
    "6505" -> "ADC $05",
    "6907" -> "ADC #$07",
    "6d0504" -> "ADC $0405",
    "7105" -> "ADC ($05),Y",
    "75BB" -> "ADC $BB,X",
    "7dA1B2" -> "ADC $B2A1,X",
    "79B2A1" -> "ADC $A1B2,Y",

    // AND
    "21BC" -> "AND ($BC,X)",
    "2588" -> "AND $88",
    "2942" -> "AND #$42",
    "2D8321" -> "AND $2183",
    "3187" -> "AND ($87),Y",
    "3588" -> "AND $88,X",
    "396655" -> "AND $5566,Y",
    "3d7522" -> "AND $2275,X",

    // ASL
    "062a" -> "ASL $2A",
    "0a" -> "ASL A",
    "0e4321" -> "ASL $2143",
    "16FE" -> "ASL $FE,X",
    "1e7533" -> "ASL $3375,X",

    // BCC
    "90ee" -> "BCC $EE",

    // BCS
    "b03f" -> "BCS $3F",

    // BEQ
    "f012" -> "BEQ $12",

    // BIT
    "24ee" -> "BIT $EE",
    "2cccbb" -> "BIT $BBCC",

    // BMI
    "30ee" -> "BMI $EE",

    // BNE
    "d0ee" -> "BNE $EE",

    // BPL
    "10ee" -> "BPL $EE",

    // BRK
    "00" -> "BRK",

    // BVC
    "5033" -> "BVC $33",

    // BVS
    "7023" -> "BVS $23",

    // CLC
    "18" -> "CLC",

    // CLD
    "d8" -> "CLD",

    // CLI
    "58" -> "CLI",

    // CLV
    "b8" -> "CLV",

    // CMP
    "c1ee" -> "CMP ($EE,X)",
    "c5ef" -> "CMP $EF",
    "c922" -> "CMP #$22",
    "cd3412" -> "CMP $1234",
    "d155" -> "CMP ($55),Y",
    "d555" -> "CMP $55,X",
    "d95511" -> "CMP $1155,Y",
    "dd8821" -> "CMP $2188,X",

    // CPX
    "e088" -> "CPX #$88",
    "e422" -> "CPX $22",
    "ec9822" -> "CPX $2298",

    // CPY
    "c089" -> "CPY #$89",
    "c429" -> "CPY $29",
    "cc9922" -> "CPY $2299",

    // DEC
    "c632" -> "DEC $32",
    "CE8822" -> "DEC $2288",
    "D643" -> "DEC $43,X",
    "DE7222" -> "DEC $2272,X",

    // DEX
    "CA" -> "DEX",

    // DEY
    "88" -> "DEY",

    // EOR
    "41BC" -> "EOR ($BC,X)",
    "4588" -> "EOR $88",
    "4942" -> "EOR #$42",
    "4D8321" -> "EOR $2183",
    "5187" -> "EOR ($87),Y",
    "5588" -> "EOR $88,X",
    "596655" -> "EOR $5566,Y",
    "5d7522" -> "EOR $2275,X",

    // INC
    "E6aa" -> "INC $AA",
    "EE1234" -> "INC $3412",
    "F683" -> "INC $83,X",
    "fe3333" -> "INC $3333,X",

    // INX
    "E8" -> "INX",

    // INY
    "C8" -> "INY",

    // JMP
    "4c4321" -> "JMP $2143",
    "6ceceb" -> "JMP ($EBEC)",

    // JSR
    "203212" -> "JSR $1232",

    // LDA
    "a901" -> "LDA #$01",
    "A505" -> "LDA $05",
    "B505" -> "LDA $05,X",
    "AD0102" -> "LDA $0201",
    "BD0102" -> "LDA $0201,X",
    "B90102" -> "LDA $0201,Y",
    "A102" -> "LDA ($02,X)",
    "B102" -> "LDA ($02),Y",

    // LDX
    "a288" -> "LDX #$88",
    "a634" -> "LDX $34",
    "ae1234" -> "LDX $3412",
    "b65e" -> "LDX $5E,Y",
    "bedddd" -> "LDX $DDDD,Y",

    // LDY
    "a088" -> "LDY #$88",
    "a434" -> "LDY $34",
    "ac1234" -> "LDY $3412",
    "b45e" -> "LDY $5E,X",
    "bcdddd" -> "LDY $DDDD,X",

    // LSR
    "4634" -> "LSR $34",
    "4a" -> "LSR A",
    "4e1234" -> "LSR $3412",
    "565e" -> "LSR $5E,X",
    "5edddd" -> "LSR $DDDD,X",

    // NOP
    "EA" -> "NOP",

    // ORA
    "01aa" -> "ORA ($AA,X)",
    "0582" -> "ORA $82",
    "09ab" -> "ORA #$AB",
    "0d1234" -> "ORA $3412",
    "1122" -> "ORA ($22),Y",
    "1533" -> "ORA $33,X",
    "19aabb" -> "ORA $BBAA,Y",
    "1d3344" -> "ORA $4433,X",

    // PHA
    "48" -> "PHA",

    // PHP
    "08" -> "PHP",

    // PLA
    "68" -> "PLA",

    // PLP
    "28" -> "PLP",

    // ROL
    "2a" -> "ROL A",
    "2634" -> "ROL $34",
    "2e1234" -> "ROL $3412",
    "365e" -> "ROL $5E,X",
    "3edddd" -> "ROL $DDDD,X",

    // ROR
    "6a" -> "ROR A",
    "6634" -> "ROR $34",
    "6e1234" -> "ROR $3412",
    "765e" -> "ROR $5E,X",
    "7edddd" -> "ROR $DDDD,X",

    // RTI
    "40" -> "RTI",

    // RTS
    "60" -> "RTS",

    // SBC
    "e1bb" -> "SBC ($BB,X)",
    "e522" -> "SBC $22",
    "e988" -> "SBC #$88",
    "ed8822" -> "SBC $2288",
    "f155" -> "SBC ($55),Y",
    "f522" -> "SBC $22,X",
    "f92222" -> "SBC $2222,Y",
    "fd1234" -> "SBC $3412,X",

    // SEC
    "38" -> "SEC",

    // SED
    "f8" -> "SED",

    // SEI
    "78" -> "SEI",

    // STA
    "8155" -> "STA ($55,X)",
    "85aa" -> "STA $AA",
    "8daabb" -> "STA $BBAA",
    "9133" -> "STA ($33),Y",
    "95bb" -> "STA $BB,X",
    "99bbaa" -> "STA $AABB,Y",
    "9deeff" -> "STA $FFEE,X",

    // STX
    "8622" -> "STX $22",
    "8E1234" -> "STX $3412",
    "96bb" -> "STX $BB,Y",

    // STY
    "8422" -> "STY $22",
    "8C1234" -> "STY $3412",
    "94bb" -> "STY $BB,X",

    // TAX
    "AA" -> "TAX",

    // TAY
    "A8" -> "TAY",

    // TSX
    "BA" -> "TSX",

    // TXA
    "8a" -> "TXA",

    // TXS
    "9a" -> "TXS",

    // TYA
    "98" -> "TYA"
  ) foreach { (input, expected) =>
    val inputBytes = ByteVector.fromHex(input.toUpperCase()).get.toArray

    test(s"$expected disassembly") {
      val state = nes.NES.Configs.Mapper0.State(Formats.V1.Mirroring.Horizontal, 0, 0, 0)
      val _ = inputBytes.copyToArray(state.internalRam)

      val (actual, inc) = nes.NES.Configs.Mapper0.disasmNext(state)(0.toU16)
      assertEquals(actual, expected)
    }
  }
}

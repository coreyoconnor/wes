package wes
package rom
package nes

import java.nio.file.Paths

class FormatsSpec extends Suite {
  test("format v1") {
    val rom = FileIO.Live.dataForPath("../../reference/nestest.nes")
    val Right(v1: Formats.V1) = Formats.read(rom): @unchecked

    assertEquals(v1.prgSize, 1)
    assertEquals(v1.chrSize, 1)
    assertEquals(v1.mapperId, 0)
    assertEquals(v1.hasTrainer, false)
    assertEquals(v1.hasSRAM, false)
    assertEquals(v1.mirroring, Formats.V1.Mirroring.Horizontal)
    assertEquals(v1.prgRoms(0).size.toInt, wes.nes.NES.prgBankSize)
    assertEquals(v1.chrRoms(0).size.toInt, wes.nes.NES.chrBankSize * 2)
  }
}

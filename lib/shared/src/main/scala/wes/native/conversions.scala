package wes.native

import types.*

object conversions {
  extension (b: Byte) {
    // Conversions
    def toNativeWord: NativeWord = NativeWord.s(b)
    def toU8: U8 = U8(b)
    def toS8: S8 = S8(b)
    def toU16: U16 = U16(b)
    def toS16: S16 = S16(b)

    // Arithmetic
    def dec: Byte = (b - 1).toByte
    def inc: Byte = (b + 1).toByte

    // Predicates
    def test(bits: Int): Boolean = (b & bits) == bits
    def testBit(bit: Int): Boolean = (b & (1 << bit)) != 0
  }

  extension (w: Short) {
    // Conversions
    def toNativeWord: NativeWord = w.toInt
    def toU8: U8 = U8(w)
    def toS8: S8 = S8(w)
    def toU16: U16 = U16(w)
    def toS16: S16 = S16(w)

    // Arithmetic
    def dec: Short = (w - 1).toShort
    def inc: Short = (w + 1).toShort

    // Predicates
    def test(bits: Int): Boolean = (w & bits) == bits
    def testBit(bit: Int): Boolean = (w & (1 << bit)) != 0
  }

  extension (w: Int) {
    // Conversions
    def toNativeWord: NativeWord = w
    def toU8: U8 = U8(w)
    def toS8: S8 = S8(w)
    def toU16: U16 = U16(w)
    def toS16: S16 = S16(w)

    // Arithmetic
    def dec: Int = w - 1
    def inc: Int = w + 1

    // Predicates
    def test(bits: Int): Boolean = (w & bits) == bits
    def testBit(bit: Int): Boolean = (w & (1 << bit)) != 0
  }

  extension (b: Boolean) {
    // Conversions
    def toBit(bit: Int): Int = if (b) then 1 << bit else 0
    def toU8(bit: Int): U8 = U8(toBit(bit))
    def toU16(bit: Int): U16 = U16(toBit(bit))
  }
}

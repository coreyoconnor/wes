package wes.native

import scala.annotation.targetName

/** For all u,v <: operations, for all i,j <: integers, for all p <: projections of [SU][8,16]
  * types, p(i (u) j) == p(i) (u) p(j)
  *
  * additionally, for all c <: constructors of [SU][8,16] types p(c(i)) == i
  *
  * but no expectation for c(p(i)) == i
  */
object types {
  type NativeWord = Int

  object NativeWord {
    def u(v: Byte): NativeWord = java.lang.Byte.toUnsignedInt(v)
    def u(v: Short): NativeWord = java.lang.Short.toUnsignedInt(v)
    def u(v: Int): NativeWord = v

    def s(v: Byte): NativeWord = v.toInt
    def s(v: Short): NativeWord = v.toInt
    def s(v: Int): NativeWord = v
  }

  object U8 {
    def apply(v: Byte): U8 = NativeWord.u(v)
    def apply(v: Short): U8 = U8(v.toByte)
    def apply(v: Int): U8 = U8(v.toByte)
  }

  opaque type U8 = NativeWord

  extension (v: U8) {
    // Conversions
    @targetName("toByteU8")
    def toByte: Byte = v.toByte

    @targetName("toIntU8")
    def toInt: Int = v

    @targetName("toU16U8")
    def toU16: U16 = U16(v.toByte)

    @targetName("toS16U8")
    def toS16: S16 = S16(v.toByte)

    @targetName("toNativeWordU8")
    def toNativeWord: NativeWord = v

    @targetName("toHexU8")
    def toHex: String = f"$v%02X"

    // Arithmetic
    @targetName("plusU8U8")
    def +(rhs: U8): U8 = (v + rhs) & 0xff

    @targetName("plusIntU8")
    def +(rhs: Int): U8 = (v + rhs) & 0xff

    @targetName("minusU8U8")
    def -(rhs: U8): U8 = (v - rhs) & 0xff

    @targetName("minusIntU8")
    def -(rhs: Int): U8 = (v - rhs) & 0xff

    @targetName("decU8")
    def dec: U8 = U8(v - 1)

    @targetName("incU8")
    def inc: U8 = U8(v + 1)

    // Bitwise
    @targetName("andU8U8")
    def &(rhs: U8): U8 = v & rhs

    @targetName("andIntU8")
    def &(rhs: Int): U8 = v & rhs

    @targetName("orU8U8")
    def |(rhs: U8): U8 = v | rhs

    @targetName("orIntU8")
    def |(rhs: Int): U8 = (v | rhs) & 0xff

    @targetName("leftShiftU8")
    def <<(count: Int): U8 = (v << count) & 0xff

    @targetName("rightShiftU8")
    def >>(count: Int): U8 = v >>> count

    @targetName("xorU8")
    def ^(rhs: U8): U8 = v ^ rhs

    @targetName("negateU8")
    def unary_~ : U8 = ~v & 0xff

    // Predicates
    @targetName("testU8")
    def test(bits: Int): Boolean = (v & bits) == bits

    @targetName("testBitU8")
    def testBit(bit: Int): Boolean = (v & (1 << bit)) != 0

    @targetName("isZeroU8")
    def isZero: Boolean = (v & 0xff) == 0

    @targetName("isNegativeU8")
    def isNegative: Boolean = (v & 0x80) != 0

    @targetName("gteU8")
    def >=(rhs: U8): Boolean = v >= rhs
  }

  object S8 {
    def apply(v: Byte): S8 = NativeWord.s(v)
    def apply(v: Short): S8 = S8(v.toByte)
    def apply(v: Int): S8 = S8(v.toByte)
  }

  opaque type S8 = NativeWord

  extension (v: S8) {
    // Conversions
    @targetName("toByteS8")
    def toByte: Byte = v.toByte

    @targetName("toIntS8")
    def toInt: Int = v.toByte.toInt

    @targetName("toNativeWordS8")
    def toNativeWord: NativeWord = v

    @targetName("toU16S8")
    def toU16: U16 = U16(v.toByte)

    @targetName("toS16S8")
    def toS16: S16 = U16(v.toByte)

    // Arithmetic
    @targetName("decS8")
    def dec: S8 = (v - 1) & 0xff

    @targetName("incS8")
    def inc: S8 = (v + 1) & 0xff

    // Bitwise
    @targetName("leftShiftS8")
    def <<(count: Int): S8 = (v << count) & 0xff

    // Predicates
    @targetName("testS8")
    def test(bits: Int): Boolean = (v & bits) == bits

    @targetName("testBitS8")
    def testBit(bit: Int): Boolean = (v & (1 << bit)) != 0
  }

  object U16 {
    def apply(v: Byte): U16 = NativeWord.u(v)
    def apply(v: Short): U16 = NativeWord.u(v)
    def apply(v: Int): U16 = U16(v.toShort)
  }

  opaque type U16 = NativeWord

  extension (v: U16) {
    // Conversions
    @targetName("byte0U16")
    def byte0: U8 = v

    @targetName("byte1U16")
    def byte1: U8 = v >>> 8

    @targetName("toU8U16")
    def toU8: U8 = U8(v.toByte)

    @targetName("toIntU16")
    def toInt: Int = v

    @targetName("toNativeWordU16")
    def toNativeWord: NativeWord = v

    @targetName("toHexU16")
    def toHex: String = f"$v%04X"

    // Arithmetic
    @targetName("decU16")
    def dec: U16 = (v - 1) & 0xffff

    @targetName("incU16")
    def inc: U16 = (v + 1) & 0xffff

    @targetName("plusU16U16")
    def +(rhs: U16): U16 = (v + rhs) & 0xffff

    @targetName("plusIntU16")
    def +(rhs: Int): U16 = (v + rhs) & 0xffff

    @targetName("minusU16U16")
    def -(rhs: U16): U16 = (v - rhs) & 0xffff

    @targetName("minusIntU16")
    def -(rhs: Int): U16 = (v - rhs) & 0xffff

    // Bitwise
    @targetName("leftShiftU16")
    def <<(count: Int): U16 = (v << count) & 0xffff

    @targetName("rightShiftU16")
    def >>(count: Int): U16 = v >>> count

    @targetName("andU16U16")
    def &(rhs: U16): U16 = v & rhs

    @targetName("andIntU16")
    def &(rhs: Int): U16 = v & rhs

    @targetName("orU16U16")
    def |(rhs: U16): U16 = v | rhs

    @targetName("orIntU16")
    def |(rhs: Int): U16 = (v | rhs) & 0xffff

    @targetName("negateU16")
    def unary_~ : U16 = ~v & 0xffff

    @targetName("sliceU16")
    def slice(size: Int, low: Int): U16 = (v >> low) & ((1 << size) - 1)

    // Predicates
    @targetName("testU16")
    def test(bits: Int): Boolean = (v & bits) == bits

    @targetName("testBitU16")
    def testBit(bit: Int): Boolean = (v & (1 << bit)) != 0

    @targetName("ltU16U16")
    def <(rhs: U16): Boolean = v < rhs
  }

  object S16 {
    def apply(v: Byte): S16 = NativeWord.s(v)
    def apply(v: Short): S16 = NativeWord.s(v)
    def apply(v: Int): S16 = S16(v.toShort)
  }

  opaque type S16 = NativeWord

  extension (v: S16) {
    // Conversions
    @targetName("toIntS16")
    def toInt: Int = v.toShort.toInt

    @targetName("toU8S16")
    def toU8: U8 = U8(v.toByte)

    @targetName("toU16S16")
    def toU16: U16 = U16(v.toByte)

    // Arithmetic
    @targetName("decS16")
    def dec: S16 = (v - 1) & 0xffff

    @targetName("incS16")
    def inc: S16 = (v + 1) & 0xffff

    // Bitwise
    @targetName("leftShiftS16")
    def <<(count: Int): S16 = (v << count) & 0xffff

    @targetName("orU16S16")
    def |(rhs: S16): S16 = v | rhs

    @targetName("orIntS16")
    def |(rhs: Int): S16 = (v | rhs) & 0xffff

    // Predicates
    @targetName("testS16")
    def test(bits: Int): Boolean = (v & bits) == bits

    @targetName("testBitS16")
    def testBit(bit: Int): Boolean = (v & (1 << bit)) != 0
  }
}

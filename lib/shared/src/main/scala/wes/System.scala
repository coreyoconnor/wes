package wes

import wes.native.conversions.*
import wes.native.types.*

trait ReadBus[Addr, -S] {
  def readU8(addr: Addr, state: S): U8
  def readS8(addr: Addr, state: S): S8
  def readU16(addr: Addr, state: S): U16
  def readS16(addr: Addr, state: S): S16
}

trait WriteBus[Addr, -S] {
  def writeU8(addr: Addr, value: U8, state: S): U8
  def writeS8(addr: Addr, value: S8, state: S): S8
  def writeU16(addr: Addr, value: U16, state: S): U16
  def writeS16(addr: Addr, value: S16, state: S): S16
}

trait ByteReadBus[Addr, -S] extends ReadBus[Addr, S] {
  def readByte(addr: Addr, state: S): Byte

  def readU8(addr: Addr, state: S): U8 = readByte(addr, state).toU8
  def readS8(addr: Addr, state: S): S8 = readByte(addr, state).toS8
}

trait ByteWriteBus[Addr, -S] extends WriteBus[Addr, S] {
  def writeByte(addr: Addr, value: Byte, state: S): Byte

  def writeU8(addr: Addr, value: U8, state: S): U8 =
    writeByte(addr, value.toByte, state).toU8
  def writeS8(addr: Addr, value: S8, state: S): S8 =
    writeByte(addr, value.toByte, state).toS8
}

final type RWBus[Addr, -S] = ReadBus[Addr, S] & WriteBus[Addr, S]

trait System { self: Singleton =>
  type Addr
  type PC

  final type SystemReadBus[-S] = ReadBus[Addr, S]
  final type SystemWriteBus[-S] = WriteBus[Addr, S]
  final type SystemRWBus[-S] = SystemReadBus[S] & SystemWriteBus[S]
}

trait LE16System extends System with WithAddrRep[U16] { self: Singleton =>
  final type PC = Addr

  trait ReadLE16From8[-S] extends ReadBus[Addr, S] {
    def readU16(addr: Addr, state: S): U16 =
      readU8(addr, state).toU16 | (readU8(addr.inc, state).toU16 << 8)

    def readS16(addr: Addr, state: S): S16 =
      readU8(addr, state).toS16 | (readS8(addr.inc, state).toS16 << 8)
  }

  trait WriteLE16By8[-S] extends WriteBus[Addr, S] {
    def writeU16(addr: Addr, value: U16, state: S): U16 =
      writeU8(addr, value.toU8, state).toU16 | (writeU8(
        addr.inc,
        ((value & 0xff00) >> 8).toU8,
        state
      ).toU16 << 8)

    def writeS16(addr: Addr, value: S16, state: S): S16 =
      writeU8(addr, value.toU8, state).toS16 | (writeU8(
        addr.inc,
        ((value.toU16 & 0xff00) >> 8).toU8,
        state
      ).toS16 << 8)
  }
}

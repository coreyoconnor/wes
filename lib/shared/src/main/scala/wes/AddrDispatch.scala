/* experiments in building a dispatch on address bounds that supports predicate pushup in call graph.
 *
package wes

import scala.quoted.*

object AddrDispatch {
  case class Exact[Addr, F[_], A](addr: Addr, rhs: F[A]) extends AddrDispatch[Addr, F, A]
  case class OnRange[Addr, F[_], A](low: Addr, high: Addr, rhs: F[A]) extends AddrDispatch[Addr, F, A]
  case class OnMask[Addr, F[_], A](mask: Addr)(rhs: NativeWord => F[A]) extends AddrDispatch[Addr, F, A]
}

trait AddrDispatch[Addr, F[_]] {
  def exact[A](addr: Addr)(rhs: A): F[A]
  def onRange[A](low: Addr, high: Addr)(rhs: F[A]): F[A]
  def onMask[A](mask: NativeWord)(rhs: F[A]): F[A]
}
 */
/*
object Dispatch {
  def mask(mask: NativeWord)(rhs: PartialFunction[Addr, Dispatch]): Dispatch = Mask(mask, List(rhs))
  def out: Dispatch = Out

  case class Mask(mask: NativeWord, rhs: List[PartialFunction[Addr, Dispatch]]) extends Dispatch {
    def apply[F[_], A](f: Addr => F[A]): Addr => F[A] = { addr =>
      (addr.toNativeWord & mask)
      ???
    }
  }
  case object Out extends Dispatch {
    def apply[F[_], A](f: Addr => F[A]): Addr => F[A] = { addr =>
      ???
    }
  }
}

sealed trait Dispatch {
  def apply[F[_], A](f: Addr => F[A]): Addr => F[A]
}
 */
/*
object Dispatch {
  case object Cont
  val cont = Cont

  sealed trait Op
  case class OnMask(mask: NativeWord) extends Op

  def deriveMask[T: Type](
    mask: Expr[NativeWord],
    withAddr: Expr[NativeWord => Cont.type]
  )(using Quotes): Expr[Op] = {
    import quotes.reflect.*
    val maskValue = mask.valueOrAbort
    println(withAddr.asTerm.show(using Printer.TreeStructure))
    println(withAddr.asTerm.show(using Printer.TreeCode))

    withAddr match {
      case '{ ( (addr: Any) => ${body}: Any ) } => println(body)
    }
    ???
  }
}

class Dispatch[T] {
  inline def mask(inline m: NativeWord)(inline f: NativeWord => Dispatch.Cont.type): Dispatch.Op = ${
    Dispatch.deriveMask[T]('m, 'f)
  }
}
 */

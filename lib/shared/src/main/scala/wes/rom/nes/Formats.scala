package wes
package rom
package nes

import wes.native.types.*
import wes.native.conversions.*

object Formats {
  val iNESTag = hex"4E45531A"

  def read(data: BitVector): Either[String, V1 | V2] = {
    val tag = data.bytes.take(4)

    if (tag != iNESTag) Left("not NES rom")
    else {
      val (header, remaining) = data.bytes.splitAt(16)

      if ((header(7) & 0x0c) == 0x08) V2.read(header, remaining)
      else V1.read(header, remaining)
    }
  }

  object V1 {
    val chrRomSize = 0x2000

    enum Mirroring {
      case Horizontal, Vertical
    }

    def read(header: ByteVector, remaining: ByteVector): Either[String, V1] = {
      val prgSize = header(4).toU8
      val chrSize = header(5).toU8
      val flags0 = header(6)
      val flags1 = header(7)

      val mapperId = (0xf0 & flags1) | ((0xf0 & flags0) >> 4)
      val hasTrainer = (flags0 & 0x04) != 0
      assert(!hasTrainer, "iNES trainer not handled")

      val hasSRAM = (flags0 & 0x02) != 0
      val mirroring =
        if ((flags0 & 0x01) == 1) V1.Mirroring.Vertical
        else V1.Mirroring.Horizontal

      val ramPages = header(8) match {
        case 0 => 1
        case n => n
      }
      val prgRomStart = remaining
      val (chrRomStart, prgRoms) =
        (0 until prgSize.toNativeWord).foldLeft(
          (prgRomStart, List.empty[ByteVector])
        ) { case ((romStart, out), _) =>
          (
            romStart.drop(wes.nes.NES.prgBankSize),
            out :+ romStart.take(wes.nes.NES.prgBankSize)
          )
        }

      val (_, chrRoms) =
        (0 until chrSize.toNativeWord).foldLeft(
          (chrRomStart, List.empty[ByteVector])
        ) { case ((romStart, out), _) =>
          (
            romStart.drop(chrRomSize),
            out :+ romStart.take(chrRomSize)
          )
        }

      Right {
        V1(
          mapperId,
          hasTrainer,
          hasSRAM,
          mirroring,
          ramPages,
          prgRoms,
          chrRoms
        )
      }
    }
  }

  case class V1(
      mapperId: Int,
      hasTrainer: Boolean,
      hasSRAM: Boolean,
      mirroring: V1.Mirroring,
      ramPages: Int,
      prgRoms: Seq[ByteVector],
      chrRoms: Seq[ByteVector]
  ) {
    val prgSize: Int = prgRoms.size
    val chrSize: Int = chrRoms.size
    val usesCHRRAM: Boolean = chrSize == 0

    override def toString(): String =
      s"V1(mapperId = $mapperId, mirroring = $mirroring, ramPages = $ramPages, prgSize = $prgSize, chrSize = $chrSize)"
  }

  object V2 {
    def read(header: ByteVector, remaining: ByteVector): Either[String, V2] =
      Left("V2 format not implemented")
  }

  case class V2()
}

package wes

final case class RGB24(r: Byte, g: Byte, b: Byte)

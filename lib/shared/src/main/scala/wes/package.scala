package wes

import wes.native.types.*
import wes.native.conversions.*

export scodec.bits.{BitVector, ByteVector, hex}
export util.{Failure, Success, Try}

type Cycles = Int
type Offset = Int

type Inst = String
type Listing = Seq[Inst]

def parseHexU8(str: String): U8 =
  Integer.parseInt(str.trim.take(2), 16).toByte.toU8
def parseHexU16(str: String): U16 =
  Integer.parseInt(str.trim.take(4), 16).toShort.toU16

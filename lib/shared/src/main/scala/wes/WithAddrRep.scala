package wes

import wes.native.conversions.*
import wes.native.types.*
import scala.annotation.targetName

transparent trait WithAddrRep[T] {
  final type Addr = T

  extension (w: U8)(using eq: U16 =:= Addr) {
    @targetName("toAddrU16U8")
    def toAddr: Addr = eq(w.toU16)
  }

  extension (w: U16)(using eq: U16 =:= Addr) {
    @targetName("toAddrU16U16")
    def toAddr: Addr = eq(w)
  }

  extension (w: Int)(using eq: U16 =:= Addr) {
    @targetName("toAddrU16Int")
    def toAddr: Addr = eq(U16(w))
  }
}

package wes

import wes.native.conversions.*
import wes.native.types.*

object CPU6502 extends WithAddrRep[U16] {
  final type PC = Addr

  extension (w: Int) {
    def toPC: PC = U16(w)
  }

  extension (w: Byte) {
    def toPC: PC = U16(w)
  }

  trait SystemState {
    final var cpu = new State
  }

  class State {
    final var registers = new Registers
    final var flags: Flags = new Flags
    // fake the expected initial cycle count. 7 cycles before
    // start execution at reset vector
    // https://www.pagetable.com/?p=410
    final var cycleCount: Long = 7L
  }

  class Registers {
    final var pc: PC = 0.toU16
    final var sp: U8 = 0xfd.toU8
    final var acc: U8 = 0.toU8
    final var x: U8 = 0.toU8
    final var y: U8 = 0.toU8

    def incPC: Unit = {
      pc = pc.inc
    }

    def incPC2: Unit = {
      pc = pc.inc.inc
    }

    def incPC3: Unit = {
      pc = pc.inc.inc.inc
    }

    def decSP: Unit = {
      sp = sp.dec
    }

    def decSP2: Unit = {
      sp = sp.dec.dec
    }

    def incSP: Unit = {
      sp = sp.inc
    }

    def incSP2: Unit = {
      sp = sp.inc.inc
    }

    def stackAddr(offset: U8): Addr = offset.toU16 | 0x100

    def stackAddr: Addr = stackAddr(sp)
  }

  val nmiVector: Addr = 0xfffa.toAddr
  val resetVector: Addr = 0xfffc.toAddr
  val brkVector: Addr = 0xfffe.toAddr
  val irqVector: Addr = brkVector

  object Flags {
    val brkBit: U8 = 1.toU8 << 4
  }

  class Flags {
    var carry: Boolean = false
    var zero: Boolean = false
    var interruptDisabled: Boolean = true
    var decimal: Boolean = false
    var break: Boolean = false
    var overflow: Boolean = false
    var negative: Boolean = false

    def registerValue: U8 = {
      carry.toU8(0) | zero.toU8(1) | interruptDisabled.toU8(2) | decimal.toU8(
        3
      ) | {
        break.toU8(4) | (1 << 5).toU8 | overflow.toU8(6) | negative.toU8(7)
      }
    }

    def setValue(value: U8): Flags = {
      carry = value.testBit(0)
      zero = value.testBit(1)
      interruptDisabled = value.testBit(2)
      decimal = value.testBit(3)
      // can only be set by BRK
      // break = (value & (1 << 4)) != 0
      overflow = value.testBit(6)
      negative = value.testBit(7)

      this
    }

    def clearCarry: Flags = { carry = false; this }

    def setCarry: Flags = { carry = true; this }

    def clearZero: Flags = { zero = false; this }

    def setZero: Flags = { zero = true; this }

    def clearInterruptDisabled: Flags = { interruptDisabled = false; this }

    def setInterruptDisabled: Flags = { interruptDisabled = true; this }

    def clearDecimal: Flags = { decimal = false; this }

    def setDecimal: Flags = { decimal = true; this }

    def clearBreak: Flags = { break = false; this }

    def setBreak: Flags = { break = true; this }

    def clearOverflow: Flags = { overflow = false; this }

    def setOverflow: Flags = { overflow = true; this }

    def clearNegative: Flags = { negative = false; this }

    def setNegative: Flags = { negative = true; this }

    def updateZero(v: U8): Flags = { zero = v.isZero; this }

    def updateNegative(v: U8): Flags = { negative = v.isNegative; this }
  }

  def pushStackU8[S <: SystemState](
      bus: RWBus[Addr, S],
      state: S,
      value: U8
  ): U8 = {
    val _ = bus.writeU8(state.cpu.registers.stackAddr, value, state)
    state.cpu.registers.decSP
    value
  }

  def pushStackU16[S <: SystemState](
      bus: RWBus[Addr, S],
      state: S,
      value: U16
  ): U16 = {
    val _ = bus.writeU8(state.cpu.registers.stackAddr, value.byte1, state)
    state.cpu.registers.decSP
    val _ = bus.writeU8(state.cpu.registers.stackAddr, value.byte0, state)
    state.cpu.registers.decSP
    value
  }

  def peekStackU8[S <: SystemState](
      bus: RWBus[Addr, S],
      state: S
  ): U8 = {
    val sp = state.cpu.registers.sp.inc
    bus.readU8(state.cpu.registers.stackAddr(sp), state)
  }

  def popStackU8[S <: SystemState](
      bus: RWBus[Addr, S],
      state: S
  ): U8 = {
    state.cpu.registers.incSP
    bus.readU8(state.cpu.registers.stackAddr, state)
  }

  def peekStackU16[S <: SystemState](
      bus: RWBus[Addr, S],
      state: S
  ): U16 = {
    val sp0 = state.cpu.registers.sp.inc
    val sp1 = sp0.inc
    val lowByte = bus.readU8(state.cpu.registers.stackAddr(sp0), state)
    val highByte = bus.readU8(state.cpu.registers.stackAddr(sp1), state)
    lowByte.toU16 | (highByte.toU16 << 8)
  }

  def popStackU16[S <: SystemState](
      bus: RWBus[Addr, S],
      state: S
  ): U16 = {
    state.cpu.registers.incSP
    val lowByte = bus.readU8(state.cpu.registers.stackAddr, state)
    state.cpu.registers.incSP
    lowByte.toU16 | (bus
      .readU8(state.cpu.registers.stackAddr, state)
      .toU16 << 8)
  }

  def readIndirectU16[S <: SystemState](
      bus: RWBus[Addr, S],
      state: S,
      lowAddrPtr: U16
  ): U16 = {
    val addrLow = bus.readU8(lowAddrPtr, state)
    val highAddrPtr = (lowAddrPtr & 0xff00) | (lowAddrPtr.inc & 0x00ff)
    val addrHigh = bus.readU8(highAddrPtr, state)
    (addrHigh.toU16 << 8) | addrLow.toU16
  }

  def performInterrupt[S <: SystemState](
      bus: RWBus[Addr, S],
      state: S,
      vector: Addr
  ): Unit = {
    pushStackU16(bus, state, state.cpu.registers.pc)
    pushStackU8(bus, state, state.cpu.flags.registerValue)
    state.cpu.registers.pc = bus.readU16(vector, state)
    state.cpu.flags.setInterruptDisabled
    ()
  }

  object ADC extends Decl {
    object Modes
        extends ImmediateMode[0x69],
          ZeroPageMode[0x65],
          ZeroPageXMode[0x75],
          AbsoluteMode[0x6d],
          AbsoluteXMode[0x7d],
          AbsoluteYMode[0x79],
          IndirectXMode[0x61],
          IndirectYMode[0x71] {

      def adc[S <: SystemState](
          state: S,
          m: U8
      ): Cycles = {
        val acc = state.cpu.registers.acc
        val valueU16 = acc.toU16 + m.toU16 + state.cpu.flags.carry.toU16(0)
        val value = valueU16.toU8

        state.cpu.flags.carry = valueU16.testBit(8)
        state.cpu.flags.overflow = (acc ^ ~m).test(0x80) && (acc ^ value).test(0x80)
        state.cpu.flags.negative = value.test(0x80)
        state.cpu.registers.acc = value
        state.cpu.flags.zero = value.isZero
        2
      }

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = adc(state, bus.readU8(addr, state)) + 1

      def zeroPageX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = adc(state, bus.readU8(addr, state)) + 2

      def absoluteY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = adc(state, bus.readU8(addr, state)) + 2

      def absoluteX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = adc(state, bus.readU8(addr, state)) + 2

      def indirectY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = adc(state, bus.readU8(addr, state)) + 3

      def indirectX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = adc(state, bus.readU8(addr, state)) + 4

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = adc(state, bus.readU8(addr, state)) + 2

      def immediate[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          imm: U8
      ): Cycles = adc(state, imm)
    }
  }

  object AND extends Decl {
    object Modes
        extends ImmediateMode[0x29],
          ZeroPageMode[0x25],
          ZeroPageXMode[0x35],
          AbsoluteMode[0x2d],
          AbsoluteXMode[0x3d],
          AbsoluteYMode[0x39],
          IndirectXMode[0x21],
          IndirectYMode[0x31] {

      def and[S <: SystemState](
          state: S,
          m: U8
      ): Cycles = {
        val value = state.cpu.registers.acc & m
        val _ = state.cpu.flags.updateZero(value).updateNegative(value)
        state.cpu.registers.acc = value
        2
      }

      def zeroPageX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = and(state, bus.readU8(addr, state)) + 2

      def absoluteY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = and(state, bus.readU8(addr, state)) + 2

      def absoluteX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = and(state, bus.readU8(addr, state)) + 2

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = and(state, bus.readU8(addr, state)) + 1

      def indirectY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = and(state, bus.readU8(addr, state)) + 3

      def indirectX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = and(state, bus.readU8(addr, state)) + 4

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = and(state, bus.readU8(addr, state)) + 2

      def immediate[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          imm: U8
      ): Cycles = and(state, imm)
    }
  }

  object ASL extends Decl {
    object Modes
        extends AccumulatorMode[0x0a],
          ZeroPageMode[0x06],
          ZeroPageXMode[0x16],
          AbsoluteMode[0x0e],
          AbsoluteXMode[0x1e] {

      def asl[S <: SystemState](
          state: S,
          m: U8
      ): U8 = {
        state.cpu.flags.carry = m.test(0x80)
        val value = m << 1
        val _ = state.cpu.flags.updateZero(value).updateNegative(value)
        value
      }

      def zeroPageX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        val _ = bus.writeU8(addr, asl(state, bus.readU8(addr, state)), state)
        6
      }

      def accumulator[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S
      ): Cycles = {
        state.cpu.registers.acc = asl(state, state.cpu.registers.acc)
        2
      }

      def absoluteX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        val _ = bus.writeU8(addr, asl(state, bus.readU8(addr, state)), state)
        6
      }

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        val _ = bus.writeU8(addr, asl(state, bus.readU8(addr, state)), state)
        5
      }

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        val _ = bus.writeU8(addr, asl(state, bus.readU8(addr, state)), state)
        6
      }
    }
  }

  object BCC extends Decl {
    object Modes extends RelativeMode[0x90] {
      def relative[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          offset: U8
      ): Cycles = {
        if (!state.cpu.flags.carry) {
          state.cpu.registers.pc = state.cpu.registers.pc + offset.toByte
          3
        } else {
          2
        }
      }
    }
  }

  object BCS extends Decl {
    object Modes extends RelativeMode[0xb0] {
      def relative[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          offset: U8
      ): Cycles = {
        if (state.cpu.flags.carry) {
          state.cpu.registers.pc = state.cpu.registers.pc + offset.toByte
          3
        } else {
          2
        }
      }
    }
  }

  object BEQ extends Decl {
    object Modes extends RelativeMode[0xf0] {
      def relative[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          offset: U8
      ): Cycles = {
        if (state.cpu.flags.zero) {
          state.cpu.registers.pc = state.cpu.registers.pc + offset.toByte
          3
        } else {
          2
        }
      }
    }
  }

  object BIT extends Decl {
    object Modes extends ZeroPageMode[0x24], AbsoluteMode[0x2c] {
      def bit[S <: SystemState](
          state: S,
          value: U8
      ): Cycles = {
        val maskedAcc = state.cpu.registers.acc & value

        state.cpu.flags
          .updateZero(maskedAcc)
          .updateNegative(value)
          .overflow = value.testBit(6)

        3
      }

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = bit(state, bus.readU8(addr, state))

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = bit(state, bus.readU8(addr, state)) + 1
    }
  }

  object BMI extends Decl {
    object Modes extends RelativeMode[0x30] {
      def relative[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          offset: U8
      ): Cycles = {
        if (state.cpu.flags.negative) {
          state.cpu.registers.pc = state.cpu.registers.pc + offset.toByte
          3
        } else {
          2
        }
      }
    }
  }

  object BNE extends Decl {
    object Modes extends RelativeMode[0xd0] {
      def relative[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          offset: U8
      ): Cycles = {
        if (!state.cpu.flags.zero) {
          state.cpu.registers.pc = state.cpu.registers.pc + offset.toByte
          3
        } else {
          2
        }
      }
    }
  }

  object BPL extends Decl {
    object Modes extends RelativeMode[0x10] {
      def relative[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          offset: U8
      ): Cycles = {
        if (!state.cpu.flags.negative) {
          state.cpu.registers.pc = state.cpu.registers.pc + offset.toByte
          3
        } else {
          2
        }
      }
    }
  }

  object BRK extends Decl {
    object Modes extends ImpliedMode[0x00] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Int = {
        val _ = state.cpu.flags.setBreak
        state.cpu.registers.pc = state.cpu.registers.pc.inc
        performInterrupt(bus, state, brkVector)
        7
      }
    }
  }

  object BVC extends Decl {
    object Modes extends RelativeMode[0x50] {
      def relative[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          offset: U8
      ): Cycles = {
        if (!state.cpu.flags.overflow) {
          state.cpu.registers.pc = state.cpu.registers.pc + offset.toByte
          3
        } else {
          2
        }
      }
    }
  }

  object BVS extends Decl {
    object Modes extends RelativeMode[0x70] {
      def relative[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          offset: U8
      ): Cycles = {
        if (state.cpu.flags.overflow) {
          state.cpu.registers.pc = state.cpu.registers.pc + offset.toByte
          3
        } else {
          2
        }
      }
    }
  }

  object CLC extends Decl {
    object Modes extends ImpliedMode[0x18] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        val _ = state.cpu.flags.clearCarry
        2
      }
    }
  }

  object CLD extends Decl {
    object Modes extends ImpliedMode[0xd8] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        val _ = state.cpu.flags.clearDecimal
        2
      }
    }
  }

  object CLI extends Decl {
    object Modes extends ImpliedMode[0x58] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        val _ = state.cpu.flags.clearInterruptDisabled
        2
      }
    }
  }

  object CLV extends Decl {
    object Modes extends ImpliedMode[0xb8] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        val _ = state.cpu.flags.clearOverflow
        2
      }
    }
  }

  def compare[S <: SystemState](
      state: S,
      u: U8,
      v: U8
  ): Cycles = {
    val value = u - v
    state.cpu.flags.zero = value.isZero
    state.cpu.flags.negative = value.test(0x80)
    state.cpu.flags.carry = u >= v
    2
  }

  object CMP extends Decl {
    object Modes
        extends ImmediateMode[0xc9],
          ZeroPageMode[0xc5],
          ZeroPageXMode[0xd5],
          AbsoluteMode[0xcd],
          AbsoluteXMode[0xdd],
          AbsoluteYMode[0xd9],
          IndirectXMode[0xc1],
          IndirectYMode[0xd1] {

      def cmp[S <: SystemState](state: S, value: U8): Cycles =
        compare(state, state.cpu.registers.acc, value)

      def zeroPageX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = cmp(state, bus.readU8(addr, state)) + 2

      def absoluteY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = cmp(state, bus.readU8(addr, state)) + 2

      def absoluteX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = cmp(state, bus.readU8(addr, state)) + 2

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = cmp(state, bus.readU8(addr, state)) + 1

      def indirectY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = cmp(state, bus.readU8(addr, state)) + 3

      def indirectX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = cmp(state, bus.readU8(addr, state)) + 4

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = cmp(state, bus.readU8(addr, state)) + 2

      def immediate[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          imm: U8
      ): Cycles = cmp(state, imm)
    }
  }

  object CPX extends Decl {
    object Modes extends ImmediateMode[0xe0], ZeroPageMode[0xe4], AbsoluteMode[0xec] {

      def cpx[S <: SystemState](
          state: S,
          m: U8
      ): Cycles = compare(state, state.cpu.registers.x, m)

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = cpx(state, bus.readU8(addr, state)) + 1

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = cpx(state, bus.readU8(addr, state)) + 2

      def immediate[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          imm: U8
      ): Cycles = cpx(state, imm)
    }
  }

  object CPY extends Decl {
    object Modes extends ImmediateMode[0xc0], ZeroPageMode[0xc4], AbsoluteMode[0xcc] {

      def cpy[S <: SystemState](
          state: S,
          m: U8
      ): Cycles = compare(state, state.cpu.registers.y, m)

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = cpy(state, bus.readU8(addr, state)) + 1

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = cpy(state, bus.readU8(addr, state)) + 2

      def immediate[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          imm: U8
      ): Cycles = cpy(state, imm)
    }
  }

  object DEC extends Decl {
    object Modes
        extends ZeroPageMode[0xc6],
          ZeroPageXMode[0xd6],
          AbsoluteMode[0xce],
          AbsoluteXMode[0xde] {

      def dec[S <: SystemState](
          state: S,
          m: U8
      ): U8 = {
        val value = m.dec
        val _ = state.cpu.flags.updateZero(value).updateNegative(value)
        value
      }

      def zeroPageX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        bus.writeU8(addr, dec(state, bus.readU8(addr, state)), state)
        6
      }

      def absoluteX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        val _ = bus.writeU8(addr, dec(state, bus.readU8(addr, state)), state)
        6
      }

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        val _ = bus.writeU8(addr, dec(state, bus.readU8(addr, state)), state)
        5
      }

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        val _ = bus.writeU8(addr, dec(state, bus.readU8(addr, state)), state)
        6
      }
    }
  }

  object DEX extends Decl {
    object Modes extends ImpliedMode[0xca] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        val value = state.cpu.registers.x.dec
        state.cpu.registers.x = value
        val _ = state.cpu.flags.updateZero(value).updateNegative(value)
        2
      }
    }
  }

  object DEY extends Decl {
    object Modes extends ImpliedMode[0x88] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        val value = state.cpu.registers.y.dec
        state.cpu.registers.y = value
        val _ = state.cpu.flags.updateZero(value).updateNegative(value)
        2
      }
    }
  }

  object EOR extends Decl {
    object Modes
        extends ImmediateMode[0x49],
          ZeroPageMode[0x45],
          ZeroPageXMode[0x55],
          AbsoluteMode[0x4d],
          AbsoluteXMode[0x5d],
          AbsoluteYMode[0x59],
          IndirectXMode[0x41],
          IndirectYMode[0x51] {

      def eor[S <: SystemState](
          state: S,
          m: U8
      ): Cycles = {
        val value = m ^ state.cpu.registers.acc
        state.cpu.registers.acc = value
        val _ = state.cpu.flags.updateNegative(value).updateZero(value)
        2
      }

      def zeroPageX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = eor(state, bus.readU8(addr, state)) + 2

      def absoluteY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = eor(state, bus.readU8(addr, state)) + 2

      def absoluteX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = eor(state, bus.readU8(addr, state)) + 2

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = eor(state, bus.readU8(addr, state)) + 1

      def indirectY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = eor(state, bus.readU8(addr, state)) + 3

      def indirectX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = eor(state, bus.readU8(addr, state)) + 4

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = eor(state, bus.readU8(addr, state)) + 2

      def immediate[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          imm: U8
      ): Cycles = {
        eor(state, imm)
      }
    }
  }

  object INC extends Decl {
    object Modes
        extends ZeroPageMode[0xe6],
          ZeroPageXMode[0xf6],
          AbsoluteMode[0xee],
          AbsoluteXMode[0xfe] {

      def inc[S <: SystemState](
          state: S,
          m: U8
      ): U8 = {
        val value = m.inc
        state.cpu.flags.updateZero(value).updateNegative(value)
        value
      }

      def zeroPageX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        val _ = bus.writeU8(addr, inc(state, bus.readU8(addr, state)), state)
        6
      }

      def absoluteX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        val _ = bus.writeU8(addr, inc(state, bus.readU8(addr, state)), state)
        7
      }

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        val _ = bus.writeU8(addr, inc(state, bus.readU8(addr, state)), state)
        5
      }

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        bus.writeU8(addr, inc(state, bus.readU8(addr, state)), state)
        6
      }
    }
  }

  object INX extends Decl {
    object Modes extends ImpliedMode[0xe8] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        val value = state.cpu.registers.x.inc
        state.cpu.registers.x = value
        val _ = state.cpu.flags.updateZero(value).updateNegative(value)
        2
      }
    }
  }

  object INY extends Decl {
    object Modes extends ImpliedMode[0xc8] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        val value = state.cpu.registers.y.inc
        state.cpu.registers.y = value
        val _ = state.cpu.flags.updateZero(value).updateNegative(value)
        2
      }
    }
  }

  object JMP extends Decl {
    object Modes extends AbsoluteMode[0x4c], IndirectMode[0x6c] {
      def indirect[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        state.cpu.registers.pc = addr
        5
      }

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        state.cpu.registers.pc = addr
        3
      }
    }
  }

  object JSR extends Decl {
    object Modes extends AbsoluteMode[0x20] {
      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        val _ = pushStackU16(bus, state, state.cpu.registers.pc - 1)
        state.cpu.registers.pc = addr
        6
      }
    }
  }

  object LDA extends Decl {
    object Modes
        extends AbsoluteMode[0xad],
          AbsoluteXMode[0xbd],
          AbsoluteYMode[0xb9],
          ImmediateMode[0xa9],
          IndirectXMode[0xa1],
          IndirectYMode[0xb1],
          ZeroPageMode[0xa5],
          ZeroPageXMode[0xb5] {

      def lda[S <: SystemState](state: S, value: U8): Cycles = {
        state.cpu.registers.acc = value
        state.cpu.flags.updateNegative(value).updateZero(value)
        2
      }

      def zeroPageX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = lda(state, bus.readU8(addr, state)) + 2

      def absoluteY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = lda(state, bus.readU8(addr, state)) + 2

      def absoluteX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = lda(state, bus.readU8(addr, state)) + 2

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = lda(state, bus.readU8(addr, state)) + 1

      def indirectY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = lda(state, bus.readU8(addr, state)) + 3

      def indirectX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = lda(state, bus.readU8(addr, state)) + 4

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = lda(state, bus.readU8(addr, state)) + 2

      def immediate[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          imm: U8
      ): Cycles = lda(state, imm)
    }
  }

  object LDX extends Decl {
    object Modes
        extends ImmediateMode[0xa2],
          ZeroPageMode[0xa6],
          ZeroPageYMode[0xb6],
          AbsoluteMode[0xae],
          AbsoluteYMode[0xbe] {

      def ldx[S <: SystemState](state: S, value: U8): Cycles = {
        state.cpu.registers.x = value
        state.cpu.flags.updateNegative(value).updateZero(value)
        2
      }

      def absoluteY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = ldx(state, bus.readU8(addr, state)) + 2

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = ldx(state, bus.readU8(addr, state)) + 1

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = ldx(state, bus.readU8(addr, state)) + 2

      def zeroPageY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = ldx(state, bus.readU8(addr, state)) + 2

      def immediate[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          imm: U8
      ): Cycles = ldx(state, imm)
    }
  }

  object LDY extends Decl {
    object Modes
        extends ImmediateMode[0xa0],
          ZeroPageMode[0xa4],
          ZeroPageXMode[0xb4],
          AbsoluteMode[0xac],
          AbsoluteXMode[0xbc] {

      def ldy[S <: SystemState](state: S, value: U8): Cycles = {
        state.cpu.registers.y = value
        state.cpu.flags.updateNegative(value).updateZero(value)
        2
      }

      def zeroPageX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = ldy(state, bus.readU8(addr, state)) + 2

      def absoluteX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = ldy(state, bus.readU8(addr, state)) + 2

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = ldy(state, bus.readU8(addr, state)) + 1

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = ldy(state, bus.readU8(addr, state)) + 2

      def immediate[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          imm: U8
      ): Cycles = ldy(state, imm)
    }
  }

  object LSR extends Decl {
    object Modes
        extends AccumulatorMode[0x4a],
          ZeroPageMode[0x46],
          ZeroPageXMode[0x56],
          AbsoluteMode[0x4e],
          AbsoluteXMode[0x5e] {

      def lsr[S <: SystemState](state: S, m: U8): U8 = {
        state.cpu.flags.carry = m.test(0x01)
        val out = m >> 1
        state.cpu.flags.updateZero(out).clearNegative
        out
      }

      def zeroPageX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        bus.writeU8(addr, lsr(state, bus.readU8(addr, state)), state)
        6
      }

      def accumulator[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S
      ): Cycles = {
        state.cpu.registers.acc = lsr(state, state.cpu.registers.acc)
        2
      }

      def absoluteX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        bus.writeU8(addr, lsr(state, bus.readU8(addr, state)), state)
        6
      }

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        bus.writeU8(addr, lsr(state, bus.readU8(addr, state)), state)
        5
      }

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        bus.writeU8(addr, lsr(state, bus.readU8(addr, state)), state)
        6
      }
    }
  }

  object NOP extends Decl {
    object Modes extends ImpliedMode[0xea] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = 2
    }
  }

  abstract class UnofficialNOP[Op <: Int: ValueOf] extends Decl {
    self: Singleton =>
    object Modes extends ImpliedMode[Op] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = 2
    }
  }

  object NOP1A extends UnofficialNOP[0x1a]

  object NOP3A extends UnofficialNOP[0x3a]

  object NOP5A extends UnofficialNOP[0x5a]

  object NOP7A extends UnofficialNOP[0x7a]

  object NOPDA extends UnofficialNOP[0xda]

  object NOPFA extends UnofficialNOP[0xfa]

  object ORA extends Decl {
    object Modes
        extends AbsoluteMode[0x0d],
          AbsoluteXMode[0x1d],
          AbsoluteYMode[0x19],
          ImmediateMode[0x09],
          IndirectXMode[0x01],
          IndirectYMode[0x11],
          ZeroPageMode[0x05],
          ZeroPageXMode[0x15] {

      def ora[S <: SystemState](
          state: S,
          m: U8
      ): Byte = {
        val result = state.cpu.registers.acc | m
        state.cpu.registers.acc = result
        state.cpu.flags.updateZero(result).updateNegative(result)
        2
      }

      def zeroPageX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = ora(state, bus.readU8(addr, state)) + 2

      def absoluteY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = ora(state, bus.readU8(addr, state)) + 2

      def absoluteX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = ora(state, bus.readU8(addr, state)) + 2

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = ora(state, bus.readU8(addr, state)) + 1

      def indirectY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = ora(state, bus.readU8(addr, state)) + 3

      def indirectX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = ora(state, bus.readU8(addr, state)) + 4

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = ora(state, bus.readU8(addr, state)) + 2

      def immediate[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          imm: U8
      ): Cycles = ora(state, imm)
    }
  }

  object PHA extends Decl {
    object Modes extends ImpliedMode[0x48] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        pushStackU8(bus, state, state.cpu.registers.acc)
        3
      }
    }
  }

  object PHP extends Decl {
    object Modes extends ImpliedMode[0x08] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        pushStackU8(bus, state, state.cpu.flags.registerValue | Flags.brkBit)
        3
      }
    }
  }

  object PLA extends Decl {
    object Modes extends ImpliedMode[0x68] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        val acc = popStackU8(bus, state)
        state.cpu.registers.acc = acc
        state.cpu.flags.updateZero(acc).updateNegative(acc)
        4
      }
    }
  }

  object PLP extends Decl {
    object Modes extends ImpliedMode[0x28] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        state.cpu.flags.setValue(popStackU8(bus, state))
        4
      }
    }
  }

  object ROL extends Decl {
    object Modes
        extends AccumulatorMode[0x2a],
          ZeroPageMode[0x26],
          ZeroPageXMode[0x36],
          AbsoluteMode[0x2e],
          AbsoluteXMode[0x3e] {

      def rol[S <: SystemState](
          state: S,
          m: U8
      ): U8 = {
        val carry = m.test(0x80)
        val value = {
          if (state.cpu.flags.carry) {
            (m << 1) | 1
          } else {
            (m << 1)
          }
        }
        state.cpu.flags.carry = carry
        state.cpu.flags.updateZero(value).updateNegative(value)
        value
      }

      def zeroPageX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        bus.writeU8(addr, rol(state, bus.readU8(addr, state)), state)
        6
      }

      def accumulator[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S
      ): Cycles = {
        state.cpu.registers.acc = rol(state, state.cpu.registers.acc)
        2
      }

      def absoluteX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        bus.writeU8(addr, rol(state, bus.readU8(addr, state)), state)
        6
      }

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        bus.writeU8(addr, rol(state, bus.readU8(addr, state)), state)
        5
      }

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        bus.writeU8(addr, rol(state, bus.readU8(addr, state)), state)
        6
      }
    }
  }

  object ROR extends Decl {
    object Modes
        extends AccumulatorMode[0x6a],
          ZeroPageMode[0x66],
          ZeroPageXMode[0x76],
          AbsoluteMode[0x6e],
          AbsoluteXMode[0x7e] {

      def ror[S <: SystemState](
          state: S,
          m: U8
      ): U8 = {
        val carry = m.test(1)
        val value = {
          if (state.cpu.flags.carry) {
            (m >> 1) | 0x80
          } else {
            (m >> 1)
          }
        }
        state.cpu.flags.carry = carry
        state.cpu.flags.updateZero(value).updateNegative(value)
        value
      }

      def zeroPageX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        bus.writeU8(addr, ror(state, bus.readU8(addr, state)), state)
        6
      }

      def accumulator[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S
      ): Cycles = {
        state.cpu.registers.acc = ror(state, state.cpu.registers.acc)
        2
      }

      def absoluteX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        bus.writeU8(addr, ror(state, bus.readU8(addr, state)), state)
        5
      }

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        bus.writeU8(addr, ror(state, bus.readU8(addr, state)), state)
        5
      }

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        bus.writeU8(addr, ror(state, bus.readU8(addr, state)), state)
        6
      }
    }
  }

  object RTI extends Decl {
    object Modes extends ImpliedMode[0x40] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        state.cpu.flags.setValue(popStackU8(bus, state))
        state.cpu.registers.pc = popStackU16(bus, state)
        6
      }
    }
  }

  object RTS extends Decl {
    object Modes extends ImpliedMode[0x60] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        val pc = popStackU16(bus, state)
        state.cpu.registers.pc = pc + 1
        6
      }
    }
  }

  object SBC extends Decl {
    object Modes
        extends ImmediateMode[0xe9],
          ZeroPageMode[0xe5],
          ZeroPageXMode[0xf5],
          AbsoluteMode[0xed],
          AbsoluteXMode[0xfd],
          AbsoluteYMode[0xf9],
          IndirectXMode[0xe1],
          IndirectYMode[0xf1] {

      def sbc[S <: SystemState](
          state: S,
          m: U8
      ): Cycles = {
        val acc = state.cpu.registers.acc
        val valueU16 = acc.toU16 + (~m.toU16 + state.cpu.flags.carry.toU16(0))
        val value = valueU16.toU8

        state.cpu.flags.carry = !valueU16.testBit(8)
        state.cpu.flags.overflow = (acc ^ m).test(0x80) && !(acc ^ ~value).test(0x80)
        state.cpu.flags.negative = value.testBit(7)
        state.cpu.registers.acc = value
        state.cpu.flags.zero = value.isZero
        2
      }

      def zeroPageX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = sbc(state, bus.readU8(addr, state)) + 2

      def absoluteY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = sbc(state, bus.readU8(addr, state)) + 2

      def absoluteX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = sbc(state, bus.readU8(addr, state)) + 2

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = sbc(state, bus.readU8(addr, state)) + 1

      def indirectY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = sbc(state, bus.readU8(addr, state)) + 3

      def indirectX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = sbc(state, bus.readU8(addr, state)) + 4

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = sbc(state, bus.readU8(addr, state)) + 2

      def immediate[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          imm: U8
      ): Cycles = sbc(state, imm)
    }
  }

  object SEC extends Decl {
    object Modes extends ImpliedMode[0x38] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        state.cpu.flags.setCarry
        2
      }
    }
  }

  object SED extends Decl {
    object Modes extends ImpliedMode[0xf8] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        state.cpu.flags.setDecimal
        2
      }
    }
  }

  object SEI extends Decl {
    object Modes extends ImpliedMode[0x78] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        state.cpu.flags.setInterruptDisabled
        2
      }
    }
  }

  object STA extends Decl {
    object Modes
        extends ZeroPageMode[0x85],
          ZeroPageXMode[0x95],
          AbsoluteMode[0x8d],
          AbsoluteXMode[0x9d],
          AbsoluteYMode[0x99],
          IndirectXMode[0x81],
          IndirectYMode[0x91] {

      def sta[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        bus.writeU8(addr, state.cpu.registers.acc, state)
        3
      }

      def zeroPageX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        sta(bus, state, addr) + 1
      }

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        sta(bus, state, addr) + 1
      }

      def absoluteY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = sta(bus, state, addr) + 2

      def absoluteX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = sta(bus, state, addr) + 2

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = sta(bus, state, addr)

      def indirectY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = sta(bus, state, addr) + 3

      def indirectX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = sta(bus, state, addr) + 3
    }
  }

  object STX extends Decl {
    object Modes extends ZeroPageMode[0x86], ZeroPageYMode[0x96], AbsoluteMode[0x8e] {

      def stx[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        bus.writeU8(addr, state.cpu.registers.x, state)
        3
      }

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = stx(bus, state, addr)

      def zeroPageY[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = stx(bus, state, addr) + 1

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = stx(bus, state, addr) + 1
    }
  }

  object STY extends Decl {
    object Modes extends ZeroPageMode[0x84], ZeroPageXMode[0x94], AbsoluteMode[0x8c] {

      def sty[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = {
        bus.writeU8(addr, state.cpu.registers.y, state)
        3
      }

      def zeroPageX[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = sty(bus, state, addr) + 1

      def zeroPage[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = sty(bus, state, addr)

      def absolute[S <: SystemState](
          bus: RWBus[Addr, S],
          state: S,
          addr: Addr
      ): Cycles = sty(bus, state, addr) + 1
    }
  }

  object TAX extends Decl {
    object Modes extends ImpliedMode[0xaa] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        val value = state.cpu.registers.acc
        state.cpu.registers.x = value
        state.cpu.flags.updateZero(value).updateNegative(value)
        2
      }
    }
  }

  object TAY extends Decl {
    object Modes extends ImpliedMode[0xa8] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        val value = state.cpu.registers.acc
        state.cpu.registers.y = value
        state.cpu.flags.updateZero(value).updateNegative(value)
        2
      }
    }
  }

  object TSX extends Decl {
    object Modes extends ImpliedMode[0xba] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        val value = state.cpu.registers.sp
        state.cpu.registers.x = value
        state.cpu.flags.updateZero(value).updateNegative(value)
        2
      }
    }
  }

  object TXA extends Decl {
    object Modes extends ImpliedMode[0x8a] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        val value = state.cpu.registers.x
        state.cpu.registers.acc = value
        state.cpu.flags.updateZero(value).updateNegative(value)
        2
      }
    }
  }

  object TXS extends Decl {
    object Modes extends ImpliedMode[0x9a] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        state.cpu.registers.sp = state.cpu.registers.x
        2
      }
    }
  }

  object TYA extends Decl {
    object Modes extends ImpliedMode[0x98] {
      def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles = {
        val value = state.cpu.registers.y
        state.cpu.registers.acc = value
        state.cpu.flags.updateZero(value).updateNegative(value)
        2
      }
    }
  }

  sealed trait AddressingMode { self: Singleton => }

  transparent sealed abstract class Opcode[
      Op <: Int: ValueOf,
      Size <: Int: ValueOf
  ](implicit instructionName: String) {
    final val opcode = valueOf[Op]
    final val size = valueOf[Size]
  }

  trait AbsoluteMode[Op <: Int: ValueOf](implicit
      instructionName: String
  ) extends AddressingMode { self: Singleton =>
    def absolute[S <: SystemState](
        bus: RWBus[Addr, S],
        state: S,
        addr: Addr
    ): Cycles

    object Absolute extends Opcode[Op, 3] {
      def asm(addr: Addr) = f"$instructionName $$$addr%04X"
    }
  }

  trait AbsoluteXMode[Op <: Int: ValueOf](implicit
      instructionName: String
  ) extends AddressingMode { self: Singleton =>
    def absoluteX[S <: SystemState](
        bus: RWBus[Addr, S],
        state: S,
        addr: Addr
    ): Cycles

    object AbsoluteX extends Opcode[Op, 3] {
      def asm(addr: Addr) = f"$instructionName $$$addr%04X,X"
    }
  }

  trait AbsoluteYMode[Op <: Int: ValueOf](implicit
      instructionName: String
  ) extends AddressingMode { self: Singleton =>
    def absoluteY[S <: SystemState](
        bus: RWBus[Addr, S],
        state: S,
        addr: Addr
    ): Cycles

    object AbsoluteY extends Opcode[Op, 3] {
      def asm(addr: Addr) = f"$instructionName $$$addr%04X,Y"
    }
  }

  trait AccumulatorMode[Op <: Int: ValueOf](implicit
      instructionName: String
  ) extends AddressingMode { self: Singleton =>
    def accumulator[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles

    object Accumulator extends Opcode[Op, 1] {
      def asm = f"$instructionName A"
    }
  }

  trait ImmediateMode[Op <: Int: ValueOf](implicit
      instructionName: String
  ) extends AddressingMode { self: Singleton =>
    def immediate[S <: SystemState](
        bus: RWBus[Addr, S],
        state: S,
        imm: U8
    ): Cycles

    object Immediate extends Opcode[Op, 2] {
      def asm(imm: U8) = f"$instructionName #$$$imm%02X"
    }
  }

  trait ImpliedMode[Op <: Int: ValueOf](implicit
      instructionName: String
  ) extends AddressingMode { self: Singleton =>
    def implied[S <: SystemState](bus: RWBus[Addr, S], state: S): Cycles

    object Implied extends Opcode[Op, 1] {
      def asm = f"$instructionName"
    }
  }

  trait IndirectMode[Op <: Int: ValueOf](implicit
      instructionName: String
  ) extends AddressingMode { self: Singleton =>
    def indirect[S <: SystemState](
        bus: RWBus[Addr, S],
        state: S,
        addr: Addr
    ): Cycles

    object Indirect extends Opcode[Op, 3] {
      def asm(addr: Addr) = f"$instructionName ($$$addr%04X)"
    }
  }

  trait IndirectXMode[Op <: Int: ValueOf](implicit
      instructionName: String
  ) extends AddressingMode { self: Singleton =>
    def indirectX[S <: SystemState](
        bus: RWBus[Addr, S],
        state: S,
        addr: Addr
    ): Cycles

    object IndirectX extends Opcode[Op, 2] {
      def asm(index: U8) = f"$instructionName ($$$index%02X,X)"
    }
  }

  trait IndirectYMode[Op <: Int: ValueOf](implicit
      instructionName: String
  ) extends AddressingMode { self: Singleton =>
    def indirectY[S <: SystemState](
        bus: RWBus[Addr, S],
        state: S,
        addr: Addr
    ): Cycles

    object IndirectY extends Opcode[Op, 2] {
      def asm(index: U8) = f"$instructionName ($$$index%02X),Y"
    }
  }

  trait RelativeMode[Op <: Int: ValueOf](implicit
      instructionName: String
  ) extends AddressingMode { self: Singleton =>
    def relative[S <: SystemState](
        bus: RWBus[Addr, S],
        state: S,
        index: U8
    ): Cycles

    object Relative extends Opcode[Op, 2] {
      def asm(offset: U8) = f"$instructionName $$$offset%02X"
    }
  }

  trait ZeroPageMode[Op <: Int: ValueOf](implicit
      instructionName: String
  ) extends AddressingMode { self: Singleton =>
    def zeroPage[S <: SystemState](
        bus: RWBus[Addr, S],
        state: S,
        addr: Addr
    ): Cycles

    object ZeroPage extends Opcode[Op, 2] {
      def asm(offset: U8) = f"$instructionName $$$offset%02X"
    }
  }

  trait ZeroPageXMode[Op <: Int: ValueOf](implicit
      instructionName: String
  ) extends AddressingMode { self: Singleton =>
    def zeroPageX[S <: SystemState](
        bus: RWBus[Addr, S],
        state: S,
        addr: Addr
    ): Cycles

    object ZeroPageX extends Opcode[Op, 2] {
      def asm(offset: U8) = f"$instructionName $$$offset%02X,X"
    }
  }

  trait ZeroPageYMode[Op <: Int: ValueOf](implicit
      instructionName: String
  ) extends AddressingMode { self: Singleton =>
    def zeroPageY[S <: SystemState](
        bus: RWBus[Addr, S],
        state: S,
        addr: Addr
    ): Cycles

    object ZeroPageY extends Opcode[Op, 2] {
      def asm(offset: U8) = f"$instructionName $$$offset%02X,Y"
    }
  }

  abstract class Decl { self: Singleton =>
    implicit def instructionName: String =
      getClass().getSimpleName().dropRight(1)

    val Modes: AddressingMode
  }
}

package wes

import wes.native.conversions.*
import wes.native.types.*
import wes.rom.nes.Formats

object Disasm {

  def apply(data: ByteVector, pc: CPU6502.PC, limit: Int = 10): Listing = {
    val state = nes.NES.Configs.Mapper0.State(Formats.V1.Mirroring.Horizontal, 0, 0, 0)
    val _ = data.toArray.copyToArray(state.internalRam)

    val finalPC = if (limit < 0) pc + data.size.toInt else pc + limit

    val out = collection.mutable.Buffer.empty[Inst]

    var _pc = pc

    while (_pc < finalPC) {
      val (asm, inc) = nes.NES.Configs.Mapper0.disasmNext(state)(_pc)
      _pc = _pc + inc
      out += asm
    }

    out.toSeq
  }

  def apply(mem: ByteVector): Listing = apply(mem, 0.toU16, -1)
}

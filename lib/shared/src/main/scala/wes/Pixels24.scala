package wes

case class Pixels24(rgb: Array[RGB24], width: Int, height: Int)

package wes

import wes.native.types.*
import wes.native.conversions.*

import cats.data.*
import cats.instances.function.*
import cats.syntax.semigroup.*
import cats.syntax.validated.*

object AppCommand {
  type Parse[T] = Seq[String] => ValidatedNel[String, T]

  val parse: Parse[AppCommand] = args =>
    Disasm.parse(args) orElse RomInfo.parse(args) orElse TestGBlargg.parse(
      args
    ) orElse Run.parse(args)

  object Disasm {
    val parse: Parse[Disasm] = {
      case "disasm" +: path +: Nil => Disasm(Some(path)).validNel
      case "disasm" +: Nil         => Disasm(None).validNel
      case _                       => "not a disasm command".invalidNel
    }
  }

  case class Disasm(
      path: Option[String],
      pc: U16 = U16(0x0000),
      loadAddr: U16 = U16(0x0000)
  ) extends AppCommand

  object RomInfo {
    val parse: Parse[RomInfo] = {
      case "rom-info" +: paths => RomInfo(paths).validNel
      case _                   => "not a rom-info command".invalidNel
    }
  }

  case class RomInfo(
      paths: Seq[String]
  ) extends AppCommand

  object TestGBlargg {
    val parse: Parse[TestGBlargg] = {
      case "test-gblargg" +: path +: _ => TestGBlargg(path).validNel
      case _                           => "not a test-gblargg command".invalidNel
    }
  }

  case class TestGBlargg(
      path: String
  ) extends AppCommand

  object Run {
    val parse: Parse[Run] = {
      case "run" +: "--trace" +: tracePath +: path +: _ =>
        Run(path, Some(tracePath)).validNel
      case "run" +: path +: _ => Run(path).validNel
      case _                  => "not a run command".invalidNel
    }
  }

  case class Run(
      path: String,
      tracePath: Option[String] = None
  ) extends AppCommand
}

sealed trait AppCommand

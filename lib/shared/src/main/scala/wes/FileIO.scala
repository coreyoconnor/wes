package wes

import java.nio.channels.FileChannel
import java.nio.file.{FileSystems, StandardOpenOption}

trait FileIO {
  def dataForPath(pathStr: String): BitVector
}

object FileIO {
  object Live extends FileIO {
    def dataForPath(pathStr: String): BitVector = {
      val channel = FileChannel.open(
        FileSystems.getDefault.getPath(pathStr),
        StandardOpenOption.READ
      )
      BitVector.fromChannel(channel)
    }
  }
}

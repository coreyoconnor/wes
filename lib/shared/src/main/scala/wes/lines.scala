package wes.lines

implicit class LinesStringContext(sc: StringContext) extends AnyVal {
  def lines(args: Any*): List[String] = {
    val rawLines = sc.s(args*).linesIterator.toList
    if rawLines.head.isEmpty() then rawLines.tail else rawLines
  }
}

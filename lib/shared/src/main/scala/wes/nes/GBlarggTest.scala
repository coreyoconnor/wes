package wes
package nes

import wes.native.conversions.*
import wes.native.types.*
import wes.rom.nes.*

import java.nio.file.Paths
import java.nio.file.Files
import java.io.FileInputStream
import scala.collection.mutable

object GBlarggTest {
  import NES.*

  trait TestState {
    def done: Boolean = ???
    def ready: Boolean = ???
    var alerts: Seq[TestMonitor] = Seq.empty
  }

  object TestMonitor {
    def default = new Default

    class Default extends TestMonitor {
      val probeAddr: Addr = 0x6000.toAddr

      override def writeByte(addr: Addr, value: Byte, readByte: Addr => Byte): Unit = {
        if (addr == probeAddr) {
          println(f"test status $value%02X")

          value.toU8.toInt match {
            case 0x80 => {
              ready = true
            }
            case 0x00 if ready => {
              done = true
              message = "success"
            }
            case _ if value.toU8.toInt <= 0x80 => {
              var nextMsgByteAddr = 0x6004.toAddr
              var b = readByte(nextMsgByteAddr)
              val messageBuild = mutable.StringBuilder()
              while (b != 0) {
                messageBuild += b.toChar
                nextMsgByteAddr = nextMsgByteAddr.inc
                b = readByte(nextMsgByteAddr)
              }
              message = messageBuild.mkString
              done = true
              statusCode = value
            }
          }
        }
      }
    }
  }

  abstract class TestMonitor {
    var done = false
    var ready = false
    var statusCode: Int = 0
    var message: String | Null = null
    def writeByte(addr: Addr, value: Byte, readByte: Addr => Byte): Unit
  }

  trait TestMapper extends NES.Mapper with TestROMMapper {

    type State <: NES.NESState & TestState

    override def hasStarted(state: State): Boolean = state.alerts.forall(_.ready)
    override def isDone(state: State): Boolean = state.alerts.exists(_.done)

    abstract class TestStatusMonitor[
        B <: ByteReadBus[Addr, State] & ByteWriteBus[Addr, State]
    ](
        bus: B
    ) extends DelegateCPUBus[B](bus) {
      override def writeByte(addr: Addr, value: Byte, state: State) = {

        super.writeByte(addr, value, state)
      }
    }
  }

  object TestMapper0 extends TestMapper {
    class State(mirroring: Formats.V1.Mirroring, sramSize: Int, prgBanks: Int, chrSize: Int)
        extends Configs.Mapper0.State(mirroring, sramSize, prgBanks, chrSize)
        with TestState

    object CPUBus extends TestStatusMonitor(Configs.Mapper0.CPUBus)

    object PPUBus extends DelegatePPUBus(Configs.Mapper0.PPUBus)

    def init(rom: Formats.V1): State = {
      val state: State = new State(
        rom.mirroring,
        rom.ramPages,
        rom.prgSize,
        rom.chrSize
      )
      state.load(rom)
      state.cpu.registers.pc = CPUBus.readU16(CPU6502.resetVector, state)
      state
    }

    def disasmNext(state: State) = deriveDisasmNext(CPUBus, state)

    def execNext(state: State) = deriveExecNext(CPUBus, PPUBus, state)
  }

  object TestMapper1 extends TestMapper {

    class State(sramSize: Int, prgBanks: Int, chrSize: Int)
        extends Configs.Mapper1.State(sramSize, prgBanks, chrSize)
        with TestState

    object CPUBus extends TestStatusMonitor(Configs.Mapper1.CPUBus)

    object PPUBus extends DelegatePPUBus(Configs.Mapper1.PPUBus)

    def init(rom: Formats.V1): State = {
      val state: State = new State(
        rom.ramPages,
        rom.prgSize,
        rom.chrSize
      )
      state.load(rom)
      state.cpu.registers.pc = CPUBus.readU16(CPU6502.resetVector, state)
      state
    }

    def disasmNext(state: State) = deriveDisasmNext(CPUBus, state)

    def execNext(state: State) = deriveExecNext(CPUBus, PPUBus, state)
  }

  def run(
      path: String,
      monitor: TestMonitor = TestMonitor.default,
      config: TestROMRunner.Config = TestROMRunner.Config()
  ): TestState = {
    val is = new FileInputStream(Paths.get(path).toFile)
    val romData = BitVector.fromInputStream(is)
    val Right(v1: Formats.V1) = wes.rom.nes.Formats.read(romData): @unchecked

    v1.mapperId match {
      case 0 => TestROMRunner.run(TestMapper0, path, config)
      case 1 => TestROMRunner.run(TestMapper1, path, config)
      case _ => sys.error(s"unsupported gblarg test mapper ${v1.mapperId}")
    }
  }
}

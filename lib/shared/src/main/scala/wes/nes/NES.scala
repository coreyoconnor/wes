package wes
package nes

import wes.native.conversions.*
import wes.native.types.*
import wes.rom.nes.Formats

object NES extends LE16System {
  val cpuHz = 21441960L / 12L

  // 8k
  val sramBankSize = 0x2000
  // 16k
  val prgBankSize = 0x4000
  // 4k
  val chrBankSize = 0x1000
  // TODO: Assumes the 8k of mapper 0 and all the other mappers I could see.
  val chrRomSize = 0x2000

  class BusState(sramSize: Int, prgSize: Int, chrSize: Int) {
    val internalRam: Array[Byte] = Array.fill(InternalRam.byteSize)(0.toByte)

    val srams: Array[Array[Byte]] = (0 until sramSize).map { _ =>
      Array.fill(sramBankSize)(0.toByte)
    }.toArray

    val prgRoms: Array[Array[Byte]] = (0 until prgSize).map { _ =>
      Array.fill(prgBankSize)(0.toByte)
    }.toArray

    val chrBankMem: Array[Array[Byte]] =
      (0 until chrSize * 2).map { _ =>
        Array.fill(chrBankSize)(0.toByte)
      }.toArray

    val chrRam0: Array[Byte] = Array.fill(chrBankSize)(0.toByte)
    val chrRam1: Array[Byte] = Array.fill(chrBankSize)(0.toByte)

    val useChrRam = chrSize == 0

    def chrRomBank0: Int = 0

    def chrRomBank1: Int = 1

    var openBus: Byte = 0x00
  }

  sealed abstract class NESState(
      sramSize: Int,
      prgSize: Int,
      chrSize: Int
  ) extends BusState(sramSize, prgSize, chrSize)
      with CPU6502.SystemState
      with PPU.SystemState
      with APU.SystemState
      with Joysticks.SystemState {

    val ppu = new nes.ppu.State()

    val apu = new APU.State()

    val joysticks = new Joysticks.State()

    var cpuHaltCountdown: Int = 0

    var delayedNMI: Boolean = false

    def loadPrgRoms(rom: Formats.V1): Unit = {
      rom.prgRoms.zip(prgRoms) foreach { case (src, dst) =>
        src.copyToArray(dst, 0)
      }
    }

    // Maps the 8k chr roms to 4k chr banks.
    def loadChrRoms(rom: Formats.V1): Unit = {
      chrBankMem.toSeq.grouped(2) zip rom.chrRoms foreach { case (dests, src) =>
        assert(src.size == chrRomSize)
        assert(dests(0).size == chrBankSize)
        assert(dests(1).size == chrBankSize)
        src.copyToArray(dests(0), 0, 0, chrBankSize)
        src.copyToArray(dests(1), 0, chrBankSize, chrBankSize)
      }
    }

    def load(rom: Formats.V1): Unit = {
      loadPrgRoms(rom)
      loadChrRoms(rom)
    }
  }

  object InternalRam {
    val byteSize = 2048

    def readByte(addr: Addr, state: BusState): Byte =
      state.internalRam((addr & 0x7ff).toInt)

    def writeByte(addr: Addr, value: Byte, state: BusState): Byte = {
      state.internalRam((addr & 0x7ff).toInt) = value
      value
    }
  }

  trait Mapper {
    type State <: NESState

    trait CPUBus
        extends ReadLE16From8[State]
        with WriteLE16By8[State]
        with ByteReadBus[Addr, State]
        with ByteWriteBus[Addr, State]

    val CPUBus: CPUBus

    trait PPUBus extends PPU.Bus[State] with ReadLE16From8[State] with WriteLE16By8[State] {

      def readPatternTable0(addr: Addr, state: State): Byte = {
        if (state.useChrRam) {
          state.chrRam0(addr.toInt)
        } else {
          state.chrBankMem(state.chrRomBank0)(addr.toInt)
        }
      }

      def readPatternTable1(addr: Addr, state: State): Byte = {
        if (state.useChrRam) {
          state.chrRam1(addr.toInt)
        } else {
          state.chrBankMem(state.chrRomBank1)(addr.toInt & 0x0fff)
        }
      }

      def writePatternTable0(
          addr: Addr,
          value: Byte,
          state: State
      ): Byte = {
        if (state.useChrRam) {
          state.chrRam0.update(addr.toInt, value)
        } else {
          state.openBus = value
        }
        value
      }

      def writePatternTable1(
          addr: Addr,
          value: Byte,
          state: State
      ): Byte = {
        if (state.useChrRam) {
          state.chrRam1.update(addr.toInt, value)
        } else {
          state.openBus = value
        }
        value
      }
    }

    val PPUBus: PPUBus

    def init(rom: Formats.V1): State

    inline def deriveDisasmNext(
        bus: ReadBus[Addr, State],
        state: State
    ) = ${
      cpu6502.Disasm.derive('bus, 'state)
    }

    inline def deriveCPUExecNext(
        bus: CPUBus,
        state: State
    ) = ${
      cpu6502.Exec.derive('bus, 'state)
    }

    inline def deriveExecNext(
        cpuBus: CPUBus,
        ppuBus: PPUBus,
        state: State
    ) = {
      // this should be generalized
      if (state.cpuHaltCountdown > 0) {
        // somewhat arbitrary
        val maxCountDownCycles = 32.min(state.cpuHaltCountdown)

        (0 until maxCountDownCycles) foreach { _ =>
          if (PPU.stepOneCPUCycle(cpuBus, ppuBus, state)) {
            CPU6502.performInterrupt(cpuBus, state, CPU6502.nmiVector)
          }
          if (
            APU.stepOneCPUCycle(
              cpuBus,
              state
            ) && !state.cpu.flags.interruptDisabled
          ) {
            CPU6502.performInterrupt(cpuBus, state, CPU6502.irqVector)
          }
        }
        state.cpuHaltCountdown = state.cpuHaltCountdown - maxCountDownCycles

        maxCountDownCycles
      } else {
        val cpuCycles = deriveCPUExecNext(cpuBus, state)
        if state.delayedNMI then {
          CPU6502.performInterrupt(cpuBus, state, CPU6502.nmiVector)
          state.delayedNMI = false
        } else if state.ppu.raiseNMI then {
          state.delayedNMI = true
        }

        (0 until cpuCycles) foreach { _ =>
          if (PPU.stepOneCPUCycle(cpuBus, ppuBus, state)) {
            CPU6502.performInterrupt(cpuBus, state, CPU6502.nmiVector)
          }
          if (
            APU.stepOneCPUCycle(
              cpuBus,
              state
            ) && !state.cpu.flags.interruptDisabled
          ) {
            CPU6502.performInterrupt(cpuBus, state, CPU6502.irqVector)
          }
        }

        state.cpu.cycleCount = state.cpu.cycleCount + cpuCycles
        cpuCycles
      }
    }

    def disasmNext(state: State): PC => (Inst, Offset)

    def disasm(state: State, pc: PC, count: Int): List[(Inst, Offset)] = {
      val (acc, offset) = (0 until count).foldLeft((List.empty[(Inst, Offset)], 0)) { case ((acc, offsetAcc), _) =>
        val (inst, offset) = disasmNext(state)(pc + offsetAcc)
        ((inst, offsetAcc) :: acc, offsetAcc + offset)
      }

      acc.reverse
    }

    def disasmLines(state: State, pc: PC, count: Int): List[String] = {
      disasm(state, pc, count) map { case (inst, offset) =>
        f"${pc + offset}%04X $inst"
      }
    }

    def execNext(state: State): Cycles

    class DelegateCPUBus[
        B <: ByteReadBus[Addr, State] & ByteWriteBus[Addr, State]
    ](
        bus: B
    ) extends CPUBus {
      def readByte(addr: Addr, state: State): Byte =
        bus.readByte(addr, state)
      def writeByte(addr: Addr, value: Byte, state: State): Byte =
        bus.writeByte(addr, value, state)
    }

    class DelegatePPUBus[B <: PPU.Bus[State]](
        bus: B
    ) extends PPUBus {
      override def readPatternTable0(addr: Addr, state: State): Byte =
        bus.readPatternTable0(addr, state)

      override def readPatternTable1(addr: Addr, state: State): Byte =
        bus.readPatternTable1(addr, state)

      override def writePatternTable0(
          addr: Addr,
          value: Byte,
          state: State
      ): Byte = bus.writePatternTable0(addr, value, state)

      override def writePatternTable1(
          addr: Addr,
          value: Byte,
          state: State
      ): Byte = bus.writePatternTable1(addr, value, state)

      def readNametable(index: Int, addr: Addr, state: State): Byte =
        bus.readNametable(index, addr, state)

      def writeNametable(
          index: Int,
          addr: Addr,
          value: Byte,
          state: State
      ): Byte =
        bus.writeNametable(index, addr, value, state)
    }
  }

  object Configs {
    object Mapper0 extends Mapper {
      import Formats.V1.Mirroring

      class State(val mirroring: Mirroring, sramSize: Int, prgSize: Int, chrSize: Int)
          extends NESState(sramSize, prgSize, chrSize)

      object PPUBus extends PPUBus {
        def mapNametable(index: Int, state: State): Int = state.mirroring match
          case Mirroring.Horizontal => index >> 1
          case Mirroring.Vertical   => index & 1

        def readNametable(index: Int, addr: Addr, state: State): Byte =
          PPU.readNametableRam(mapNametable(index, state), addr, state)

        def writeNametable(
            index: Int,
            addr: Addr,
            value: Byte,
            state: State
        ): Byte = PPU.writeNametableRam(mapNametable(index, state), addr, value, state)
      }

      object CPUBus extends CPUBus {
        def readByte(addr: Addr, state: State): Byte =
          (((addr & 0xf000) >> 12).toInt: @annotation.switch) match {
            case 0x0 | 0x1 => InternalRam.readByte(addr, state)
            case 0x2 | 0x3 => PPU.readRegister(PPUBus, addr, state).toByte
            case 0x4 =>
              addr.toInt match {
                case 0x4015 => APU.read(addr, state).toByte
                case 0x4016 => Joysticks.read1(state).toByte
                case 0x4017 => Joysticks.read2(state).toByte
                case _      => state.openBus.toByte
              }
            case 0x6 | 0x7 => {
              state.srams.head((addr & 0x1fff).toInt)
            }
            case 0x8 | 0x9 | 0xa | 0xb =>
              state.prgRoms.head((addr & 0x3fff).toInt)
            case 0xc | 0xd | 0xe | 0xf =>
              state.prgRoms.last((addr & 0x3fff).toInt)
            case _ => sys.error(f"unmapped address $addr%04X")
          }

        def writeByte(addr: Addr, value: Byte, state: State): Byte =
          (((addr & 0xf000) >> 12).toInt: @annotation.switch) match {
            case 0x0 | 0x1 => InternalRam.writeByte(addr, value, state)
            case 0x2 | 0x3 =>
              PPU.writeRegister(PPUBus, addr, value.toU8, state).toByte
            case 0x4 =>
              addr.toInt match {
                case 0x4009 => {
                  state.openBus = value
                  value
                }
                case 0x4014 => {
                  val out = PPU.writeOAMDMA(value.toU8, state).toByte
                  state.cpuHaltCountdown = state.ppu.oamDMACyclesRemaining
                  out
                }
                case 0x4016 => Joysticks.writeStrobe(value.toU8, state).toByte
                case _      => APU.write(addr, value.toU8, state).toByte
              }
            case 0x6 | 0x7 => {
              state.srams.head((addr & 0x1fff).toInt) = value
              value
            }
            case 0x8 | 0x9 | 0xa | 0xb => {
              state.openBus = value
              value
            }
            case 0xc | 0xd | 0xe | 0xf => {
              state.openBus = value
              value
            }
            case _ => sys.error(f"unmapped address $addr%04X")
          }
      }

      def init(rom: Formats.V1): State = {
        val state: State = new State(rom.mirroring, rom.ramPages, rom.prgSize, rom.chrSize)
        state.load(rom)
        state.cpu.registers.pc = CPUBus.readU16(CPU6502.resetVector, state)
        state
      }

      def disasmNext(state: State) = deriveDisasmNext(CPUBus, state)

      def execNext(state: State) = deriveExecNext(CPUBus, PPUBus, state)
    }

    object Mapper1 extends Mapper {
      enum Mirroring {
        case OneScreenLowerBank, OneScreenUpperBank, Veritical, Horizontal
      }

      enum PRGBankMode {
        case Switch32KB0, Switch32KB1, FixFirst16KB, FixLast16KB
      }

      enum CHRBankMode {
        case Switch8KB, Switch4KB
      }

      class State(sramSize: Int, prgSize: Int, chrSize: Int)
          extends NESState(sramSize, prgSize, chrSize) {

        var shiftRegister: U8 = 0.toU8
        var shiftWriteCount: Int = 0

        var chrBank0: Int = 0

        override def chrRomBank0: Int = chrBankMode match {
          case CHRBankMode.Switch4KB => chrBank0
          case CHRBankMode.Switch8KB => chrBank0 & 0x1e
        }

        var chrBank1: Int = 0

        override def chrRomBank1: Int = chrBankMode match {
          case CHRBankMode.Switch4KB => chrBank1
          case CHRBankMode.Switch8KB => chrBank0 | 0x01
        }

        var prgBank: Int = 0

        var chrBankMode: CHRBankMode = CHRBankMode.Switch8KB
        var prgBankMode: PRGBankMode = PRGBankMode.FixLast16KB

        def prgBank0 = prgBankMode match {
          case PRGBankMode.Switch32KB0 | PRGBankMode.Switch32KB1 =>
            prgBank & 0x1e
          case PRGBankMode.FixFirst16KB => 0
          case PRGBankMode.FixLast16KB  => prgBank & 0x1f
        }

        def prgBank1 = prgBankMode match {
          case PRGBankMode.Switch32KB0 | PRGBankMode.Switch32KB1 =>
            prgBank | 0x01
          case PRGBankMode.FixFirst16KB => prgBank & 0x1f
          case PRGBankMode.FixLast16KB  => prgRoms.size - 1
        }

        var mirroring: Mirroring = Mirroring.OneScreenLowerBank
      }

      object PPUBus extends PPUBus {
        def mapNametable(index: Int, state: State): Int =
          state.mirroring match {
            case Mirroring.Horizontal         => index >> 1
            case Mirroring.Veritical          => index & 1
            case Mirroring.OneScreenLowerBank => 0
            case Mirroring.OneScreenUpperBank => 1
          }

        def readNametable(index: Int, addr: Addr, state: State): Byte =
          PPU.readNametableRam(mapNametable(index, state), addr, state)

        def writeNametable(
            index: Int,
            addr: Addr,
            value: Byte,
            state: State
        ): Byte = PPU.writeNametableRam(mapNametable(index, state), addr, value, state)
      }

      object CPUBus extends CPUBus {
        def readByte(addr: Addr, state: State): Byte =
          (((addr & 0xf000) >> 12).toInt: @annotation.switch) match {
            case 0x0 | 0x1 => InternalRam.readByte(addr, state)
            case 0x2 | 0x3 => PPU.readRegister(PPUBus, addr, state).toByte
            case 0x4 =>
              addr.toInt match {
                case 0x4015 => APU.read(addr, state).toByte
                case 0x4016 => Joysticks.read1(state).toByte
                case 0x4017 => Joysticks.read2(state).toByte
                case _      => state.openBus.toByte
              }
            case 0x6 | 0x7 => {
              state.srams.head((addr & 0x1fff).toInt)
            }
            case 0x8 | 0x9 | 0xa | 0xb => {
              state.prgRoms(state.prgBank0)((addr & 0x3fff).toInt)
            }
            case 0xc | 0xd | 0xe | 0xf =>
              state.prgRoms(state.prgBank1)((addr & 0x3fff).toInt)
            case _ => sys.error(f"unmapped address $addr%04X")
          }

        def writeByte(addr: Addr, value: Byte, state: State): Byte =
          (((addr & 0xf000) >> 12).toInt: @annotation.switch) match {
            case 0x0 | 0x1 => InternalRam.writeByte(addr, value, state)
            case 0x2 | 0x3 =>
              PPU.writeRegister(PPUBus, addr, value.toU8, state).toByte
            case 0x4 =>
              addr.toInt match {
                case 0x4009 => {
                  state.openBus = value
                  value
                }
                case 0x4014 => {
                  val out = PPU.writeOAMDMA(value.toU8, state).toByte
                  state.cpuHaltCountdown = state.ppu.oamDMACyclesRemaining
                  out
                }
                case 0x4016 => Joysticks.writeStrobe(value.toU8, state).toByte
                case _      => APU.write(addr, value.toU8, state).toByte
              }
            case 0x6 | 0x7 => {
              state.srams.head((addr & 0x1fff).toInt) = value
              value
            }
            case _ => {
              if (value.testBit(7)) {
                state.shiftRegister = 0.toU8
                state.shiftWriteCount = 0
              } else {
                val bit = value & 0x01
                state.shiftRegister = (state.shiftRegister >> 1) | (bit << 4)

                if (state.shiftWriteCount == 4) {
                  val targetRegister = (addr & 0x6000) >> 13
                  targetRegister.toInt match {
                    case 0 => {
                      state.mirroring = (state.shiftRegister & 0x3).toInt match {
                        case 0 => Mirroring.OneScreenLowerBank
                        case 1 => Mirroring.OneScreenUpperBank
                        case 2 => Mirroring.Veritical
                        case _ => Mirroring.Horizontal
                      }

                      state.prgBankMode = ((state.shiftRegister & 0xc) >> 2).toInt match {
                        case 0 => PRGBankMode.Switch32KB0
                        case 1 => PRGBankMode.Switch32KB1
                        case 2 => PRGBankMode.FixFirst16KB
                        case _ => PRGBankMode.FixLast16KB
                      }

                      state.chrBankMode =
                        if state.shiftRegister.testBit(5) then CHRBankMode.Switch4KB
                        else CHRBankMode.Switch8KB
                    }
                    case 1 => state.chrBank0 = state.shiftRegister.toInt
                    case 2 => state.chrBank1 = state.shiftRegister.toInt
                    case _ => state.prgBank = (state.shiftRegister & 0xf).toInt
                  }
                  state.shiftRegister = 0.toU8
                  state.shiftWriteCount = 0
                } else {
                  state.shiftWriteCount = state.shiftWriteCount.inc
                }
              }

              value
            }
          }
      }

      def init(rom: Formats.V1): State = {
        val state: State = new State(
          rom.ramPages,
          rom.prgSize,
          rom.chrSize
        )
        state.load(rom)
        state.cpu.registers.pc = CPUBus.readU16(CPU6502.resetVector, state)
        state
      }

      def disasmNext(state: State) = deriveDisasmNext(CPUBus, state)

      def execNext(state: State) = deriveExecNext(CPUBus, PPUBus, state)
    }
  }
}

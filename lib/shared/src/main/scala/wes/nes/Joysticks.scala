package wes
package nes

import wes.native.conversions.*
import wes.native.types.*

object Joysticks {
  enum Button:
    case Start, Select, A, B, Up, Down, Left, Right

  case class ButtonsPressed(
      a: Boolean = false,
      b: Boolean = false,
      select: Boolean = false,
      start: Boolean = false,
      up: Boolean = false,
      down: Boolean = false,
      left: Boolean = false,
      right: Boolean = false
  ) {
    def outBit(step: Int): U8 = {
      step match {
        case 0 => (!a).toU8(0)
        case 1 => (!b).toU8(0)
        case 2 => (!select).toU8(0)
        case 3 => (!start).toU8(0)
        case 4 => (!up).toU8(0)
        case 5 => (!down).toU8(0)
        case 6 => (!left).toU8(0)
        case 7 => (!right).toU8(0)
        case _ => 1.toU8
      }
    }
  }

  class State {
    var input1: ButtonsPressed = ButtonsPressed()
    var joy1: ButtonsPressed = ButtonsPressed()
    var joy2: ButtonsPressed = ButtonsPressed()
    var strobe: Boolean = false
    var joy1ShiftStep: Int = 0
  }

  trait SystemState {
    val joysticks: State
  }

  def writeStrobe[S <: SystemState](
      value: U8,
      state: S
  ): U8 = {

    if (value.testBit(0)) {
      state.joysticks.joy1 = state.joysticks.input1
      state.joysticks.joy1ShiftStep = 0
      state.joysticks.strobe = true
    } else {
      state.joysticks.strobe = false
    }

    value
  }

  def read1[S <: SystemState](
      state: S
  ): U8 = {
    0x40.toU8 | {
      if (state.joysticks.strobe) {
        state.joysticks.joy1.outBit(0)
      } else {
        val out = state.joysticks.joy1.outBit(state.joysticks.joy1ShiftStep)
        state.joysticks.joy1ShiftStep = state.joysticks.joy1ShiftStep + 1
        out
      }
    }
  }

  def read2[S <: SystemState](
      state: S
  ): U8 = {
    // typical last byte on bus ORd with no button pressed
    0x41.toU8
  }
}

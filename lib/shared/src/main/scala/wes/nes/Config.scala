package wes.nes

import wes.nes.ppu.Structure

case class Config(
    targetFPS: Int = 1,
    rowScanlineSize: Int = 16,
    rowColumnSize: Int = 8
) {
  val rowsPerFrame = Structure.outputHeight / rowScanlineSize
  val columnsPerFrame = Structure.outputWidth / rowColumnSize
  val cpuCyclesPerRow: Long = NES.cpuHz / (rowsPerFrame * targetFPS)
  val charMap = (33 to 126).map(_.toChar).toArray
}

package wes
package nes

import wes.native.conversions.*
import wes.native.types.*

object APU extends WithAddrRep[U16] {

  class State {
    var currentFrameCpuCycles: Long = 0L
    var apuFrames: Long = 0L
    var substep: Int = 0

    var dmcInterruptFlag = false
    var frameInterruptFlag = false

    var frameCounterMode = FrameCounterMode.Step4
    var irqInhibit = false

    object dmc extends HasCounter {
      var irqEnable = false
      var loop = false
      var freq: U8 = 0x00.toU8
      var sampleAddress: U8 = 0x00.toU8
      var sampleLength: U8 = 0x00.toU8
    }

    object noise extends HasCounter {
      var envelopeLoop = false
      def lengthCounterHalt = envelopeLoop
      var constantVolume = false
      var envelope: U8 = 0x00.toU8
      var loop: Boolean = false
      var period: U8 = 0x00.toU8
    }

    object triangle extends HasCounter {
      var linearCounterControl = false
      var linearCounterLoad: U8 = 0x00.toU8
      var timer: U16 = 0x0000.toU16
    }

    sealed trait PulseRegisters extends HasCounter {
      var duty: U8 = 0x00.toU8
      var envelopeLoop = false
      var constantVolume = false
      var envelope: U8 = 0x00.toU8
      var sweepEnable = false
      var sweepPeriod: U8 = 0x00.toU8
      var sweepNegate = false
      var sweepShift: U8 = 0x00.toU8
      var timer: U16 = 0x0000.toU16
    }

    object pulse1 extends PulseRegisters

    object pulse2 extends PulseRegisters

    trait HasCounter {
      var enable: Boolean = false

      var counter: Int = 0
      def isActive: Boolean = counter != 0 && enable

      def stepCounter: Unit = {
        if (counter > 0) {
          counter = counter - 1

          if (counter == 0) {
            enable = false
          }
        }
      }
    }

    def statusToBits: U8 = {
      val out = pulse1.isActive.toBit(0) | {
        pulse2.isActive.toBit(1)
      } | {
        noise.isActive.toBit(2)
      } | {
        triangle.isActive.toBit(3)
      } | {
        dmc.isActive.toBit(4)
      } | {
        frameInterruptFlag.toBit(6)
      } | {
        dmcInterruptFlag.toBit(7)
      }

      U8(out)
    }
  }

  trait SystemState {
    val apu: State
  }
  //
  // from https://www.nesdev.org/wiki/APU_Frame_Counter
  case class Step(
      apuCycles: Float,
      num: Int,
      quarterFrame: Boolean,
      halfFrame: Boolean,
      interrupt: Boolean
  ) {
    def cpuCycles: Int = (apuCycles * 2.0f).toInt
  }

  val ntsc4StepCycle = Seq(
    Step(
      3728.5,
      num = 1,
      quarterFrame = true,
      halfFrame = false,
      interrupt = false
    ),
    Step(
      7456.5,
      num = 2,
      quarterFrame = true,
      halfFrame = true,
      interrupt = false
    ),
    Step(
      11185.5,
      num = 3,
      quarterFrame = true,
      halfFrame = false,
      interrupt = false
    ),
    Step(
      14914,
      num = 4,
      quarterFrame = false,
      halfFrame = false,
      interrupt = true
    ),
    Step(
      14914.5,
      num = 4,
      quarterFrame = true,
      halfFrame = true,
      interrupt = true
    ),
    Step(
      14915,
      num = 4,
      quarterFrame = false,
      halfFrame = false,
      interrupt = true
    )
  )

  val ntsc5StepCycle = Seq(
    Step(
      3728.5,
      num = 1,
      quarterFrame = true,
      halfFrame = false,
      interrupt = false
    ),
    Step(
      7456.5,
      num = 2,
      quarterFrame = true,
      halfFrame = true,
      interrupt = false
    ),
    Step(
      11185.5,
      num = 3,
      quarterFrame = true,
      halfFrame = false,
      interrupt = false
    ),
    Step(
      14940.5,
      num = 4,
      quarterFrame = false,
      halfFrame = false,
      interrupt = false
    ),
    Step(
      18640.5,
      num = 5,
      quarterFrame = true,
      halfFrame = true,
      interrupt = false
    ),
    Step(
      18641,
      num = 5,
      quarterFrame = false,
      halfFrame = false,
      interrupt = false
    )
  )

  enum FrameCounterMode {
    case Step4, Step5
  }

  sealed trait SequenceEvent
  object SequenceEvent {
    case object Keep extends SequenceEvent
    case class Trigger(step: Step) extends SequenceEvent
    case object Next extends SequenceEvent
    case object Reset extends SequenceEvent
  }

  def nextSequenceEvent(
      mode: FrameCounterMode,
      substep: Int,
      cpuCycles: Long
  ): SequenceEvent = {
    val cycle = mode match {
      case FrameCounterMode.Step4 => ntsc4StepCycle
      case FrameCounterMode.Step5 => ntsc5StepCycle
    }

    val currentSubstep = cycle(substep)

    import SequenceEvent.*

    if cpuCycles == currentSubstep.cpuCycles then Trigger(currentSubstep)
    else {
      if cpuCycles > currentSubstep.cpuCycles + 2 then {
        if substep < cycle.size - 1 then Next else Reset
      } else {
        Keep
      }
    }
  }

  def write[S <: SystemState](
      addr: Addr,
      value: U8,
      state: S
  ): U8 = {
    val addrInt = addr.toInt
    if (addrInt >= 0x4000 && addrInt <= 0x4003) {
      writePulseRegister(1, addr, value, state)
    } else if (addrInt >= 0x4004 && addrInt <= 0x4007) {
      writePulseRegister(2, addr, value, state)
    } else if (addrInt >= 0x4008 && addrInt <= 0x400b) {
      writeTriangleRegister(addr, value, state)
    } else if (addrInt >= 0x400c && addrInt <= 0x400f) {
      writeNoiseRegister(addr, value, state)
    } else if (addrInt >= 0x4010 && addrInt <= 0x4013) {
      writeDMCRegister(addr, value, state)
    } else if (addrInt == 0x4015) {
      writeControlRegister(value, state)
    } else if (addrInt == 0x4017) {
      writeFrameRegister(value, state)
    } else {
      sys.error(f"not a write APU register $addr%04X")
    }
  }

  def writePulseRegister[S <: SystemState](
      num: Int,
      addr: Addr,
      value: U8,
      state: S
  ): U8 = {
    val pulse = num match {
      case 1 => state.apu.pulse1
      case 2 => state.apu.pulse2
    }

    (addr & 0x4003).toInt match {
      case 0x4000 => {
        pulse.duty = (value & 0xc0) >> 6
        pulse.envelopeLoop = value.testBit(5)
        pulse.constantVolume = value.testBit(4)
        pulse.envelope = value & 0x0f
      }
      case 0x4001 => {
        pulse.sweepEnable = value.testBit(7)
        pulse.sweepPeriod = (value & 0x70) >> 4
        pulse.sweepNegate = value.testBit(3)
        pulse.sweepShift = value & 0x07
      }
      case 0x4002 => {
        pulse.timer = (pulse.timer & 0x0700) | value.toU16
      }
      case 0x4003 => {
        pulse.counter = ((value & 0xf8) >> 3).toInt
        pulse.timer = (pulse.timer & 0x00ff) | ((value.toU16 & 0x7) << 8)
      }
    }

    value
  }

  def writeTriangleRegister[S <: SystemState](
      addr: Addr,
      value: U8,
      state: S
  ): U8 = {
    import state.apu.triangle

    addr.toInt match {
      case 0x4008 => {
        triangle.linearCounterControl = value.testBit(7)
        triangle.counter = (value & 0x7f).toInt
      }
      case 0x400a => {
        triangle.timer = (triangle.timer & 0x0700) | (value.toU16 & 0x00ff)
      }
      case 0x400b => {
        triangle.counter = ((value & 0xf8) >> 3).toInt
        triangle.timer = (triangle.timer & 0x00ff) | ((value.toU16 & 0x7) << 8)
      }
    }

    value
  }

  def writeNoiseRegister[S <: SystemState](
      addr: Addr,
      value: U8,
      state: S
  ): U8 = {
    import state.apu.noise

    addr.toInt match {
      case 0x400c => {
        noise.envelopeLoop = value.testBit(5)
        noise.constantVolume = value.testBit(4)
        noise.envelope = value & 0x0f
      }
      case 0x400d => ()
      case 0x400e => {
        noise.loop = value.testBit(7)
        noise.period = value & 0x0f
      }
      case 0x400f => {
        noise.counter = ((value & 0xf8) >> 3).toInt
      }
    }

    value
  }

  def writeDMCRegister[S <: SystemState](
      addr: Addr,
      value: U8,
      state: S
  ): U8 = {
    import state.apu.dmc

    addr.toInt match {
      case 0x4010 => {
        dmc.irqEnable = value.test(7)
        dmc.loop = value.testBit(6)
        dmc.freq = value & 0x0f
      }
      case 0x4011 => {
        dmc.counter = (value & 0x7f).toInt
      }
      case 0x4012 => {
        dmc.sampleAddress = value
      }
      case 0x4013 => {
        dmc.sampleLength = value
      }
    }

    value
  }

  def writeControlRegister[S <: SystemState](
      value: U8,
      state: S
  ): U8 = {
    import state.apu

    if (value.testBit(4)) then {
      apu.dmc.enable = true
      apu.dmc.counter = 0
    }

    if (value.testBit(3)) then {
      apu.noise.enable = true
      apu.noise.counter = 0
    }

    if (value.testBit(2)) then {
      apu.triangle.enable = true
      apu.triangle.counter = 0
    }

    if (value.testBit(1)) then {
      apu.pulse2.enable = true
      apu.pulse2.counter = 0
    }

    if (value.testBit(0)) then {
      apu.pulse1.enable = true
      apu.pulse1.counter = 0
    }

    apu.dmcInterruptFlag = false

    value
  }

  def writeFrameRegister[S <: SystemState](
      value: U8,
      state: S
  ): U8 = {
    import state.apu

    apu.frameCounterMode = value.testBit(7) match {
      case false => FrameCounterMode.Step4
      case true  => FrameCounterMode.Step5
    }

    if (value.testBit(6)) {
      apu.irqInhibit = true
      apu.frameInterruptFlag = false
    } else {
      apu.irqInhibit = false
    }

    apu.substep = 0
    apu.currentFrameCpuCycles = 0L

    value
  }

  def read[S <: SystemState](
      addr: Addr,
      state: S
  ): U8 = {
    if (addr == 0x4015.toAddr) {
      readStatusRegister(state)
    } else {
      sys.error(f"not a read APU register $addr%04X")
    }
  }

  def readStatusRegister[S <: SystemState](
      state: S
  ): U8 = {
    import state.apu

    val out = apu.statusToBits

    apu.frameInterruptFlag = false

    out
  }

  def stepOneCPUCycle[S <: SystemState](
      bus: ReadBus[Addr, S] & WriteBus[Addr, S],
      state: S
  ): Boolean = {
    import state.apu
    apu.currentFrameCpuCycles = apu.currentFrameCpuCycles + 1L

    nextSequenceEvent(
      apu.frameCounterMode,
      apu.substep,
      apu.currentFrameCpuCycles
    ) match {
      case SequenceEvent.Keep => false

      case SequenceEvent.Next => {
        apu.substep = apu.substep + 1
        false
      }

      case SequenceEvent.Reset => {
        apu.apuFrames = apu.apuFrames + 1L
        apu.currentFrameCpuCycles = 0L
        apu.substep = 0
        false
      }

      case SequenceEvent.Trigger(step) => {
        // if (step.quarterFrame) {...}
        if step.halfFrame then {
          apu.dmc.stepCounter
          apu.noise.stepCounter
          apu.triangle.stepCounter
          apu.pulse1.stepCounter
          apu.pulse2.stepCounter
        }

        if step.interrupt && !apu.irqInhibit then {
          apu.frameInterruptFlag = true
          true
        } else {
          false
        }
      }
    }
  }
}

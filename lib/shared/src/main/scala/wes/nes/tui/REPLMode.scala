package wes
package nes
package tui

import wes.native.conversions.*
import wes.native.types.*
import wes.nes.NES.{Addr, toAddr}

import fastparse.*, ScriptWhitespace.*

object REPLMode {
  object Parser {
    def parse[$: P]: ParsingRun[Command] = stmt

    def stmt[$: P]: ParsingRun[Command] = P {
      import Stmt.*

      `break-off` | `break-on` |
        `disasm` |
        `play` | `play-timed` |
        `screenshot` | `show-color-out` |
        `show-breaks` |
        `show-nametable` |
        `show-pattern-table` |
        `show-registers` |
        `show-memory` |
        `trace` |
        `trace-timed` |
        `trace-show-registers` |
        `tap` | `exit`
    }

    def decimalInt[$: P]: ParsingRun[Int] = P(CharIn("0-9").repX(1).!.map(_.toInt))
    def hexInt[$: P]: ParsingRun[Int] =
      P("$" ~~/ CharIn("0-9a-fA-F").repX(1).!).map(Integer.parseInt(_, 16))
    def integer[$: P]: ParsingRun[Int] = P(decimalInt | hexInt)

    def space[$: P] = P(CharIn(" "))

    def button[$: P]: ParsingRun[Joysticks.Button] =
      P("start" | "i").map(_ => Joysticks.Button.Start) |
        P("select" | "o").map(_ => Joysticks.Button.Select) |
        P("a" | "o").map(_ => Joysticks.Button.A) |
        P("b" | "o").map(_ => Joysticks.Button.B) |
        P("up" | "u").map(_ => Joysticks.Button.Up) |
        P("down" | "d").map(_ => Joysticks.Button.Down) |
        P("left" | "l").map(_ => Joysticks.Button.Left) |
        P("right" | "r").map(_ => Joysticks.Button.Right)

    object Stmt {
      def `break-off`[$: P]: ParsingRun[BreakOff] = P(
        "break-off" ~~ space ~/ integer ~ End
      ).map(BreakOff.apply)

      def `break-on`[$: P]: ParsingRun[BreakOn] = {
        val longForm = "break-on" ~~ space ~/ (`break-on-rts` | `break-on-pc`) ~ End
        val shortForm = "b" ~~ space ~/ (`break-on-rts` | `break-on-pc`) ~ End

        P(longForm | shortForm)
      }

      def `break-on-rts`[$: P]: ParsingRun[BreakOn.RTS.type] = P("rts").map(_ => BreakOn.RTS)
      def `break-on-pc`[$: P]: ParsingRun[BreakOn.PC] = P(integer).map { addr =>
        BreakOn.PC(addr.toU16)
      }

      def `disasm`[$: P]: ParsingRun[Disasm] = {
        val longForm = "disasm" ~~ space
        val shortForm = "d" ~~ space

        P((longForm | shortForm) ~/ (`disasm-pc` | `disasm-addr`) ~ End)
      }

      def `disasm-pc`[$: P]: ParsingRun[Disasm.AtPC] = P("pc").map(_ => Disasm.AtPC())
      def `disasm-addr`[$: P]: ParsingRun[Disasm.AtAddr] = P(integer).map { addr =>
        Disasm.AtAddr(addr.toU16)
      }

      def `trace-timed`[$: P]: ParsingRun[TraceTimed] = P(
        "trace-timed" ~~ space ~/ integer ~ End | "tt" ~~ space ~/ integer ~ End
      ).map(TraceTimed.apply)

      def `play`[$: P]: ParsingRun[Play.type] = P("play").map(_ => Play)

      def `play-timed`[$: P]: ParsingRun[PlayTimed] = P(
        "play-timed" ~~ space ~/ integer ~ End | "pt" ~~ space ~/ integer ~ End
      ).map(PlayTimed.apply)

      def `screenshot`[$: P]: ParsingRun[ColorOutAsImage.type] = P(
        "screenshot" ~ End | "ss" ~ End
      ).map(_ => ColorOutAsImage)

      def `show-breaks`[$: P]: ParsingRun[ShowBreaks.type] = P(
        "show-breaks" ~ End
      ).map(_ => ShowBreaks)

      def `show-color-out`[$: P]: ParsingRun[ShowColorOut.type] = P(
        "show-color-out" ~ End | "sc" ~ End
      ).map(_ => ShowColorOut)

      def `show-memory`[$: P]: ParsingRun[ShowMemory] = {
        val longArg = "show-memory" ~~ space ~/ integer ~~ (space ~ integer).? ~ End
        val shortArg = "sm" ~~ space ~/ integer ~~ (space ~ integer).? ~ End

        P(longArg | shortArg).map { (addr, maybeLen) =>
          ShowMemory(addr.toU16, maybeLen.getOrElse(1))
        }
      }

      def `show-nametable`[$: P]: ParsingRun[ShowNametable] = P(
        "show-nametable" ~~ space ~/ integer ~ End | "sn" ~~ space ~/ integer ~ End
      ).map(ShowNametable.apply)

      def `show-pattern-table`[$: P]: ParsingRun[ShowPatternTable] = P(
        "show-pattern-table" ~~ space ~/ integer ~ End | "sp" ~~ space ~/ integer ~ End
      ).map(ShowPatternTable.apply)

      def `show-registers`[$: P]: ParsingRun[ShowRegisters.type] = P(
        "show-registers" ~ End | "sr" ~ End
      ).map(_ => ShowRegisters)

      def `tap`[$: P]: ParsingRun[InputTap] = P(
        "tap" ~~ space ~/ button ~ End | "input-tap" ~~ space ~/ button ~ End | "i" ~~ space ~/ button ~ End
      ).map(InputTap.apply)

      def `trace`[$: P]: ParsingRun[TraceCount] = P(
        ("trace" ~ End) | ("t" ~ End)
      ).map { _ => TraceCount(1) } | P(
        ("trace" ~~ space ~/ integer ~ End) | ("t" ~~ space ~/ integer ~ End)
      ).map { count => TraceCount(count) }

      def `trace-show-registers`[$: P]: ParsingRun[TraceAndShowRegisters] = P(
        ("trace-show-registers" ~ End) | ("tsr" ~ End)
      ).map { _ => TraceAndShowRegisters(1) } | P(
        ("trace-show-registers" ~~ space ~/ integer ~ End) | ("tsr" ~~ space ~/ integer ~ End)
      ).map { count => TraceAndShowRegisters(count) }

      def `exit`[$: P]: ParsingRun[Exit.type] =
        P("exit" ~ End | "done" ~ End | "quit" ~ End).map(_ => Exit)
    }
  }

  case class BreakOff(index: Int) extends Command
  object BreakOn {
    case object RTS extends BreakOn
    case class PC(pc: Addr) extends BreakOn
  }
  sealed trait BreakOn extends Command
  object Disasm {
    case class AtPC() extends Disasm
    case class AtAddr(addr: Addr) extends Disasm
  }
  sealed trait Disasm extends Command
  case object Play extends Command
  case class PlayTimed(durationMs: Int) extends Command
  case object ColorOutAsImage extends Command
  case object ShowColorOut extends Command
  case class InputTap(button: Joysticks.Button) extends Command
  case object ShowBreaks extends Command
  case class ShowMemory(addr: Addr, len: Int) extends Command
  case class ShowNametable(index: Int) extends Command
  case class ShowPatternTable(index: Int) extends Command
  case object ShowRegisters extends Command
  case class TraceTimed(durationMs: Int) extends Command
  case class TraceCount(count: Int) extends Command
  case class TraceAndShowRegisters(count: Int) extends Command
  case object Exit extends Command

  sealed trait Command
}

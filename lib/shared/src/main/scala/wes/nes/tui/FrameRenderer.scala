package wes
package nes
package tui

import wes.native.conversions.*
import wes.nes.ppu.palette.Color
import wes.nes.NES.NESState
import wes.nes.ppu.Structure

import java.io.PrintStream

object FrameRenderer {
  object VisibleChars {
    val alphaNums = ('A' until 'Z').inclusive ++ ('0' until '9').inclusive
    val blocks = (0x25a0 until 0x25ff).inclusive map (_.toChar)

    @AllOfType
    object ForeColors {
      val Black = fansi.Color.Black.escape
      val Red = fansi.Color.Red.escape
      val Green = fansi.Color.Green.escape
      val Yellow = fansi.Color.Yellow.escape
      val Blue = fansi.Color.Blue.escape
      val Magenta = fansi.Color.Magenta.escape
      val Cyan = fansi.Color.Cyan.escape
      val LightGray = fansi.Color.LightGray.escape
      val DarkGray = fansi.Color.DarkGray.escape
      val LightRed = fansi.Color.LightRed.escape
      val LightGreen = fansi.Color.LightGreen.escape
      val LightYellow = fansi.Color.LightYellow.escape
      val LightBlue = fansi.Color.LightBlue.escape
      val LightMagenta = fansi.Color.LightMagenta.escape
      val LightCyan = fansi.Color.LightCyan.escape
      val White = fansi.Color.White.escape

      val all: Seq[String] = AllOfType.derive
    }

    @AllOfType
    object BackColors {
      val Black = fansi.Back.Black.escape
      val Red = fansi.Back.Red.escape
      val Green = fansi.Back.Green.escape
      val Yellow = fansi.Back.Yellow.escape
      val Blue = fansi.Back.Blue.escape
      val Magenta = fansi.Back.Magenta.escape
      val Cyan = fansi.Back.Cyan.escape
      val LightGray = fansi.Back.LightGray.escape
      val DarkGray = fansi.Back.DarkGray.escape
      val LightRed = fansi.Back.LightRed.escape
      val LightGreen = fansi.Back.LightGreen.escape
      val LightYellow = fansi.Back.LightYellow.escape
      val LightBlue = fansi.Back.LightBlue.escape
      val LightMagenta = fansi.Back.LightMagenta.escape
      val LightCyan = fansi.Back.LightCyan.escape
      val White = fansi.Back.White.escape

      val all: Seq[String] = AllOfType.derive
    }

    val valueMap: Array[String] = {
      for {
        backColor <- BackColors.all.toArray
        foreColor <- ForeColors.all.toArray
        ch <- alphaNums ++ blocks
      } yield (foreColor ++ backColor) :+ ch
    }.toArray
  }

  val colorReset: String = fansi.Color.Reset.escape + fansi.Back.Reset.escape
  val cursorUp: String = "\u001b[1F"
  val cursorDown: String = "\u001b[1E"
}

class FrameRenderer(config: Config) {
  import FrameRenderer._

  def updateColorBufferDisplay(out: PrintStream, buffer: Array[Color], cycles: Long): Unit = {
    val row = (cycles / config.cpuCyclesPerRow) % config.rowsPerFrame
    val linesUp = (config.rowsPerFrame - row).toInt

    (0 until linesUp).foreach { _ =>
      out.print(cursorUp)
    }

    out.print(rowToString(out, buffer, row.toInt))

    out.print("\r")
    out.print(cursorDown * linesUp)
    out.print(colorReset)
    out.flush()
  }

  def rowToString(out: PrintStream, buffer: Array[Color], row: Int): String = {
    val lowScanline = row * config.rowScanlineSize
    val highScanline = (row + 1) * config.rowScanlineSize

    val rowValueSum = (lowScanline.toInt until highScanline.toInt) map { i =>
      buffer
        .slice(i * Structure.outputWidth, (i + 1) * Structure.outputWidth)
        .grouped(config.rowColumnSize)
        .map { vs =>
          vs.map(c => c.value + c.hue).sum
        }
    } reduce { (a, b) =>
      a.zip(b).map((u, v) => (u + v).toByte)
    }

    rowValueSum.map(i => VisibleChars.valueMap(i.toU16.toInt % VisibleChars.valueMap.size)).mkString
  }

  def printColorBuffer(out: PrintStream, buffer: Array[Color]): Unit = {
    for (row <- 0 until config.rowsPerFrame) {
      out.println(rowToString(out, buffer, row))
    }
    out.print(colorReset)
    out.flush()
  }

  def printNametable(out: PrintStream, buffer: IndexedSeq[Byte]): Unit = {
    for ((b, i) <- buffer.zipWithIndex) {
      out.print(f"$b%02X")
      if (i % Structure.nametableWidth) == (Structure.nametableWidth - 1) then out.println()
    }
  }

  def printPatternTable(out: PrintStream, buffer: IndexedSeq[Byte]): Unit = {
    for ((b, i) <- buffer.zipWithIndex) {
      out.print(f"$b%02X")
      if (i & 0xf) == 0xf then out.println()
    }
  }
}

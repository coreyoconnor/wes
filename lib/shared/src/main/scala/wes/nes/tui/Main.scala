package wes
package nes
package tui

import fansi.*

import cats.data.*
import cats.syntax.validated.*
import java.nio.file.Paths
import java.nio.file.Files
import java.io.FileInputStream

class Main(fileIO: FileIO) {
  def run(args: Seq[String]): Unit = {
    val result = AppCommand.parse(args) andThen { appCommand =>
      appCommand match {
        case disasm: AppCommand.Disasm => {
          val inputData: BitVector =
            disasm.path match {
              case Some(p) => {
                val is = new FileInputStream(Paths.get(p).toFile)
                BitVector.fromInputStream(is)
              }
              case None => BitVector.fromInputStream(java.lang.System.in)
            }
          disasmData(inputData)
        }

        case romInfo: AppCommand.RomInfo =>
          {
            romInfo.paths foreach { path =>
              val is = new FileInputStream(Paths.get(path).toFile)
              val romData = BitVector.fromInputStream(is)
              val rom = wes.rom.nes.Formats.read(romData)
              println(rom.toString)
            }
          }.validNel

        case testGBlarg: AppCommand.TestGBlargg =>
          {
            GBlarggTest.run(testGBlarg.path)
          }.validNel

        case run: AppCommand.Run =>
          {
            TUI.live.run(run.path, run.tracePath)
          }.validNel
      }
    }

    result match {
      case Validated.Valid(_) => Console.err.println(Bold.On("Done").render)
      case Validated.Invalid(errors) => {
        val prefix = Bold.On(Color.Red("Fatal: "))
        val outMsg = prefix ++ Str(errors.toList.mkString(","))
        Console.err.println(outMsg.render)
      }
    }
  }

  def disasmData(inputData: BitVector): ValidatedNel[String, Unit] = {
    val listingOrError = inputData.decodeUtf8 match {
      case Right(probablyHex) => {
        BitVector
          .fromHex(probablyHex, scodec.bits.Bases.Alphabets.HexUppercase)
          .orElse {
            BitVector.fromHex(
              probablyHex,
              scodec.bits.Bases.Alphabets.HexLowercase
            )
          } match {
          case Some(data) => Right(Disasm(data.bytes))
          case None       => Left("Unable to parse hex listing")
        }
      }
      case Left(_) => {
        ???
      }
    }

    listingOrError match {
      case Left(error) => error.invalidNel
      case Right(listing) => {
        println(listing.mkString("\n"))
        ().validNel
      }
    }
  }
}

object Main extends Main(FileIO.Live) {
  def main(args: Array[String]): Unit = run(args.toIndexedSeq)
}

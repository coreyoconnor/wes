package wes
package nes
package tui

object InteractiveMode {
  enum Command:
    case SwitchToREPL
    case Exit
    case Go
    case REPL(cmd: REPLMode.Command)

  object Parser {
    import fastparse.*, ScriptWhitespace.*

    def parse[$: P]: ParsingRun[Command] = repl | exit | go

    def repl[$: P]: ParsingRun[Command.SwitchToREPL.type | Command.REPL] =
      P("repl" ~/ REPLMode.Parser.parse.?) map {
        case Some(replCommand) => Command.REPL(replCommand)
        case None              => Command.SwitchToREPL
      }

    def exit[$: P]: ParsingRun[Command.Exit.type] =
      P("exit" | "done" | "quit").map(_ => Command.Exit)

    def go[$: P]: ParsingRun[Command.Go.type] =
      P("go").map(_ => Command.Go)
  }
}

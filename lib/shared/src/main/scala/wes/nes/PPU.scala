package wes
package nes

import wes.native.conversions.*
import wes.native.types.*
import wes.nes.ppu.palette.*
import wes.nes.ppu.*

object PPU extends WithAddrRep[U16] {
  import Structure.*

  trait SystemState {
    val ppu: State
  }

  def onSpriteEvaluationTransition[S <: NES.NESState](bus: Bus[S], state: S): Unit = {
    import state.ppu
    import SpriteEvaluation.*
    import Eval.*

    ppu.spriteEvaluation.state match {
      case Disabled => ()

      case PreRender(cycles) => ()

      case PreOutputH => ()

      case SecondaryOAMClear(cycle) => {
        val out = cycle >> 3
        val b = (cycle >> 1) & 0x3
        ppu.secondarySpriteMem(out) = ppu.secondarySpriteMem(out).updateByte(b, 0xff.toU8)
        if cycle == 0 then ppu.sprite0InSecondary = false
      }

      case ReadByte(in, _, b) => {
        ppu.oamLatch = ppu.spriteMem(in).toByte(b)
      }

      case WriteByte(in, out, b) => {
        ppu.secondarySpriteMem(out) = ppu.secondarySpriteMem(out).updateByte(b, ppu.oamLatch)
        if in == 0 && b == 0 then ppu.sprite0InSecondary = true
      }

      case SkipSpriteWriteByte(in, out) => ()

      case SkipAllReadByte(out) =>
        ppu.oamLatch = ppu.spriteMem(0).toByte(0)

      case SkipAllWriteByte(out) => ()

      case SpriteOverflow(in) => {
        ppu.spriteOverflow = true
        // TODO: properly emulate the sprite overflow bug
      }

      case SpriteFetches(n, cycle, secondaryCount) =>
        if n < secondaryCount then {
          // TODO: does not handle each cycle correctly.
          if cycle == 0 then {
            val src = ppu.secondarySpriteMem(n)
            val (lowBits, highBits) = ppu.spriteSize match {
              case SpriteSize.`8x8` => {
                val tileOffset = src.tileIndex.toInt * 16
                val ySlice = ppu.spriteEvaluation.scanline
                val yOffset = if src.flipVert then {
                  8 - (ySlice - src.y.toInt + 1)
                } else {
                  ySlice - src.y.toInt
                }
                val lowBitsOffset = (tileOffset + yOffset).toU16
                val highBitsOffset = (tileOffset + yOffset + 8).toU16

                ppu.sprite8x8Addr match {
                  case PatternTableAddr.`0x0000` => {
                    val l = bus.readPatternTable0(lowBitsOffset, state)
                    val h = bus.readPatternTable0(highBitsOffset, state)
                    (l, h)
                  }
                  case PatternTableAddr.`0x1000` => {
                    val l = bus.readPatternTable1(lowBitsOffset, state)
                    val h = bus.readPatternTable1(highBitsOffset, state)
                    (l, h)
                  }
                }
              }
              case SpriteSize.`8x16` => {
                val tileOffset = (src.tileIndex.toInt >> 1) * 32
                val ySlice = ppu.spriteEvaluation.scanline
                val yOffset = if src.flipVert then {
                  16 - (ySlice.toInt - src.y.toInt + 1)
                } else {
                  ySlice.toInt - src.y.toInt
                }
                val lowBitsOffset = (tileOffset + yOffset).toU16
                val highBitsOffset = (tileOffset + yOffset + 8).toU16

                (src.tileIndex.toInt & 0x01) match {
                  case 0 => {
                    val l = bus.readPatternTable0(lowBitsOffset, state)
                    val h = bus.readPatternTable0(highBitsOffset, state)
                    (l, h)
                  }
                  case _ /* 1 */ => {
                    val l = bus.readPatternTable1(lowBitsOffset, state)
                    val h = bus.readPatternTable1(highBitsOffset, state)
                    (l, h)
                  }
                }
              }
            }

            ppu.renderSprites(n) = RenderSprite(
              src.x,
              lowBits.toU8,
              highBits.toU8,
              src.palette,
              src.behindBackground,
              src.flipHoriz,
              n == 0 && ppu.sprite0InSecondary
            )
          }
        } else {
          if (cycle == 0) ppu.renderSprites(n) = RenderSprite.zero
        }

      case BackgroundPipelineInit => ()
    }
  }

  enum Register {
    // 0x2000
    case PPUCTRL extends Register
    // 0x2001
    case PPUMASK extends Register
    // 0x2002
    case PPUSTATUS extends Register
    // 0x2003
    case OAMADDR extends Register
    // 0x2004
    case OAMDATA extends Register
    // 0x2005
    case PPUSCROLL extends Register
    // 0x2006
    case PPUADDR extends Register
    // 0x2007
    case PPUDATA extends Register
  }

  def toPPURegister(addr: Addr): Register = Register.fromOrdinal(addr.toInt & 0x7)
  def toPage(addr: Addr): Int = (addr & 0xff00).toInt
  def toSubpage(addr: Addr): Int = (addr & 0x3000).toInt
  def nametableAddr(addr: Addr): Addr = addr & 0x03ff
  def nametableIndex(addr: Addr): Int = ((addr & 0x0c00) >> 10).toInt
  def patternTableAddr(addr: Addr): Addr = addr & 0x0fff
  def patternTableOffset(patternTable: PatternTableAddr): Addr = patternTable match {
    case PatternTableAddr.`0x0000` => 0x0000.toU16
    case PatternTableAddr.`0x1000` => 0x1000.toU16
  }
  def paletteAddr(addr: Addr): Int = (addr & 0x001f).toInt

  trait Bus[-S <: SystemState] extends ByteReadBus[Addr, S] with ByteWriteBus[Addr, S] {
    final def readByte(addr: Addr, state: S): Byte = {
      toPage(addr) match {
        case 0x3f00 => readPalette(paletteAddr(addr), state)

        case _ =>
          toSubpage(addr) match {
            case 0x0000 => readPatternTable0(patternTableAddr(addr), state)
            case 0x1000 => readPatternTable1(patternTableAddr(addr), state)
            case 0x2000 =>
              readNametable(nametableIndex(addr), nametableAddr(addr), state)
            case 0x3000 =>
              readNametable(nametableIndex(addr), nametableAddr(addr), state)
          }
      }
    }

    final def writeByte(addr: Addr, value: Byte, state: S): Byte = {
      toPage(addr) match {
        case 0x3f00 => writePalette(paletteAddr(addr), value, state)

        case _ =>
          toSubpage(addr) match {
            case 0x0000 =>
              writePatternTable0(patternTableAddr(addr), value, state)
            case 0x1000 =>
              writePatternTable1(patternTableAddr(addr), value, state)
            case 0x2000 => {
              writeNametable(
                nametableIndex(addr),
                nametableAddr(addr),
                value,
                state
              )
            }
            case 0x3000 => {
              writeNametable(
                nametableIndex(addr),
                nametableAddr(addr),
                value,
                state
              )
            }
          }
      }
    }

    def readPatternTable0(addr: Addr, state: S): Byte
    def readPatternTable1(addr: Addr, state: S): Byte
    def writePatternTable0(addr: Addr, value: Byte, state: S): Byte
    def writePatternTable1(addr: Addr, value: Byte, state: S): Byte

    def readNametable(index: Int, addr: Addr, state: S): Byte
    def writeNametable(index: Int, addr: Addr, value: Byte, state: S): Byte
  }

  def readRegister[S <: SystemState](bus: Bus[S], addr: Addr, state: S): U8 =
    readRegister[S](bus, toPPURegister(addr), state)

  def writeRegister[S <: SystemState](
      bus: Bus[S],
      addr: Addr,
      value: U8,
      state: S
  ): U8 =
    writeRegister[S](bus, toPPURegister(addr), value, state)

  def readRegister[S <: SystemState](
      bus: Bus[S],
      reg: Register,
      state: S
  ): U8 = {
    import state.ppu

    reg match {
      case Register.PPUCTRL => state.ppu.cpuBusLatch
      case Register.PPUMASK => ???
      case Register.PPUSTATUS => {
        val nextRead = ppu.nextStatusValue
        ppu.vblankFlag = false
        ppu.sharedWriteLatch = SharedWriteLatch.First
        nextRead
      }
      case Register.OAMADDR => ???
      case Register.OAMDATA =>
        if state.ppu.spriteEnable || state.ppu.backgroundEnable then {
          state.ppu.spriteEvaluation.state match {
            case SpriteEvaluation.SecondaryOAMClear(_) => 0xff.toU8
            case _ => ppu.spriteMem(ppu.spriteId).toByte(ppu.spriteByte)
          }
        } else {
          ppu.spriteMem(ppu.spriteId).toByte(ppu.spriteByte)
        }
      case Register.PPUSCROLL => ???
      case Register.PPUADDR   => ???
      case Register.PPUDATA => {
        val out = bus.readU8(ppu.v.value, state)
        ppu.addrIncPerRead match {
          case AddrIncrPerRead.Add1  => ppu.incrementV1()
          case AddrIncrPerRead.Add32 => ppu.incrementV32()
        }
        // the above has different wrap around behavior during vblank?
        // https://www.nesdev.org/wiki/PPU_scrolling#$2007_(PPUDATA)_reads_and_writes
        out
      }
    }
  }

  def writeRegister[S <: SystemState](
      bus: Bus[S],
      reg: Register,
      value: U8,
      state: S
  ): U8 = {
    import state.ppu

    reg match {
      case Register.PPUCTRL => {
        if (ppu.cycles > minCyclesUntilCtrlWrite) {
          val priorNMIEnable = ppu.nmiEnable
          ppu.nmiEnable = value.testBit(7)
          if !priorNMIEnable && ppu.nmiEnable && ppu.vblankFlag then ppu.raiseNMI = true
          ppu.outputOnEXT = value.testBit(6)

          if (value.testBit(5)) {
            ppu.spriteSize = SpriteSize.`8x16`
          } else {
            ppu.spriteSize = SpriteSize.`8x8`
          }

          if (value.testBit(4)) {
            ppu.backgroundAddr = PatternTableAddr.`0x1000`
          } else {
            ppu.backgroundAddr = PatternTableAddr.`0x0000`
          }

          if (value.testBit(3)) {
            ppu.sprite8x8Addr = PatternTableAddr.`0x1000`
          } else {
            ppu.sprite8x8Addr = PatternTableAddr.`0x0000`
          }

          if (value.testBit(2)) {
            ppu.addrIncPerRead = AddrIncrPerRead.Add32
          } else {
            ppu.addrIncPerRead = AddrIncrPerRead.Add1
          }

          ppu.baseNametableIndex = (value & 0x3).toInt
        }
      }

      case Register.PPUMASK => {
        if (ppu.cycles > minCyclesUntilCtrlWrite) {
          import state.ppu

          ppu.blueEmphasis = value.testBit(7)
          ppu.greenEmphasis = value.testBit(6)
          ppu.redEmphasis = value.testBit(5)
          ppu.spriteEnable = value.testBit(4)
          ppu.backgroundEnable = value.testBit(3)
          ppu.spriteLeftColumnEnabled = value.testBit(2)
          ppu.backgroundLeftColumnEnabled = value.testBit(1)
          ppu.greyscale = value.testBit(0)
        }
      }

      case Register.PPUSTATUS => {
        ppu.cpuBusLatch = value
      }

      case Register.OAMADDR => {
        ppu.oamAddr = value.toByte
      }

      case Register.OAMDATA => {
        ppu.spriteMem(ppu.spriteId) = ppu.spriteMem(ppu.spriteId).updateByte(ppu.spriteByte, value)
        ppu.oamAddr = ppu.oamAddr.inc
      }

      case Register.PPUSCROLL => {
        if ppu.cycles > minCyclesUntilCtrlWrite then {
          ppu.sharedWriteLatch match {
            case SharedWriteLatch.First => {
              ppu.sharedWriteLatch = SharedWriteLatch.Second
              ppu.scrollX = value.toByte
            }
            case SharedWriteLatch.Second => {
              ppu.sharedWriteLatch = SharedWriteLatch.First
              ppu.scrollY = value.toByte
            }
          }
        }
      }

      case Register.PPUADDR => {
        if ppu.cycles > minCyclesUntilCtrlWrite then {
          ppu.sharedWriteLatch match {
            case SharedWriteLatch.First => {
              ppu.sharedWriteLatch = SharedWriteLatch.Second
              ppu.t.updateHigh(value)
            }
            case SharedWriteLatch.Second => {
              ppu.sharedWriteLatch = SharedWriteLatch.First
              ppu.t.updateLow(value)
              ppu.updateVFromT()
            }
          }
        }
      }

      case Register.PPUDATA => {
        val _ = bus.writeU8(ppu.v.value, value, state)

        ppu.addrIncPerRead match {
          case AddrIncrPerRead.Add1  => ppu.incrementV1()
          case AddrIncrPerRead.Add32 => ppu.incrementV32()
        }
        // the above has different wrap around behavior during vblank?
        // https://www.nesdev.org/wiki/PPU_scrolling#$2007_(PPUDATA)_reads_and_writes
      }
    }

    value
  }

  def writeOAMDMA[S <: SystemState](value: U8, state: S): U8 = {
    state.ppu.oamDMACyclesRemaining = cyclesForDMA
    state.ppu.oamDMASrcAddr = value.toU16 << 8

    value
  }

  def readNametableRam[S <: SystemState](
      index: Int,
      addr: Addr,
      state: S
  ): Byte = {
    state.ppu.nametableRam(index)(addr.toInt)
  }

  def writeNametableRam[S <: SystemState](
      index: Int,
      addr: Addr,
      value: Byte,
      state: S
  ): Byte = {
    state.ppu.nametableRam(index)(addr.toInt) = value
    value
  }

  def readPalette[S <: SystemState](addr: Int, state: S): Byte = {
    val selectSprite = addr.testBit(4)
    val colorIndex = addr & 0x3
    val paletteIndex = (addr >> 2) & 0x3
    if colorIndex == 0 then {
      Color.encode(state.ppu.paletteZeroColors(paletteIndex))
    } else {
      if selectSprite then {
        Color.encode(
          state.ppu.spritePalette(paletteIndex).colors(colorIndex - 1)
        )
      } else {
        Color.encode(
          state.ppu.backgroundPalette(paletteIndex).colors(colorIndex - 1)
        )
      }
    }
  }

  def writePalette[S <: SystemState](addr: Int, value: Byte, state: S): Byte = {
    val selectSprite = addr.testBit(4)
    val colorIndex = addr & 0x3
    val paletteIndex = (addr >> 2) & 0x3
    if (colorIndex == 0) {
      state.ppu.paletteZeroColors(paletteIndex) = Color.decode(value)
    } else {
      if (selectSprite) {
        state.ppu.spritePalette(paletteIndex).colors(colorIndex - 1) = Color.decode(value)
      } else {
        state.ppu.backgroundPalette(paletteIndex).colors(colorIndex - 1) = Color.decode(value)
      }
    }
    value
  }

  /** Step the PPU a CPU cycle, return true if NMI occurred and `nmiEnable` is true. As the PPU
    * cycles are higher frequency than CPU cycles, this likely should be changed to: step at most
    * the given PPU cycles, return the minimum number of PPU cycles until the next event.
    *
    * @param bus
    *   the PPU memory space
    * @param state
    *   the system state
    */
  def stepOneCPUCycle[S <: NES.NESState](
      cpuBus: ReadBus[U16, S],
      ppuBus: Bus[S],
      state: S
  ): Boolean = {
    if (state.ppu.oamDMACyclesRemaining > 0) {
      // just do the read and write on second cycle
      if (state.ppu.oamDMACyclesRemaining % 2 == 0) {
        val value = cpuBus.readU8(state.ppu.oamDMASrcAddr, state)

        val _ = writeRegister(ppuBus, Register.OAMDATA, value, state)

        state.ppu.oamDMASrcAddr = state.ppu.oamDMASrcAddr.inc
      }
      state.ppu.oamDMACyclesRemaining = state.ppu.oamDMACyclesRemaining.dec
    }

    state.ppu.raiseNMI = false

    (0 until ppuCyclesPerCPUCycle).foldLeft(false) { (interrupt, _) =>
      interrupt | step(ppuBus, state)
    }
  }

  def step[S <: NES.NESState](ppuBus: Bus[S], state: S): Boolean = {
    import state.ppu

    val priorVBlankFlag = ppu.vblankFlag

    if (ppu.hPos == outputStartH && ppu.vPos == (outputHeight + 1)) {
      ppu.vblankFlag = true
    }

    if (ppu.hPos == outputStartH && ppu.vPos == preRenderScanline) {
      ppu.vblankFlag = false
      ppu.sprite0Hit = false
      ppu.spriteOverflow = false
    }

    stepSpriteEvaluation(ppuBus, state)
    stepSpriteRendering(state)
    stepTileRendering(ppuBus, state)
    stepScanout(state)

    if !ppu.renderingEnabled then {
      ppu.hPos = ppu.hPos.inc
      if (ppu.hPos >= cyclesPerScanline) {
        ppu.hPos = 0
        ppu.vPos = ppu.vPos.inc
        if (ppu.vPos >= scanlinesPerFrame) {
          ppu.vPos = 0
          ppu.frameCount += 1L
        }
      }
    } else {
      val oddFrame = (ppu.frameCount & 1) == 1
      val skipCycle = ppu.vPos == preRenderScanline && ppu.hPos == (cyclesPerScanline - 1)

      if oddFrame && skipCycle then {
        ppu.hPos = 0
        ppu.vPos = 0
        ppu.frameCount += 1L
      } else {
        ppu.hPos = ppu.hPos.inc
        if (ppu.hPos >= cyclesPerScanline) {
          ppu.hPos = 0
          ppu.vPos = ppu.vPos.inc
          if (ppu.vPos >= scanlinesPerFrame) {
            ppu.vPos = 0
            ppu.frameCount += 1L
          }
        }
      }
    }

    ppu.cycles += 1L

    val vblankTransition = ppu.vblankFlag && !priorVBlankFlag
    vblankTransition && ppu.nmiEnable
  }

  def stepSpriteEvaluation[S <: NES.NESState](
      ppuBus: Bus[S],
      state: S
  ): Unit = {
    import state.ppu
    if ppu.spriteEnable || ppu.backgroundEnable then {
      if ppu.hPos > hblankStart && ppu.hPos <= nextTilesStart && ppu.vPos >= 0 && ppu.vPos < outputHeight
      then ppu.oamAddr = 0x00.toByte

      if ppu.hPos > hblankStart && ppu.hPos <= nextTilesStart && ppu.vPos == preRenderScanline
      then ppu.oamAddr = 0x00.toByte

      val currentSubstate = ppu.spriteEvaluation.state
      ppu.spriteEvaluation = SpriteEvaluation.next(
        ppu.hPos,
        ppu.vPos,
        ppu.oamLatch,
        ppu.spriteSize,
        ppu.spriteMem.toIndexedSeq,
        ppu.spriteEvaluation
      )
      val nextSubstate = ppu.spriteEvaluation.state

      if (currentSubstate != nextSubstate) {
        onSpriteEvaluationTransition(ppuBus, state)
      }
    }
  }

  def stepSpriteRendering[S <: NES.NESState](state: S): Unit = {
    import state.ppu

    ppu.spriteRendering = if ppu.hPos > 0 then {
      SpriteRendering.next(
        ppu.hPos - 1,
        ppu.spriteLeftColumnEnabled,
        ppu.renderSprites.toIndexedSeq
      )
    } else SpriteRendering.initial
  }

  def stepTileRenderingState[S <: NES.NESState](
      bus: Bus[S],
      nes: S,
      hPos: Int,
      patternTable: PatternTableAddr,
      v: U16,
      state: TileRendering
  ): TileRendering = {
    val cycle = (hPos - 1) % 8
    val validNT = hPos <= outputWidth || hPos >= nextTilesStart
    cycle match {
      case 0 => {
        val addr: Addr = 0x2000.toU16 | (v & 0x0fff)

        state.copy(
          nextNametableValue = bus.readU8(addr, nes)
        )
      }

      case 2 => {
        val addr: Addr = 0x23c0.toU16 | (v & 0x0c00) | ((v >> 4) & 0x38) | ((v >> 2) & 0x07)

        state.copy(
          nextAttrs = bus.readU8(addr, nes)
        )
      }

      case 4 if validNT => {
        val addr: Addr = patternTableOffset(patternTable) | (
          state.nextNametableValue.toU16 << 4
        ) | (
          (v & 0x7000) >> 12
        )

        state.copy(
          nextLowBits = bus.readU8(addr, nes)
        )
      }

      case 6 if validNT => {
        val addr: Addr = patternTableOffset(patternTable) | (
          state.nextNametableValue.toU16 << 4
        ) | (
          0x0008.toU16
        ) | (
          (v & 0x7000) >> 12
        )

        val nextHighBits = bus.readU8(addr, nes)

        val attrX = (v >> 1) & 0x01
        val attrY = (v >> 5) & 0x02
        val attrShift = (attrX | attrY).toInt << 1
        val attrBits = (state.nextAttrs >> attrShift) & 0x03

        val nextRenderTile = RenderTile(
          state.nextLowBits,
          nextHighBits,
          attrBits
        )

        state.copy(
          nextRenderTile = nextRenderTile
        )
      }

      // increment horizontal components in v
      case 7 if validNT => {
        nes.ppu.incrementVCourseHoriz()
        state
      }

      case _ => state
    }
  }

  def stepTileRendering[S <: NES.NESState](ppuBus: Bus[S], state: S): Unit = {
    import state.ppu

    if ppu.backgroundEnable && ppu.hPos >= outputStartH && (ppu.vPos == preRenderScanline || ppu.vPos < outputHeight)
    then {
      val cycle = (ppu.hPos - 1) % 8

      val tileRendering = stepTileRenderingState(
        ppuBus,
        state,
        ppu.hPos,
        ppu.backgroundAddr,
        ppu.v.value,
        ppu.tileRendering
      )

      // increment vert in v
      if ppu.hPos == outputWidth then ppu.incrementVVert()

      // update horizontal components in v from t
      if ppu.hPos == hblankStart then ppu.updateVHorizFromT()

      // update vertical components in v from t
      if ppu.hPos >= 280 && ppu.hPos <= 304 && ppu.vPos == preRenderScanline
      then ppu.updateVVertFromT()

      if ppu.hPos == outputStartH then {
        ppu.outputRegisters.shiftIn(ppu.tileRendering.nextRenderTile)
      }

      if ppu.vPos < outputHeight && ppu.hPos > outputStartH && ppu.hPos <= outputWidth then {
        ppu.outputRegisters.shift()
        if cycle == 0 then ppu.outputRegisters.orIn(ppu.tileRendering.nextRenderTile)
      }

      if ppu.hPos == 329 then {
        ppu.outputRegisters.shiftIn(ppu.tileRendering.nextRenderTile)
      }

      ppu.tileRendering = tileRendering
    }
  }

  def stepScanout[S <: NES.NESState](state: S): Unit = {
    import state.ppu

    if ppu.hPos >= outputStartH && ppu.hPos <= outputWidth && ppu.vPos < outputHeight
    then {
      inline def backdropColor = ppu.backdropColor

      val backgroundValue = if ppu.backgroundLeftColumnEnabled then {
        ppu.outputRegisters.pixel(ppu.fineScrollX)
      } else {
        if ppu.hPos <= 8 then 0 else ppu.outputRegisters.pixel(ppu.fineScrollX)
      }

      inline def backgroundColor = {
        if backgroundValue == 0 then backdropColor
        else {
          val backgroundPalette = ppu.outputRegisters.palette(ppu.fineScrollX)
          ppu.backgroundPalette(backgroundPalette).colors(backgroundValue - 1)
        }
      }

      val outColor = ppu.spriteRendering.sprite match {
        case Some(sprite) if ppu.hPos < outputWidth => {
          val spriteValue = sprite.pixel(ppu.hPos - 1)

          inline def spriteColor = {
            if spriteValue == 0 then backdropColor
            else {
              val spritePalette = sprite.palette.toInt
              ppu.spritePalette(spritePalette).colors(spriteValue - 1)
            }
          }

          inline def shouldCheckForHit =
            !ppu.sprite0Hit && ppu.backgroundEnable && ppu.spriteEnable && sprite.isSprite0
          inline def bothNonZero = spriteValue != 0 && backgroundValue != 0

          if shouldCheckForHit && bothNonZero then ppu.sprite0Hit = true

          if ppu.spriteEnable then {
            (backgroundValue, spriteValue, sprite.behindBackground) match {
              case (0, _, _)     => spriteColor
              case (_, 0, _)     => backgroundColor
              case (_, _, false) => spriteColor
              case (_, _, true)  => backgroundColor
            }
          } else {
            backgroundColor
          }
        }
        case _ => backgroundColor
      }

      ppu.colorOut(ppu.vPos * outputWidth + (ppu.hPos - outputStartH)) = outColor
    }
  }

  def pixels[S <: NES.NESState](state: S): Pixels24 = {
    import state.ppu

    val rgb = ppu.colorOut map { c =>
      DisplayLUT.lookupRGB(c, ppu.redEmphasis, ppu.greenEmphasis, ppu.blueEmphasis)
    }
    Pixels24(rgb, outputWidth, outputHeight)
  }
}

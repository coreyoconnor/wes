package wes
package nes

import wes.native.conversions.*
import wes.native.types.*
import wes.rom.nes.*
import wes.nes.TUI

import scala.collection.mutable

object TestROMMapper {
  object Disasm {
    object Region {
      given lowOrd: Ordering[Region] with {
        def compare(a: Region, b: Region): Int =
          Ordering[Int].compare(a.start, b.start)
      }
    }
    case class Region(name: String, start: Int, `end`: Int)

    object RegionExclusions {
      def apply(regions: Region*): RegionExclusions = {
        val out = new RegionExclusions
        regions foreach out.addRegion
        out
      }
    }

    class RegionExclusions {
      private var currentRegion: String | Null = null
      private var regions: Seq[Region] = Seq.empty

      def addRegion(region: Region): Unit = regions = (regions :+ region).sorted
      def removeRegion(name: String): Unit = regions = regions.filter(_.name != name)

      def disasm[S <: NES.NESState](state: S): Boolean = {
        regions
          .filter(_.start <= state.cpu.registers.pc.toInt)
          .filter(_.`end` >= state.cpu.registers.pc.toInt)
          .sortBy(r => r.`end` - r.start)
          .headOption match {
          case None => {
            currentRegion = null
            true
          }
          case Some(region) => {
            if region.name != currentRegion then {
              println(
                f"entering region ${region.name} at ${state.cpu.registers.pc}%04X"
              )
              currentRegion = region.name
            }
            false
          }
        }
      }
    }
  }
}

trait TestROMMapper { self: NES.Mapper =>
  val requiresInit: Boolean = true
  def hasStarted(state: State): Boolean = !requiresInit
  def isDone(state: State): Boolean
}

object TestROMRunner {
  import TestROMMapper.Disasm.RegionExclusions

  case class Config(
      initLimit: Long = 35000L,
      completeLimit: Long = 150000000L,
      displayPPU: Boolean = false,
      trace: Boolean = false,
      tracePerf: Boolean = false,
      disasmExclusions: RegionExclusions = RegionExclusions(),
      repl: Boolean = false
  )

  def run[M <: NES.Mapper & TestROMMapper](
      mapper: M,
      path: String,
      config: Config = Config()
  ): mapper.State = {
    val rom = FileIO.Live.dataForPath(path)
    val Right(v1: Formats.V1) = Formats.read(rom): @unchecked

    if config.repl then TUI.live.run(mapper, v1, None) else run(mapper, v1, config)
  }

  def run[M <: NES.Mapper & TestROMMapper](
      mapper: M,
      rom: Formats.V1,
      config: Config
  ): mapper.State = {
    val state = mapper.init(rom)

    if mapper.requiresInit then {
      (0L until config.initLimit) foreach { _ =>
        val pc = state.cpu.registers.pc
        try {
          if config.trace && config.disasmExclusions.disasm(state) then {
            val (asm, inc) = mapper.disasmNext(state)(pc)
            println(f"$pc%04X $asm")
          }
          mapper.execNext(state)
        } catch {
          case ex: Throwable => {
            println(f"PC: $pc%04X")
            val (asm, inc) = mapper.disasmNext(state)(pc)
            println(s"$asm")
            throw ex
          }
        }
      }
    }

    assert(
      mapper.hasStarted(state),
      s"test rom did not signal initialized in ${config.initLimit} instructions"
    )

    var remainingInsts = config.completeLimit
    var perfCycles = 0L
    var perfStartTime = java.lang.System.nanoTime()
    var averageHz: Double = 0.0
    var averageHzCount: Long = 0L

    var cpuCyclesOfNextDisplayUpdate =
      state.cpu.cycleCount + TUI.live.config.cpuCyclesPerRow

    while remainingInsts > 0 && !mapper.isDone(state) do {
      val pc = state.cpu.registers.pc
      try {
        if config.trace && config.disasmExclusions.disasm(state) then {
          val (asm, inc) = mapper.disasmNext(state)(pc)
          println(f"$pc%04X $asm")
        }
        val cycles = mapper.execNext(state)
        perfCycles = perfCycles + cycles.toLong

        if config.displayPPU && state.cpu.cycleCount > cpuCyclesOfNextDisplayUpdate then {
          TUI.live.displayColorOut(state)
          cpuCyclesOfNextDisplayUpdate = state.cpu.cycleCount + TUI.live.config.cpuCyclesPerRow
        }
      } catch {
        case ex: Throwable => {
          println(f"PC: $pc%04X")
          val (asm, inc) = mapper.disasmNext(state)(pc)
          println(s"$asm")
          throw ex
        }
      }

      if (perfCycles >= 100000L) {
        val now = java.lang.System.nanoTime()
        val duration = now - perfStartTime
        val hz = 100000.0 / (duration.toDouble / 1000000000.0)
        if config.tracePerf then println(f"\nTarget: ${NES.cpuHz}Hz Actual: ${hz}%fHz")
        averageHz += hz
        averageHzCount += 1
        perfCycles = 0L
        perfStartTime = now
      }

      remainingInsts = remainingInsts - 1L
    }

    println(f"\nTarget: ${NES.cpuHz}Hz Average: ${averageHz / averageHzCount.toDouble}Hz")

    assert(remainingInsts > 0L, f"did not succeed before instruction limit")

    state
  }
}

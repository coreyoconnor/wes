package wes
package nes

import wes.rom.nes.Formats
import wes.native.conversions.*
import wes.native.types.*
import wes.io.BMPWriter
import wes.nes.NES.{Addr, NESState, toAddr}
import wes.nes.ppu.Structure
import wes.nes.tui.FrameRenderer
import wes.nes.tui.InteractiveMode
import wes.nes.tui.REPLMode

import java.nio.file.Paths
import java.time.Instant
import java.io.*
import scala.annotation.tailrec

object TUI {
  val defaultConfig = Config()
  lazy val live = TUI(
    config = defaultConfig,
    new FileInputStream(FileDescriptor.in),
    new FileOutputStream(FileDescriptor.out)
  )

  enum Mode:
    case REPL, Interactive, Done

  object StopCondition {
    case class CycleCount(count: Long) extends StopCondition
    case class InstCount(count: Long) extends StopCondition
  }
  sealed trait StopCondition

  enum StopReason:
    case Break(i: Int)
    case Limit

  enum Breakpoint:
    case PC(pc: Addr)
    case RTS(sp: U8)

  type Breakpoints = Vector[Breakpoint]

  def bmpScreenshotColorOut(state: NES.NESState): Pixels24 = {
    import BMPWriter.Pixel
    val pixels = PPU.pixels(state)

    val bmpPixels = pixels.rgb map { rgb =>
      Pixel(r = rgb.r, g = rgb.g, b = rgb.b)
    }

    val filename = s"screenshot-${Instant.now().toString()}.bmp"
    BMPWriter.out(bmpPixels.toIndexedSeq, pixels.width, filename)
    pixels
  }

  def showRegisters(out: PrintStream, state: NES.NESState): Unit = {
    val registers = state.cpu.registers

    val msg = f"""
    |PC: ${registers.pc}%04X SP: ${registers.sp}%02X
    |ACC: ${registers.acc}%02X
    |X: ${registers.x}%02X Y: ${registers.y}%02X
    |""".stripMargin.trim

    out.println(msg)
  }

  import fastparse.*

  @tailrec
  def readCommand[T](
      out: PrintStream,
      in: BufferedReader,
      onClose: T,
      prior: Option[T],
      p: P[?] ?=> P[T]
  ): T = {
    out.print("> ")
    out.flush()
    val line = in.readLine()

    if line == null then onClose
    else {
      val input = line.trim()

      if input.isEmpty then {
        prior match {
          case None    => readCommand(out, in, onClose, prior, p)
          case Some(c) => c
        }
      } else {
        val parsed = fastparse.parse(input, p(using _))

        parsed match
          case fastparse.Parsed.Success(value, index) => value
          case error: fastparse.Parsed.Failure =>
            out.println(error.trace())
            out.println(s"parse error: ${error.msg}")
            readCommand(out, in, onClose, prior, p)
      }
    }
  }

  def printMemory[M <: NES.Mapper](
      out: PrintStream,
      mapper: M,
      state: mapper.State,
      addr: Addr,
      len: Int
  ): Unit = {
    val width = 8

    (0 until len) foreach { offset =>
      val b = mapper.CPUBus.readU8(addr + offset, state)

      val i = offset % width

      if i == 0 then {
        out.print(f"$$${addr + offset}%02X|")
      }

      out.print(f" $b%02X")

      if i == (width - 1) then {
        out.println()
      }
    }

    out.println()
  }
}

class TUI(val config: Config, sourceInputStream: InputStream, targetOutStream: OutputStream) {
  import TUI.*

  private val initialBuffer = config.rowsPerFrame * config.columnsPerFrame * 4
  private val bufferedOutput = new BufferedOutputStream(targetOutStream, initialBuffer)
  private val out = new PrintStream(bufferedOutput)
  private val in = new BufferedReader(new InputStreamReader(sourceInputStream))
  private val frameRenderer = new FrameRenderer(config)

  inline def run[M <: NES.Mapper](
      mapper: M,
      rom: Formats.V1,
      inline sessionTrace: Option[PrintStream]
  ): mapper.State = {
    val state = mapper.init(rom)

    var nextMode = Mode.REPL

    while (nextMode != Mode.Done) {
      nextMode match {
        case Mode.REPL =>
          nextMode = repl(mapper, state, sessionTrace)
        case Mode.Interactive =>
          nextMode = interactive(mapper, state, sessionTrace)
        case Mode.Done => ()
      }
    }

    state
  }

  inline def interactive[M <: NES.Mapper](
      mapper: M,
      state: mapper.State,
      inline sessionTrace: Option[PrintStream]
  ): Mode = {
    import Joysticks.ButtonsPressed
    import InteractiveMode.*

    var nextMode = Mode.Interactive
    var priorCommand: Option[Command] = None

    while (nextMode == Mode.Interactive) {
      displayColorOut(state)

      state.joysticks.input1 = ButtonsPressed()

      val command = readCommand(out, in, Command.Exit, priorCommand, Parser.parse)
      priorCommand = Some(command)

      command match {
        case Command.SwitchToREPL      => nextMode = Mode.REPL
        case Command.Exit              => nextMode = Mode.Done
        case Command.Go                => ???
        case Command.REPL(replCommand) => ???
      }
    }

    nextMode
  }

  inline def repl[M <: NES.Mapper](
      mapper: M,
      state: mapper.State,
      inline sessionTrace: Option[PrintStream]
  ): Mode = {
    import REPLMode.*

    var nextMode = Mode.REPL
    var priorCommand: Option[REPLMode.Command] = None
    var breakpoints: Breakpoints = Vector.empty

    while (nextMode == Mode.REPL) {
      val command = readCommand(out, in, Exit, priorCommand, REPLMode.Parser.parse)
      priorCommand = Some(command)
      var stopReason: Option[StopReason] = None

      command match {
        case BreakOff(i) =>
          val (prior, rest) = breakpoints.splitAt(i)
          breakpoints = prior ++ rest.tail

        case BreakOn.PC(pc) =>
          val bp = Breakpoint.PC(pc)
          out.println(f"set breakpoint ${breakpoints.size} for pc $pc%04X")
          breakpoints = breakpoints :+ bp

        case BreakOn.RTS =>
          val sp = state.cpu.registers.sp
          val bp = Breakpoint.RTS(sp)
          out.println(f"set breakpoint ${breakpoints.size} for RTS to sp > $sp%02X")
          breakpoints = breakpoints :+ bp

        case ColorOutAsImage =>
          bmpScreenshotColorOut(state)

        case Disasm.AtAddr(addr) =>
          val lines = mapper.disasmLines(state, addr, 10)
          out.println(lines.mkString("\n"))

        case Disasm.AtPC() =>
          val lines = mapper.disasmLines(state, state.cpu.registers.pc, 10)
          out.println(lines.mkString("\n"))

        case Exit =>
          nextMode = Mode.Done

        case InputTap(button) => ???

        case Play =>
          nextMode = Mode.Interactive

        case PlayTimed(durationMs) => ???

        case ShowBreaks =>
          breakpoints.zipWithIndex foreach {
            case (Breakpoint.PC(pc), i) =>
              out.println(f"$i%02d - pc  @ pc = $pc%04X")
            case (Breakpoint.RTS(sp), i) =>
              out.println(f"$i%02d - rts @ sp > $sp%02X")
          }

        case ShowColorOut =>
          frameRenderer.printColorBuffer(out, state.ppu.colorOut)

        case ShowMemory(addr, len) =>
          printMemory(out, mapper, state, addr, len)

        case ShowNametable(index) =>
          val buffer = (0 until Structure.nametableBankSize) map { offset =>
            mapper.PPUBus.readNametable(index, offset.toU16, state)
          }
          frameRenderer.printNametable(out, buffer)

        case ShowPatternTable(index) =>
          val buffer = (0 until 0x100) map { offset =>
            index match {
              case 0 => mapper.PPUBus.readPatternTable0(offset.toU16, state)
              case _ => mapper.PPUBus.readPatternTable1(offset.toU16, state)
            }
          }
          frameRenderer.printPatternTable(out, buffer)

        case ShowRegisters =>
          showRegisters(out, state)

        case TraceAndShowRegisters(count) =>
          if breakpoints.isEmpty then
            stopReason = Some {
              runForInstCount[M](mapper, state, count, Mode.REPL, true, sessionTrace, None)
            }
          else
            stopReason = Some {
              runForInstCount[M](
                mapper,
                state,
                count,
                Mode.REPL,
                true,
                sessionTrace,
                Some(breakpoints)
              )
            }
          showRegisters(out, state)

        case TraceCount(count) =>
          if breakpoints.isEmpty then
            stopReason = Some {
              runForInstCount[M](mapper, state, count, Mode.REPL, true, sessionTrace, None)
            }
          else
            stopReason = Some {
              runForInstCount[M](
                mapper,
                state,
                count,
                Mode.REPL,
                true,
                sessionTrace,
                Some(breakpoints)
              )
            }

        case TraceTimed(durationMs) =>
          if breakpoints.isEmpty then
            stopReason = Some {
              runForDuration[M](mapper, state, durationMs, Mode.REPL, true, sessionTrace, None)
            }
          else
            stopReason = Some {
              runForDuration[M](
                mapper,
                state,
                durationMs,
                Mode.REPL,
                true,
                sessionTrace,
                Some(breakpoints)
              )
            }
      }

      stopReason foreach {
        case StopReason.Break(i) => {
          val pc = breakpoints(i) match {
            case Breakpoint.PC(bpPC)  => bpPC
            case Breakpoint.RTS(bpSP) => state.cpu.registers.pc
          }

          val (asm, _) = mapper.disasmNext(state)(pc)
          val disasm = f"> $pc%04X $asm"
          out.println(s"Hit breakpoint $i")
          out.println(disasm)

          inline sessionTrace match {
            case None => ()
            case Some(sout) =>
              sout.println(s"Hit breakpoint $i")
              sout.println(disasm)
          }
        }
        case _ => ()
      }
    }

    nextMode
  }

  def displayColorOut(state: NESState): Unit = {
    frameRenderer.updateColorBufferDisplay(out, state.ppu.colorOut, state.cpu.cycleCount)
  }

  inline def runForDuration[M <: NES.Mapper](
      mapper: M,
      state: mapper.State,
      durationMs: Float,
      inline mode: Mode,
      inline trace: Boolean,
      inline sessionTrace: Option[PrintStream],
      inline breakpoints: Option[Breakpoints]
  ): StopReason = {
    var cycleCount = (NES.cpuHz.toFloat * (durationMs / 1000.0f)).toLong

    runUntil[M](
      mapper,
      state,
      StopCondition.CycleCount(cycleCount),
      mode,
      trace,
      sessionTrace,
      breakpoints
    )
  }

  inline def runForInstCount[M <: NES.Mapper](
      mapper: M,
      state: mapper.State,
      count: Long,
      inline mode: Mode,
      inline trace: Boolean,
      inline sessionTrace: Option[PrintStream],
      inline breakpoints: Option[Breakpoints]
  ): StopReason = {
    runUntil[M](
      mapper,
      state,
      StopCondition.InstCount(count),
      mode,
      trace,
      sessionTrace,
      breakpoints
    )
  }

  inline def runUntil[M <: NES.Mapper](
      mapper: M,
      state: mapper.State,
      inline runCondition: StopCondition,
      inline mode: Mode,
      inline trace: Boolean,
      inline sessionTrace: Option[PrintStream],
      inline breakpoints: Option[Breakpoints]
  ): StopReason = {
    var count: Long = 0
    var stopReason: StopReason = null

    while (stopReason == null) {
      val pc = state.cpu.registers.pc
      try {
        inline if trace then {
          val lines = mapper.disasmLines(state, pc, 1)
          val msg = lines.mkString("\n")
          inline mode match {
            case Mode.REPL => {
              out.println(msg)
              inline sessionTrace match {
                case None       => ()
                case Some(sout) => sout.println(msg)
              }
            }
            case Mode.Interactive => {
              inline sessionTrace match {
                case None       => ()
                case Some(sout) => sout.println(msg)
              }
            }
          }
        }

        val cycles = mapper.execNext(state)

        inline breakpoints match {
          case None => ()

          case Some(bps) => {
            val nextPC = state.cpu.registers.pc
            val sp = state.cpu.registers.sp.toInt
            val rtsOpcode = CPU6502.RTS.Modes.Implied.opcode.toU8
            val inst = mapper.CPUBus.readU8(nextPC, state)

            bps.zipWithIndex foreach {
              case (Breakpoint.PC(bpPC), i) if bpPC == nextPC =>
                stopReason = StopReason.Break(i)

              case (Breakpoint.RTS(bpSP), i) if inst == rtsOpcode && sp >= bpSP.toInt =>
                stopReason = StopReason.Break(i)

              case _ => ()
            }
          }
        }

        inline runCondition match {
          case StopCondition.CycleCount(targetCount) => {
            if null == stopReason then {
              count += cycles
              if count >= targetCount then stopReason = StopReason.Limit
            }
          }
          case StopCondition.InstCount(targetCount) => {
            if null == stopReason then {
              count += 1
              if count >= targetCount then stopReason = StopReason.Limit
            }
          }
        }

        inline mode match {
          case Mode.REPL        => ()
          case Mode.Interactive => ???
        }

      } catch {
        case ex: Throwable => {
          out.println(f"PC: $pc%04X")
          val (asm, _) = mapper.disasmNext(state)(pc)
          out.println(s"$asm")

          throw ex
        }
      }
    }

    stopReason
  }

  inline def run(
      rom: Formats.V1,
      inline traceStream: Option[PrintStream]
  ): NES.NESState = {
    rom.mapperId match {
      case 0 => run(NES.Configs.Mapper0, rom, traceStream)
      case 1 => run(NES.Configs.Mapper1, rom, traceStream)
      case _ => sys.error(s"unsupport mapper $rom")
    }
  }

  inline def run(
      rom: Formats.V1 | Formats.V2,
      inline traceStream: Option[PrintStream]
  ): NES.NESState = {
    rom match {
      case v1: Formats.V1 => run(v1, traceStream)
      case _              => sys.error(s"unsupported $rom")
    }
  }

  def run(path: String, tracePath: Option[String]): NES.NESState = {
    val is = new FileInputStream(Paths.get(path).toFile)
    val romData = BitVector.fromInputStream(is)
    val Right(rom) = wes.rom.nes.Formats.read(romData): @unchecked

    tracePath match {
      case None => run(rom, None)

      case Some(tracePath) => {
        val os = new FileOutputStream(Paths.get(tracePath).toFile)
        val pos = new PrintStream(os, true)
        run(rom, Some(pos))
      }
    }
  }
}

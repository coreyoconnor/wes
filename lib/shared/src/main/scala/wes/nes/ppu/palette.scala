package wes
package nes
package ppu
package palette

import wes.native.conversions.*
import wes.native.types.*

import scodec.*
import scodec.bits.*
import scodec.codecs.*

object Color {
  val zero = Color(0, 0)

  def decode(byte: Byte): Color = {
    Color(value = (byte & 0x30) >> 4, hue = byte & 0x0f)
  }

  def encode(color: Color): Byte = ((color.value << 4) | color.hue).toByte
}

case class Color(value: Int, hue: Int) {
  assert(value >= 0 && value < 4)
  assert(hue >= 0 && hue < 16)
}

class Palette {
  val colors: Array[Color] = Array.fill(3)(Color(0, 0))
}

object DisplayLUT {
  val lutBytes = getClass().getResourceAsStream("/Composite_wiki.pal").readAllBytes()

  implicit val rgbCodec: Codec[RGB24] = {
    ("r" | byte) :: ("g" | byte) :: ("b" | byte)
  }.as[RGB24]

  val lutRGB =
    Codec[RGB24].decodeAll(Vector(_))(Vector.empty[RGB24], _ ++ _)(BitVector(lutBytes))._2.toArray

  assert(lutRGB.size == 64 * 8)

  def lookupRGB(
      color: Color,
      redEmphasis: Boolean,
      greenEmphasis: Boolean,
      blueEmphasis: Boolean
  ): RGB24 = {
    val emphasisIndex = redEmphasis.toBit(0) | greenEmphasis.toBit(1) | blueEmphasis.toBit(2)
    val colorIndex = Color.encode(color).toInt
    val index = 64 * emphasisIndex + colorIndex
    lutRGB(index)
  }
}

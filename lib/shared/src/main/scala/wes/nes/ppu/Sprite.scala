package wes
package nes
package ppu

import wes.native.conversions.*
import wes.native.types.*

object Sprite {
  val zero = Sprite(
    x = 0.toU8,
    y = 0.toU8,
    tileIndex = 0.toU8,
    palette = 0.toU8,
    behindBackground = false,
    flipHoriz = false,
    flipVert = false
  )
}

case class Sprite(
    x: U8,
    y: U8,
    tileIndex: U8,
    palette: U8,
    behindBackground: Boolean,
    flipHoriz: Boolean,
    flipVert: Boolean
) {
  def updateByte0(v: U8): Sprite = copy(y = v)

  def updateByte1(v: U8): Sprite = copy(tileIndex = v)

  def updateByte2(v: U8): Sprite = copy(
    palette = v & 0x3,
    behindBackground = v.testBit(5),
    flipHoriz = v.testBit(6),
    flipVert = v.testBit(7)
  )

  def updateByte3(v: U8): Sprite = copy(x = v)

  def toByte0: U8 = y

  def toByte1: U8 = tileIndex

  def toByte2: U8 = {
    palette | {
      if (behindBackground) 1 << 5 else 0
    } | {
      if (flipHoriz) 1 << 6 else 0
    } | {
      if (flipVert) 1 << 7 else 0
    }
  }

  def toByte3: U8 = x

  inline def toByte(b: Int): U8 = b match {
    case 0 => toByte0
    case 1 => toByte1
    case 2 => toByte2
    case _ => toByte3
  }

  inline def updateByte(b: Int, v: U8): Sprite = b match {
    case 0 => updateByte0(v)
    case 1 => updateByte1(v)
    case 2 => updateByte2(v)
    case _ => updateByte3(v)
  }
}

package wes
package nes
package ppu

import wes.native.conversions.*
import wes.native.types.*
import wes.nes.ppu.palette.*

enum PatternTableAddr {
  case `0x0000`, `0x1000`
}

enum AddrIncrPerRead {
  case Add1, Add32
}

enum SharedWriteLatch {
  case First, Second
}

class State extends WithAddrRep[U16] {
  import Structure.*

  var cpuBusLatch: U8 = 0x00.toU8
  var cycles: Long = 21L
  var hPos: Int = 21
  var vPos: Int = 0
  var frameCount: Long = 0L

  /* Post palette lookup.
   */
  val colorOut: Array[Color] = Array.fill(outputWidth * outputHeight)(Color.zero)

  val nametableRam: Array[Array[Byte]] = Array.fill(2)(Array.fill(nametableBankSize)(0.toByte))

  val backgroundPalette: Array[Palette] = Array.fill(paletteSize)(new Palette)
  val spritePalette: Array[Palette] = Array.fill(paletteSize)(new Palette)
  val paletteZeroColors: Array[Color] = Array.fill(paletteSize)(Color.zero)
  def backdropColor = paletteZeroColors.head

  var nmiEnable: Boolean = false
  var raiseNMI: Boolean = false
  var outputOnEXT: Boolean = false
  var spriteSize: SpriteSize = SpriteSize.`8x8`
  var backgroundAddr: PatternTableAddr = PatternTableAddr.`0x0000`
  var sprite8x8Addr: PatternTableAddr = PatternTableAddr.`0x0000`
  var addrIncPerRead: AddrIncrPerRead = AddrIncrPerRead.Add1

  var baseNametableIndex: Int = 0
  def nametableX: Int = baseNametableIndex & 0x1
  def nametableY: Int = (baseNametableIndex & 0x2) >> 1

  var blueEmphasis: Boolean = false
  var greenEmphasis: Boolean = false
  var redEmphasis: Boolean = false
  var spriteEnable: Boolean = false
  var backgroundEnable: Boolean = false
  var spriteLeftColumnEnabled: Boolean = false
  var backgroundLeftColumnEnabled: Boolean = false
  var greyscale: Boolean = false
  var vblankFlag: Boolean = false

  var sprite0Hit: Boolean = false
  var spriteOverflow: Boolean = false

  def renderingEnabled: Boolean = spriteEnable || backgroundEnable

  var oamAddr: Byte = 0x00.toByte
  def spriteId = (oamAddr.toU8 >> 2).toInt
  def spriteByte = (oamAddr.toU8 & 0x3).toInt

  var oamLatch: U8 = 0x00.toU8

  var scrollX: Int = 0
  def fineScrollX: Int = scrollX & 0x7
  def courseScrollX: Int = (scrollX & 0xf8) >> 3
  def updateCourseScrollX(value: Int): Unit = {
    scrollX = (value << 3) & 0xf8 | fineScrollX
  }

  var scrollY: Int = 0
  def fineScrollY: Int = scrollY & 0x7
  def courseScrollY: Int = (scrollY & 0xf8) >> 3
  def updateCourseScrollY(value: Int): Unit = {
    scrollY = ((value << 3) & 0xf8) | fineScrollY
  }

  var sharedWriteLatch: SharedWriteLatch = SharedWriteLatch.First

  var oamDMACyclesRemaining: Int = 0
  var oamDMASrcAddr: Addr = 0x0000.toAddr

  val secondarySpriteMem: Array[Sprite] =
    Array.fill(secondarySpriteCount)(Sprite.zero)
  val spriteMem: Array[Sprite] = Array.fill(spriteCount)(Sprite.zero)

  var spriteEvaluation: SpriteEvaluation = SpriteEvaluation.initial
  var sprite0InSecondary: Boolean = true
  var spriteRendering: SpriteRendering = SpriteRendering.initial
  val renderSprites: Array[RenderSprite] =
    Array.fill(renderSpriteCount)(RenderSprite.zero)

  var tileRendering: TileRendering = TileRendering.initial

  object outputRegisters {
    var low: U16 = 0.toU16
    var high: U16 = 0.toU16
    var attrLow: U16 = 0.toU16
    var attrHigh: U16 = 0.toU16

    def orIn(renderTile: RenderTile): Unit = {
      low = low | renderTile.lowBits.toU16
      high = high | renderTile.highBits.toU16
      if renderTile.attrBits.testBit(0) then attrLow = attrLow | 0xff.toU16
      else attrLow = attrLow | 0x00.toU16

      if renderTile.attrBits.testBit(1) then attrHigh = attrHigh | 0xff.toU16
      else attrHigh = attrHigh | 0x00.toU16
    }

    def shift(): Unit = {
      low = (low << 1) & 0xffff
      high = (high << 1) & 0xffff
      attrLow = (attrLow << 1) & 0xffff
      attrHigh = (attrHigh << 1) & 0xffff
    }

    def shiftIn(renderTile: RenderTile): Unit = {
      low = (low << 8) & 0xffff
      high = (high << 8) & 0xffff
      attrLow = (attrLow << 8) & 0xffff
      attrHigh = (attrHigh << 8) & 0xffff
      orIn(renderTile)
    }

    def palette(x: Int): Int = {
      val shift = 15 - x
      val out = {
        (attrLow >> shift) & 0x01
      } | {
        (attrHigh >> (shift - 1)) & 0x02
      }

      out.toInt
    }

    def pixel(x: Int): Int = {
      val shift = 15 - x
      val out = {
        (low >> shift) & 0x01
      } | {
        (high >> (shift - 1)) & 0x02
      }

      out.toInt
    }
  }

  def nextStatusValue: U8 = {
    (cpuBusLatch & 0x1f) | {
      vblankFlag.toBit(7)
    } | {
      sprite0Hit.toBit(6)
    } | {
      spriteOverflow.toBit(5)
    }
  }

  object v {
    var courseScrollX: Int = 0
    var courseScrollY: Int = 0
    var fineScrollY: Int = 0
    var nametableX: Int = 0
    var nametableY: Int = 0

    def value: U16 =
      courseScrollX.toU16 | (
        courseScrollY.toU16 << 5
      ) | (
        nametableX.toU16 << 10
      ) | (
        nametableY.toU16 << 11
      ) | (
        fineScrollY.toU16 << 12
      )

    def update(v: U16): Unit = {
      val v0 = (v & 0x7fff).toInt
      fineScrollY = (v0 >> 12) & 0x7
      nametableY = (v0 >> 11) & 0x1
      nametableX = (v0 >> 10) & 0x1
      courseScrollY = (v0 >> 5) & 0x1f
      courseScrollX = v0 & 0x1f
    }
  }

  object t {
    def value: U16 =
      courseScrollX.toU16 | (
        courseScrollY.toU16 << 5
      ) | (
        nametableX.toU16 << 10
      ) | (
        nametableY.toU16 << 11
      ) | (
        fineScrollY.toU16 << 12
      )

    def updateHigh(byte: U8): Unit = {
      // truncated to 1 bit. Corresponds to bit 14 cleared in a 15 bit register.
      val truncatedFineY = (byte >> 4) & 0x03
      baseNametableIndex = ((byte >> 2) & 0x03).toInt
      val updatedCourseScrollY = (courseScrollY.toU16 & 0x07) | (
        (byte.toU16 & 0x03) << 3
      )
      scrollY = {
        truncatedFineY.toU16 | (
          updatedCourseScrollY << 3
        )
      }.toInt
    }

    def updateLow(byte: U8): Unit = {
      updateCourseScrollX((byte & 0x1f).toInt)
      val yHighBits = courseScrollY & 0x18
      val yLowBits = byte >> 5
      updateCourseScrollY(yHighBits | yLowBits.toInt)
    }
  }

  def updateVFromT(): Unit = {
    updateVVertFromT()
    updateVHorizFromT()
  }

  def updateVHorizFromT(): Unit = {
    v.courseScrollX = courseScrollX
    v.nametableX = nametableX
  }

  def updateVVertFromT(): Unit = {
    v.courseScrollY = courseScrollY
    v.nametableY = nametableY
    v.fineScrollY = fineScrollY
  }

  /** Linear increment by 1
    */
  def incrementV1(): Unit = {
    val bits = v.value
    v.update(bits + 1)
  }

  /** Linear increment by 32
    */
  def incrementV32(): Unit = {
    val bits = v.value
    v.update(bits + 32)
  }

  def incrementVCourseHoriz(): Unit = {
    if (v.courseScrollX == 31) then {
      v.courseScrollX = 0
      v.nametableX ^= 1
    } else {
      v.courseScrollX += 1
    }
  }

  def incrementVVert(): Unit = {
    if (v.fineScrollY < 7) then {
      v.fineScrollY += 1
    } else {
      v.fineScrollY = 0
      incrementVCourseVert()
    }
  }

  def incrementVCourseVert(): Unit =
    if (v.courseScrollY == 29) then {
      v.courseScrollY = 0
      v.nametableY ^= 1
    } else if (v.courseScrollY == 31) {
      v.courseScrollY = 0
    } else {
      v.courseScrollY += 1
    }
}

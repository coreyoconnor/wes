package wes
package nes
package ppu

case class SpriteRendering(
    sprite: Option[RenderSprite]
)

object SpriteRendering {
  val initial: SpriteRendering = SpriteRendering(sprite = None)

  def next(
      x: Int,
      leftColumnEnabled: Boolean,
      renderSprites: IndexedSeq[RenderSprite]
  ): SpriteRendering = {
    val column = x / 8
    if ((column == 0) && !leftColumnEnabled) || (column >= (Structure.outputWidth / 8))
    then SpriteRendering(None)
    else {
      val minX = (x - 7).max(0)
      val maxX = x

      val sprite = renderSprites.filter { rs =>
        val spriteX = rs.x.toInt
        spriteX >= minX && spriteX <= maxX
      }.headOption

      SpriteRendering(sprite)
    }
  }
}

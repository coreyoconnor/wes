package wes
package nes
package ppu

enum SpriteSize {
  case `8x8`, `8x16`
}

package wes
package nes
package ppu

object Structure {
  val ppuCycleHz = 21441960L / 4L
  val ppuCyclesPerCPUCycle = 3
  val cyclesPerScanline = 341
  val minCyclesUntilCtrlWrite = 29658L * ppuCyclesPerCPUCycle
  val cyclesForDMA = 513

  val outputStartH = 1
  val outputWidth = 256
  val hblankStart = outputWidth + 1
  val nextTilesStart = 321

  val scanlinesPerFrame = 262
  val preRenderScanline = scanlinesPerFrame - 1
  val outputStartV = 0
  val outputHeight = 240

  val leftBorder = 16
  val rightBorder = 11
  val bottomBorder = 2

  val nametableBankSize = 0x400
  val nametableWidth = 32
  val nametableHeight = 30

  val paletteSize = 4
  val spriteCount = 64
  val secondarySpriteCount = 8
  val renderSpriteCount = 8
  val renderTileCount = 2
}

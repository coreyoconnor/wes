package wes
package nes
package ppu

import wes.native.conversions.*
import wes.native.types.*

case class RenderSprite(
    x: U8,
    lowBits: U8,
    highBits: U8,
    palette: U8,
    behindBackground: Boolean,
    flipHoriz: Boolean,
    isSprite0: Boolean
) {
  // TODO don't take in offset from x. iterate?
  def pixel(u: Int): Int = {
    val shift = if flipHoriz then {
      u - x.toInt
    } else {
      7 - (u - x.toInt)
    }

    val out = {
      (lowBits >> shift) & 0x01
    } | {
      ((highBits >> shift) & 0x01) << 1
    }

    out.toInt
  }
}

object RenderSprite {
  val zero = RenderSprite(0xff.toU8, 0.toU8, 0.toU8, 0.toU8, false, false, false)
}

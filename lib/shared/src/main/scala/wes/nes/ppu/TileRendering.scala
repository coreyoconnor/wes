package wes
package nes
package ppu

import wes.native.conversions.*
import wes.native.types.*
import wes.nes.ppu.palette.*

case class TileRendering(
    nextRenderTile: RenderTile = RenderTile.zero,
    nextNametableValue: U8 = 0.toU8,
    nextAttrs: U8 = 0.toU8,
    nextLowBits: U8 = 0.toU8
)

object TileRendering {
  def initial: TileRendering = TileRendering()
}

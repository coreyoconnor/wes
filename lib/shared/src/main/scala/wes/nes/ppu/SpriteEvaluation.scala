package wes
package nes
package ppu

import wes.native.conversions.*
import wes.native.types.*

case class SpriteEvaluation(vPos: Int, state: SpriteEvaluation.State) {
  val scanline: Int = vPos
}

object SpriteEvaluation {
  sealed abstract class State
  case object PreOutputH extends State
  case class SecondaryOAMClear(cycle: Int) extends State
  object SecondaryOAMClear {
    val maxCycle = 63
  }
  sealed abstract class Eval extends State

  object Eval {
    case class ReadByte(inIndex: Int, outIndex: Int, b: Int) extends Eval
    case class WriteByte(inIndex: Int, outIndex: Int, b: Int) extends Eval
    case class SkipSpriteWriteByte(inIndex: Int, outIndex: Int) extends Eval
    case class SpriteOverflow(inIndex: Int) extends Eval
    case class SkipAllWriteByte(outIndex: Int) extends Eval
    case class SkipAllReadByte(outIndex: Int) extends Eval
  }

  case class SpriteFetches(n: Int, cycle: Int, secondaryCount: Int) extends State
  case object BackgroundPipelineInit extends State
  case object Disabled extends State
  object PreRender {
    val maxCycle = 260
  }
  case class PreRender(cycle: Int) extends State

  val initial: SpriteEvaluation = SpriteEvaluation(-1, Disabled)

  def startScanline(vPos: Int): SpriteEvaluation =
    if vPos == Structure.preRenderScanline
    then SpriteEvaluation(vPos, PreRender(0))
    else if vPos >= Structure.outputHeight && vPos < Structure.preRenderScanline
    then SpriteEvaluation(vPos, Disabled)
    else SpriteEvaluation(vPos, PreOutputH)

  def next(
      hPos: Int,
      vPos: Int,
      oamLatch: U8,
      spriteSize: SpriteSize,
      sprites: Seq[Sprite],
      current: SpriteEvaluation
  ): SpriteEvaluation =
    if vPos != current.vPos
    then startScanline(vPos)
    else {
      import Eval.*
      val nextSubstate = current.state match {
        case Disabled => Disabled

        case PreOutputH => SecondaryOAMClear(0)

        case SecondaryOAMClear(cycle) =>
          if cycle == SecondaryOAMClear.maxCycle
          then ReadByte(0, 0, 0)
          else SecondaryOAMClear(cycle + 1)

        case ReadByte(in, out, 0) => {
          val y = current.scanline
          val minY = oamLatch.toInt
          val maxY = spriteSize match {
            case SpriteSize.`8x8`  => (minY + 8).min(Structure.outputHeight)
            case SpriteSize.`8x16` => (minY + 16).min(Structure.outputHeight)
          }
          if out == 8 then {
            SpriteOverflow(in)
          } else {
            if (y >= minY) && (y < maxY) then WriteByte(in, out, 0)
            else SkipSpriteWriteByte(in, out)
          }
        }

        case ReadByte(in, out, b) => WriteByte(in, out, b)

        case SkipSpriteWriteByte(63, out) =>
          if hPos == Structure.hblankStart
          then SpriteFetches(0, 0, out)
          else SkipAllReadByte(out)

        case SkipSpriteWriteByte(in, out) => ReadByte(in + 1, out, 0)

        case SkipAllWriteByte(out) =>
          if hPos == Structure.hblankStart
          then SpriteFetches(0, 0, out)
          else SkipAllReadByte(out)

        case SkipAllReadByte(out) =>
          if hPos == Structure.hblankStart
          then SpriteFetches(0, 0, out)
          else SkipAllWriteByte(out)

        case WriteByte(63, out, 3) =>
          // I don't think ever takes up to hblank
          if hPos == Structure.hblankStart
          then SpriteFetches(0, 0, out + 1)
          else SkipAllReadByte(out + 1)

        case WriteByte(in, out, 3) => ReadByte(in + 1, out + 1, 0)

        case WriteByte(in, out, b) => ReadByte(in, out, b + 1)

        case SpriteOverflow(in) =>
          if hPos == Structure.hblankStart
          then SpriteFetches(0, 0, 8)
          else SpriteOverflow(in)

        case SpriteFetches(7, 7, _) => BackgroundPipelineInit

        case SpriteFetches(n, 7, secondaryCount) =>
          SpriteFetches(n + 1, 0, secondaryCount)

        case SpriteFetches(n, b, secondaryCount) =>
          SpriteFetches(n, b + 1, secondaryCount)

        case PreRender(cycle) =>
          if cycle == PreRender.maxCycle
          then SpriteFetches(0, 0, 0)
          else PreRender(cycle + 1)

        case BackgroundPipelineInit => BackgroundPipelineInit
      }

      SpriteEvaluation(vPos, nextSubstate)
    }
}

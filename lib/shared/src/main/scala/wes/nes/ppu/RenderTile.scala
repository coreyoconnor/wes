package wes
package nes
package ppu

import wes.native.conversions.*
import wes.native.types.*

case class RenderTile(
    lowBits: U8,
    highBits: U8,
    // 2 bits
    attrBits: U8
)

object RenderTile {
  val zero = RenderTile(0.toU8, 0.toU8, 0.toU8)
}

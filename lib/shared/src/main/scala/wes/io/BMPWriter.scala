package wes
package io

import scodec.*
import scodec.bits.*
import scodec.codecs.*

object BMPWriter {
  case class BMPHeader(offset: Int)

  implicit val bmpHeaderCodec: Codec[BMPHeader] = {
    ("typeB" | constant(byte.encode('B'.toByte).require)) ::
      ("typeM" | constant(byte.encode('M'.toByte).require)) ::
      ("fileSize" | constant(int32L.encode(0).require)) ::
      ("reserved" | constant(int32L.encode(0).require)) ::
      ("offset" | int32L)
  }.dropUnits.as[BMPHeader]

  case class DIBHeader(imageWidth: Int, imageHeight: Int, hPPM: Int, vPPM: Int)

  implicit val dibHeaderCodec: Codec[DIBHeader] = {
    ("headerSize" | constant(int32L.encode(40).require)) ::
      ("imageWidth" | int32L) ::
      ("imageHeight" | int32L) ::
      ("numColorPlanes" | constant(int16L.encode(1).require)) ::
      ("numBitsPerPixel" | constant(int16L.encode(24).require)) ::
      ("compressionMethod" | constant(int32L.encode(0).require)) ::
      ("imageSize" | constant(int32L.encode(0).require)) ::
      ("hPPM" | int32L) ::
      ("vPPM" | int32L) ::
      ("numColors" | constant(int32L.encode(0).require)) ::
      ("numImportantColors" | constant(int32L.encode(0).require))
  }.dropUnits.as[DIBHeader]

  case class Pixel(b: Byte, g: Byte, r: Byte)

  implicit val pixelCodec: Codec[Pixel] = {
    ("b" | byte) :: ("g" | byte) :: ("r" | byte)
  }.as[Pixel]

  def encodeRow(row: Seq[Pixel]): BitVector = {
    val out = Codec[Pixel].encodeAll(row).require
    assert((out.size / 8) % 4 == 0)
    out
  }

  def encodeImage(pixels: Seq[Pixel], width: Int): BitVector = {
    assert(pixels.size % width == 0)
    val height = pixels.size / width
    val rows = pixels.grouped(width).toSeq.reverse // bottom up for bmp

    val headerSize = bmpHeaderCodec.sizeBound
    assert(headerSize.exact == Some(14 * 8))
    val dibHeaderSize = dibHeaderCodec.sizeBound
    assert(dibHeaderSize.exact == Some(40 * 8))

    val pixelData = rows.foldLeft(BitVector.empty) { (acc, row) =>
      acc ++ encodeRow(row)
    }

    val dibHeader = DIBHeader(width, height, 36, 36)
    val header = BMPHeader(54)

    val headerBits = (Codec[BMPHeader] :: Codec[DIBHeader])
      .encode(header, dibHeader)
      .require

    headerBits ++ pixelData
  }

  def out(pixels: Seq[Pixel], width: Int, path: String): Unit = {
    val outBits = encodeImage(pixels, width)
    import java.nio.file.{Files, FileSystems}

    val outBytes = outBits.toByteArray
    val outPath = FileSystems.getDefault().getPath(path)
    Files.write(outPath, outBytes)
    ()
  }
}

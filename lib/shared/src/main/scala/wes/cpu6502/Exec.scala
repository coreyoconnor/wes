package wes
package cpu6502

import wes.native.conversions.*
import wes.native.types.*

import scala.quoted.*

object Exec extends SingleDispatch {
  import CPU6502.*

  def derive[State <: SystemState: Type](
      bus: Expr[RWBus[Addr, State]],
      state: Expr[State]
  )(using Quotes): Expr[Cycles] = {
    import quotes.reflect.*

    val out = '{
      val opcode = $bus.readU8($state.cpu.registers.pc, $state)
      $state.cpu.registers.incPC

      ${
        val dispatch = new Dispatch[Cycles] {
          def absolute(mode: Expr[AbsoluteMode[?]]): Expr[Cycles] = '{
            val addr = $bus.readU16($state.cpu.registers.pc, $state)
            $state.cpu.registers.incPC2
            $mode.absolute($bus, $state, addr)
          }

          def absoluteX(mode: Expr[AbsoluteXMode[?]]): Expr[Cycles] = '{
            val immAddr = $bus.readU16($state.cpu.registers.pc, $state)
            $state.cpu.registers.incPC2
            val addr = $state.cpu.registers.x.toU16 + immAddr
            val assumedPage = immAddr & 0xff00
            val actualPage = addr & 0xff00
            $mode.absoluteX($bus, $state, addr) + (assumedPage != actualPage)
              .toBit(0)
          }

          def absoluteY(mode: Expr[AbsoluteYMode[?]]): Expr[Cycles] = '{
            val immAddr = $bus.readU16($state.cpu.registers.pc, $state)
            $state.cpu.registers.incPC2
            val addr = $state.cpu.registers.y.toU16 + immAddr
            val assumedPage = immAddr & 0xff00
            val actualPage = addr & 0xff00
            $mode.absoluteY($bus, $state, addr) + (assumedPage != actualPage)
              .toBit(0)
          }

          def accumulator(mode: Expr[AccumulatorMode[?]]): Expr[Cycles] = '{
            $mode.accumulator($bus, $state)
          }

          def immediate(mode: Expr[ImmediateMode[?]]): Expr[Cycles] = '{
            val imm = $bus.readU8($state.cpu.registers.pc, $state)
            $state.cpu.registers.incPC
            $mode.immediate($bus, $state, imm)
          }

          def implied(mode: Expr[ImpliedMode[?]]): Expr[Cycles] = '{
            $mode.implied($bus, $state)
          }

          def indirect(mode: Expr[IndirectMode[?]]): Expr[Cycles] = '{
            val indirectAddr = $bus.readU16($state.cpu.registers.pc, $state)
            $state.cpu.registers.incPC2
            val addr = readIndirectU16($bus, $state, indirectAddr)
            $mode.indirect($bus, $state, addr)
          }

          def indirectX(mode: Expr[IndirectXMode[?]]): Expr[Cycles] = '{
            val index = $bus.readU8($state.cpu.registers.pc, $state)
            $state.cpu.registers.incPC
            val indirectAddr = index + $state.cpu.registers.x
            val addr = readIndirectU16($bus, $state, indirectAddr.toU16)
            $mode.indirectX($bus, $state, addr)
          }

          def indirectY(mode: Expr[IndirectYMode[?]]): Expr[Cycles] = '{
            val index = $bus.readU8($state.cpu.registers.pc, $state)
            $state.cpu.registers.incPC
            val addrY = readIndirectU16(
              $bus,
              $state,
              index.toU16
            )
            val addr = addrY + $state.cpu.registers.y.toU16
            val assumedPage = addrY & 0xff00
            val actualPage = addr & 0xff00
            $mode.indirectY($bus, $state, addr) + (assumedPage != actualPage)
              .toBit(0)
          }

          def relative(mode: Expr[RelativeMode[?]]): Expr[Cycles] = '{
            val offset = $bus.readU8($state.cpu.registers.pc, $state)
            $state.cpu.registers.incPC
            $mode.relative($bus, $state, offset)
          }

          def zeroPage(mode: Expr[ZeroPageMode[?]]): Expr[Cycles] = '{
            val offset = $bus.readU8($state.cpu.registers.pc, $state)
            $state.cpu.registers.incPC
            $mode.zeroPage($bus, $state, offset.toU16)
          }

          def zeroPageX(mode: Expr[ZeroPageXMode[?]]): Expr[Cycles] = '{
            val offset = $bus.readU8($state.cpu.registers.pc, $state)
            $state.cpu.registers.incPC
            val addr = $state.cpu.registers.x + offset
            $mode.zeroPageX($bus, $state, addr.toU16)
          }

          def zeroPageY(mode: Expr[ZeroPageYMode[?]]): Expr[Cycles] = '{
            val offset = $bus.readU8($state.cpu.registers.pc, $state)
            $state.cpu.registers.incPC
            val addr = $state.cpu.registers.y + offset
            $mode.zeroPageY($bus, $state, addr.toU16)
          }

          def unknown: Expr[Cycles] = '{
            sys.error(s"unknown opcode $opcode")
          }
        }

        dispatch.expr('opcode)
      }
    }

    // println(out.asTerm.show(using Printer.TreeCode))

    out
  }
}

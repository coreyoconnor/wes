package wes
package cpu6502

import wes.native.conversions.*
import wes.native.types.*

import scala.annotation.switch
import scala.quoted.*

object Disasm extends SingleDispatch {
  import CPU6502.*

  def derive[Reader: Type, Bus <: ReadBus[PC, Reader]: Type](
      bus: Expr[Bus],
      reader: Expr[Reader]
  )(using Quotes): Expr[PC => (Inst, Offset)] = {
    import quotes.reflect.*

    val out = '{ (pc: PC) =>
      val opcode = $bus.readU8(pc, $reader)

      ${
        val dispatch = new Dispatch[(Inst, Offset)] {
          def absolute(mode: Expr[AbsoluteMode[?]]): Expr[(Inst, Offset)] = '{
            val opcode = $mode.Absolute
            val addr = $bus.readU16(pc.inc, $reader)
            (opcode.asm(addr), opcode.size)
          }

          def absoluteX(mode: Expr[AbsoluteXMode[?]]): Expr[(Inst, Offset)] = '{
            val opcode = $mode.AbsoluteX
            val addr = $bus.readU16(pc.inc, $reader)
            (opcode.asm(addr), opcode.size)
          }

          def absoluteY(mode: Expr[AbsoluteYMode[?]]): Expr[(Inst, Offset)] = '{
            val opcode = $mode.AbsoluteY
            val addr = $bus.readU16(pc.inc, $reader)
            (opcode.asm(addr), opcode.size)
          }

          def accumulator(
              mode: Expr[AccumulatorMode[?]]
          ): Expr[(Inst, Offset)] = '{
            val opcode = $mode.Accumulator
            (opcode.asm, opcode.size)
          }

          def immediate(mode: Expr[ImmediateMode[?]]): Expr[(Inst, Offset)] = '{
            val opcode = $mode.Immediate
            val imm = $bus.readU8(pc.inc, $reader)
            (opcode.asm(imm), opcode.size)
          }

          def implied(mode: Expr[ImpliedMode[?]]): Expr[(Inst, Offset)] = '{
            val opcode = $mode.Implied
            (opcode.asm, opcode.size)
          }

          def indirect(mode: Expr[IndirectMode[?]]): Expr[(Inst, Offset)] = '{
            val opcode = $mode.Indirect
            val addr = $bus.readU16(pc.inc, $reader)
            (opcode.asm(addr), opcode.size)
          }

          def indirectX(mode: Expr[IndirectXMode[?]]): Expr[(Inst, Offset)] = '{
            val opcode = $mode.IndirectX
            val index = $bus.readU8(pc.inc, $reader)
            (opcode.asm(index), opcode.size)
          }

          def indirectY(mode: Expr[IndirectYMode[?]]): Expr[(Inst, Offset)] = '{
            val opcode = $mode.IndirectY
            val index = $bus.readU8(pc.inc, $reader)
            (opcode.asm(index), opcode.size)
          }

          def relative(mode: Expr[RelativeMode[?]]): Expr[(Inst, Offset)] = '{
            val opcode = $mode.Relative
            val offset = $bus.readU8(pc.inc, $reader)
            (opcode.asm(offset), opcode.size)
          }

          def zeroPage(mode: Expr[ZeroPageMode[?]]): Expr[(Inst, Offset)] = '{
            val opcode = $mode.ZeroPage
            val offset = $bus.readU8(pc.inc, $reader)
            (opcode.asm(offset), opcode.size)
          }

          def zeroPageX(mode: Expr[ZeroPageXMode[?]]): Expr[(Inst, Offset)] = '{
            val opcode = $mode.ZeroPageX
            val offset = $bus.readU8(pc.inc, $reader)
            (opcode.asm(offset), opcode.size)
          }

          def zeroPageY(mode: Expr[ZeroPageYMode[?]]): Expr[(Inst, Offset)] = '{
            val opcode = $mode.ZeroPageY
            val offset = $bus.readU8(pc.inc, $reader)
            (opcode.asm(offset), opcode.size)
          }

          def unknown: Expr[(Inst, Offset)] = '{ ("???", 1) }
        }

        dispatch.expr('opcode)
      }
    }

    // println(out.asTerm.show(using Printer.TreeCode))

    out
  }
}

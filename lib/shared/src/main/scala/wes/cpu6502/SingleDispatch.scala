package wes
package cpu6502

import wes.native.types.*

import scala.annotation.switch
import scala.quoted.*

trait SingleDispatch {
  import CPU6502.*

  def modeTags(using Quotes) = {
    import quotes.reflect.*

    Seq(
      TypeRepr.of[AbsoluteMode],
      TypeRepr.of[AbsoluteXMode],
      TypeRepr.of[AbsoluteYMode],
      TypeRepr.of[AccumulatorMode],
      TypeRepr.of[ImmediateMode],
      TypeRepr.of[ImpliedMode],
      TypeRepr.of[IndirectMode],
      TypeRepr.of[IndirectXMode],
      TypeRepr.of[IndirectYMode],
      TypeRepr.of[RelativeMode],
      TypeRepr.of[ZeroPageMode],
      TypeRepr.of[ZeroPageXMode],
      TypeRepr.of[ZeroPageYMode]
    ).map(_.baseClasses.head)
  }

  abstract class Dispatch[T: Type](using Quotes) {
    import quotes.reflect.*

    def absolute(mode: Expr[AbsoluteMode[?]]): Expr[T]
    def absoluteX(mode: Expr[AbsoluteXMode[?]]): Expr[T]
    def absoluteY(mode: Expr[AbsoluteYMode[?]]): Expr[T]
    def accumulator(mode: Expr[AccumulatorMode[?]]): Expr[T]
    def immediate(mode: Expr[ImmediateMode[?]]): Expr[T]
    def implied(mode: Expr[ImpliedMode[?]]): Expr[T]
    def indirect(mode: Expr[IndirectMode[?]]): Expr[T]
    def indirectX(mode: Expr[IndirectXMode[?]]): Expr[T]
    def indirectY(mode: Expr[IndirectYMode[?]]): Expr[T]
    def relative(mode: Expr[RelativeMode[?]]): Expr[T]
    def zeroPage(mode: Expr[ZeroPageMode[?]]): Expr[T]
    def zeroPageX(mode: Expr[ZeroPageXMode[?]]): Expr[T]
    def zeroPageY(mode: Expr[ZeroPageYMode[?]]): Expr[T]

    def unknown: Expr[T]

    def expr(opcode: Expr[U8]): Expr[T] = {
      val cpu = '{ CPU6502 }
      val tree: Term = cpu.asTerm
      val declTrait = TypeRepr.of[Decl].baseClasses.head

      val Seq(
        absoluteModeTag,
        absoluteXModeTag,
        absoluteYModeTag,
        accumulatorModeTag,
        immediateModeTag,
        impliedModeTag,
        indirectModeTag,
        indirectXModeTag,
        indirectYModeTag,
        relativeModeTag,
        zeroPageModeTag,
        zeroPageXModeTag,
        zeroPageYModeTag
      ) = modeTags

      val instDecls = tree.underlying.symbol.declaredFields
        .filter(_.typeRef.derivesFrom(declTrait))
        .filterNot(_.isAbstractType)

      val opcodeCases = for (instDeclSym <- instDecls) yield {
        val instDecl = tree.select(instDeclSym).asExprOf[Decl]
        val modes = '{ $instDecl.Modes }

        val cases = modes.asTerm.underlying.tpe.baseClasses.collect {
          case c if c == absoluteModeTag => {
            val mode = modes.asExprOf[AbsoluteMode[?]]
            val Expr(opcode: Int) = '{ $mode.Absolute.opcode }: @unchecked
            (opcode, absolute(mode))
          }

          case c if c == absoluteXModeTag => {
            val mode = modes.asExprOf[AbsoluteXMode[?]]
            val Expr(opcode: Int) = '{ $mode.AbsoluteX.opcode }: @unchecked
            (opcode, absoluteX(mode))
          }

          case c if c == absoluteYModeTag => {
            val mode = modes.asExprOf[AbsoluteYMode[?]]
            val Expr(opcode: Int) = '{ $mode.AbsoluteY.opcode }: @unchecked
            (opcode, absoluteY(mode))
          }

          case c if c == accumulatorModeTag => {
            val mode = modes.asExprOf[AccumulatorMode[?]]
            val Expr(opcode: Int) = '{ $mode.Accumulator.opcode }: @unchecked
            (opcode, accumulator(mode))
          }

          case c if c == immediateModeTag => {
            val mode = modes.asExprOf[ImmediateMode[?]]
            val Expr(opcode: Int) = '{ $mode.Immediate.opcode }: @unchecked
            (opcode, immediate(mode))
          }

          case c if c == impliedModeTag => {
            val mode = modes.asExprOf[ImpliedMode[?]]
            val Expr(opcode: Int) = '{ $mode.Implied.opcode }: @unchecked
            (opcode, implied(mode))
          }

          case c if c == indirectModeTag => {
            val mode = modes.asExprOf[IndirectMode[?]]
            val Expr(opcode: Int) = '{ $mode.Indirect.opcode }: @unchecked
            (opcode, indirect(mode))
          }

          case c if c == indirectXModeTag => {
            val mode = modes.asExprOf[IndirectXMode[?]]
            val Expr(opcode: Int) = '{ $mode.IndirectX.opcode }: @unchecked
            (opcode, indirectX(mode))
          }

          case c if c == indirectYModeTag => {
            val mode = modes.asExprOf[IndirectYMode[?]]
            val Expr(opcode: Int) = '{ $mode.IndirectY.opcode }: @unchecked
            (opcode, indirectY(mode))
          }

          case c if c == relativeModeTag => {
            val mode = modes.asExprOf[RelativeMode[?]]
            val Expr(opcode: Int) = '{ $mode.Relative.opcode }: @unchecked
            (opcode, relative(mode))
          }

          case c if c == zeroPageModeTag => {
            val mode = modes.asExprOf[ZeroPageMode[?]]
            val Expr(opcode: Int) = '{ $mode.ZeroPage.opcode }: @unchecked
            (opcode, zeroPage(mode))
          }

          case c if c == zeroPageXModeTag => {
            val mode = modes.asExprOf[ZeroPageXMode[?]]
            val Expr(opcode: Int) = '{ $mode.ZeroPageX.opcode }: @unchecked
            (opcode, zeroPageX(mode))
          }

          case c if c == zeroPageYModeTag => {
            val mode = modes.asExprOf[ZeroPageYMode[?]]
            val Expr(opcode: Int) = '{ $mode.ZeroPageY.opcode }: @unchecked
            (opcode, zeroPageY(mode))
          }
        }

        cases map { (pattern, rhs) =>
          CaseDef(Expr(pattern).asTerm, None, rhs.asTerm)
        }
      }

      val unknownOpcode = CaseDef(Wildcard(), None, unknown.asTerm)

      val opcodeSwitch = Typed(
        '{ $opcode.toNativeWord }.asTerm,
        Annotated(TypeTree.of[Int], '{ new switch }.asTerm)
      )

      val opcodeMatch =
        Match(opcodeSwitch, opcodeCases.flatten :+ unknownOpcode)
      opcodeMatch.asExprOf[T]
    }
  }
}

# Intro

`wes` - an NES emulation tool. Intended to support both analysis and emulator of
the NES. Focus is more on analysis than emulation. Some consideration of 
expanding to other systems.

# Building

`sbt package`